#
# Build maven package
#
FROM maven:3.8.6-jdk-11-slim AS build

LABEL mantainer="tupn.hn@gmail.com"

# image layer
WORKDIR /app
ADD pom.xml /app
RUN mvn verify clean --fail-never

# copy source and build
COPY . /app
RUN mvn -v
RUN java -version
RUN mvn clean package

# -------------------------------------------------------------
# Copy .jar and run
#
FROM maven:3.8.6-jdk-11-slim
RUN mkdir -p /opt/ods-portal/
COPY --from=build /app/target/*.jar /opt/ods-portal/ods-portal.jar

# Run java app
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "/opt/ods-portal/ods-portal.jar"]



