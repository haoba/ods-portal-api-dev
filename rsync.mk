#!/usr/bin/make
# Synchronize data and init to all nodes
#

LOG_DIR=/opt/ods/ods-portal-api/logs

# Nodes to synchronize ----------------------------------------
NODES= \
    ods@kafka1          \
    ods@kafka2          \
    ods@kafka-connector \
    ods@job-pdi         \
    ods@job-airflow

# init log folder
$(NODES):
	@echo "$(GREEN)-----------------------------------------------------$(NC)"
	@echo "$(GREEN)Synchronize projects to node $@ $(NC)"
	ssh $@ "mkdir -p $(LOG_DIR)"

# Rsync all nodes
rsync:  $(NODES)
	@echo "$(GREEN)Synchronize complete.$(NC)"
