package vn.educa.ods.portalapi;

import com.ulisesbocchio.jasyptspringboot.annotation.EnableEncryptableProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@EnableEncryptableProperties
public class OdsPortalApiApplication {
    public static void main(String[] args) {
        SpringApplication.run(OdsPortalApiApplication.class, args);
    }
}
