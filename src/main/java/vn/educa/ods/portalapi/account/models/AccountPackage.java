package vn.educa.ods.portalapi.account.models;


import lombok.Getter;
import lombok.Setter;
import vn.educa.ods.commons.models.BaseEntity;
import vn.educa.ods.portalapi.packages.Packages;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Setter @Getter
@Entity
@Table( name = "account_packages")
public class AccountPackage extends BaseEntity {
    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    private String id;

    private String accountId;

    @Transient
    private Account account;

    private String  packageId;

    @Transient
    private Packages packages;

    private String activateCode;

    private Date startTime;

    private Date endTime;

    private BigDecimal type;

    private Boolean isPaid;

    private String product;

    private String productGroup;

    private Date createdAt;

    private Date updatedAt;
}
