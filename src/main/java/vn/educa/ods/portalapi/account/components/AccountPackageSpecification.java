package vn.educa.ods.portalapi.account.components;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;
import vn.educa.ods.portalapi.account.models.AccountPackage;

import javax.persistence.criteria.Predicate;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Component
public class AccountPackageSpecification {

    public static Specification<AccountPackage> filterBy(@NotNull AccountPackageFilter filter ){
        return (root, query, cb) -> {
            final List<Predicate> predicates = new ArrayList<>();

            // filter by field
            if (filter.getProduct() != null) {
                predicates.add(cb.equal(root.get("product"), filter.getProduct()));
            }
            if (filter.getActiveCode() != null) {
                predicates.add(cb.equal(root.get("activateCode"), filter.getActiveCode()));
            }
            // where in account
            final List<String> accountIds = filter.getAccountIds();
            if( accountIds != null && !accountIds.isEmpty() ){
                predicates.add( root.get("accountId" ).in( accountIds ));
            }
            // where date in range
            if( filter.getStartTime() != null ){
                predicates.add( cb.greaterThanOrEqualTo(root.get("startTime"  ), filter.getStartTime() ));
            }
            if( filter.getEndTime() != null ){
                predicates.add( cb.greaterThanOrEqualTo(root.get("endTime"  ), filter.getEndTime() ));
            }

            if( filter.getUpdatedFrom() != null ){
                predicates.add( cb.greaterThanOrEqualTo(root.get("updatedAt"  ), filter.getUpdatedFrom() ));
            }

            return cb.and(predicates.toArray(new Predicate[0]));
        };
    }
}
