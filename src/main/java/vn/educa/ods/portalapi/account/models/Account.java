package vn.educa.ods.portalapi.account.models;


import lombok.Getter;
import lombok.Setter;
import vn.educa.ods.commons.models.BaseEntity;

import javax.persistence.*;
import java.util.Date;

@Setter @Getter
@Entity
@Table( name = "accounts")
public class Account extends BaseEntity {
    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    private String id;

    private String system;

    private String username;

    private String fullName;

    private String phone;

    private String email;

    private Date dob;

//    @Column( name = "product" )
//    @Enumerated(EnumType.STRING)
//    private PRODUCT_CODE productCode

    private Date createdAt;

    private Date updatedAt;
}
