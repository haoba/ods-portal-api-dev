package vn.educa.ods.portalapi.account.components;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;
import vn.educa.ods.portalapi.account.models.AccountPackage;

import java.util.List;

@Repository
public interface AccountPackageRepository extends JpaRepository<AccountPackage,String>, JpaSpecificationExecutor<AccountPackage> {
    List<AccountPackage> findByAccountId( String accountId );
}
