package vn.educa.ods.portalapi.account.components;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;
import vn.educa.ods.portalapi.account.models.Account;

import javax.persistence.criteria.Predicate;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Component
public class AccountSpecification {

    public static Specification<Account> filterBy(@NotNull AccountFilter filter ){
        return (root, query, cb) -> {
            final List<Predicate> predicates = new ArrayList<>();

            // filter by field
            if (filter.getUsername() != null) {
                predicates.add(cb.equal(root.get("username"), filter.getUsername()));
            }
            if (filter.getPhone() != null) {
                predicates.add(cb.equal(root.get("phone"), filter.getPhone()));
            }
            if (filter.getFullName() != null) {
                predicates.add(cb.like(root.get("fullName"), "%" + filter.getFullName() + "%"));
            }

            return cb.and(predicates.toArray(new Predicate[0]));
        };
    }
}
