package vn.educa.ods.portalapi.account;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.*;
import vn.educa.ods.commons.controllers.BaseRestController;
import vn.educa.ods.commons.controllers.PageResponse;
import vn.educa.ods.commons.controllers.RestResponse;
import vn.educa.ods.commons.models.dto.AccountDto;
import vn.educa.ods.commons.models.dto.AccountPackageDto;
import vn.educa.ods.commons.models.dto.PackageDto;
import vn.educa.ods.portalapi.account.components.*;
import vn.educa.ods.portalapi.account.models.Account;
import vn.educa.ods.portalapi.account.models.AccountPackage;
import vn.educa.ods.portalapi.biz.masked.MaskedSensitiveDataService;
import vn.educa.ods.portalapi.exceptions.ResourceNotFoundException;
import vn.educa.ods.portalapi.helpers.PortalApiDesc;
import vn.educa.ods.portalapi.packages.Packages;
import vn.educa.ods.portalapi.packages.components.PackageMapper;
import vn.educa.ods.portalapi.packages.components.PackageRepository;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "api/v1/account", produces = "application/json")
@Tag(name = PortalApiDesc.ACCOUNT_API, description = PortalApiDesc.ACCOUNT_API_DESC )
@RequiredArgsConstructor
public class AccountPackageController extends BaseRestController {

    final AccountRepository accountRepository;

    final AccountPackageRepository accountPackageRepository;

    final PackageRepository packageRepository;

    final ProfileRepository profileRepository;

    final MaskedSensitiveDataService maskedSensitiveDataService;

    @GetMapping("{id}/packages")
    @Operation(
            summary = "Get packages of account",
            description = "Get all active packages of account"
    )
    public RestResponse<List<AccountPackageDto>> packages(@PathVariable String id){
        final Optional<Account> account = accountRepository.findById(id.toUpperCase());
        if(account.isEmpty()){
            throw new ResourceNotFoundException("[ODS] Account not found for this id:" + id );
        }

        List<AccountPackage> accountPackages = accountPackageRepository.findByAccountId( account.get().getId() );

        // get all ids
        List<Long> packageIds = accountPackages.stream().filter(
                x-> x.getPackageId() !=null
            ).map(
                x -> Long.valueOf( x.getPackageId() )
            ).collect(Collectors.toList());

        final HashMap<String,PackageDto> packages = new HashMap<>();
        packageRepository.findByIdIn( packageIds ).forEach( x->
                packages.put( String.valueOf( x.getId()), PackageMapper.INSTANCE.entity2dto( x ) )
        );

        List<AccountPackageDto> accountDtoList = accountPackages.stream().map( x->{
                AccountPackageDto dto = AccountMapper.INSTANCE.entity2dto( x );
                AccountDto accountDto = AccountMapper.INSTANCE.entity2dto( account.get() );

                // hidden sensitive data
                dto = maskedSensitiveDataService.mask( dto );
                dto.setAccount( accountDto );

                dto.setPackages( packages.getOrDefault( dto.getPackageId(), null ) );

                return dto;
            }
        ).collect(Collectors.toList());

        return new RestResponse.Builder<>(SUCCESS, accountDtoList).build();
    }

    /**
     * Search Study log of student
     * @param filter filter by
     * @param pageIndex page index
     * @param pageSize page size, default 50
     * @param orderBy order by data, default date
     * @param desc true for order desc/ otherwise order desc
     * @return pagination of StudyLogAverageDto
     */
    @PostMapping("package/search")
    @Operation(
            summary = "Search account package",
            description = "Search account package"
    )
    public PageResponse<AccountPackageDto> search(
                                                @RequestBody @Valid AccountPackageFilter filter,
                                                @RequestParam( required = false) Integer pageIndex,
                                                @RequestParam( required = false) Integer pageSize,
                                                @RequestParam( required = false, defaultValue = "updatedAt") String orderBy,
                                                @RequestParam( required = false, defaultValue = "true") Boolean desc
    ){
        Sort sort = Sort.by( orderBy );
        if( desc ){
            sort = sort.descending();
        }

        Pageable pageable = pageRequest( pageIndex, pageSize, sort );

        Page<AccountPackage> accountPackages = accountPackageRepository.findAll(
                AccountPackageSpecification.filterBy( filter ), pageable
        );

        // get all account information
        final List<String> accountIds = accountPackages.stream().map(
                AccountPackage::getAccountId
        ).collect(Collectors.toList());

        List<Account> accounts = accountRepository.findAllByIdIn( accountIds );

        final HashMap<String,Account> accountMap = new HashMap<>();
        accounts.forEach( x->{
            accountMap.put( x.getId(), x );
        });

        // get all packages
        final List<Long> packageIds = accountPackages.stream().map(
                x-> Long.valueOf( x.getPackageId() )
        ).collect(Collectors.toList());

        List<Packages> packages = packageRepository.findByIdIn( packageIds );

        final HashMap<String,PackageDto> packageMap = new HashMap<>();
        packages.forEach( x->{
            packageMap.put( "" + x.getId(), PackageMapper.INSTANCE.entity2dto(x) );
        });

        List<AccountPackageDto> packageDtoList = accountPackages.stream().map( x->{
                    AccountPackageDto dto = AccountMapper.INSTANCE.entity2dto( x );
                    AccountDto accountDto = AccountMapper.INSTANCE.entity2dto( accountMap.getOrDefault(
                            x.getAccountId(), null
                    ) );

                    // hidden sensitive data
                    dto = maskedSensitiveDataService.mask( dto );
                    dto.setAccount( accountDto );

                    dto.setPackages( packageMap.getOrDefault( x.getPackageId(), null ) );

                    return dto;
                }
        ).collect(Collectors.toList());

        return new PageResponse.Builder<>(SUCCESS, packageDtoList, accountPackages).build();
    }
}
