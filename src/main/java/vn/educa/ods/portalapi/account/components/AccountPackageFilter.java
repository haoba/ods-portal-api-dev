package vn.educa.ods.portalapi.account.components;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import vn.educa.ods.portalapi.helpers.PortalApiDesc;

import java.util.Date;
import java.util.List;

@Setter @Getter
@Schema( description = "Search account-package, package has bought by a user")
public class AccountPackageFilter {
    @Schema( description = "List of account")
    private List<String> accountIds;

    @Schema( description = PortalApiDesc.PRODUCT_CODE_DESC )
    private String product;

    @Schema( description = "Active code")
    private String activeCode;

    @Schema( description = "Start activation time")
    private Date startTime;

    @Schema( description = "Expired time")
    private Date endTime;

    @Schema( description = "Date has changed after this filter")
    private Date updatedFrom;
}
