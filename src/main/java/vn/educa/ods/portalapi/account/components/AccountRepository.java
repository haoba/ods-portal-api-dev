package vn.educa.ods.portalapi.account.components;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;
import vn.educa.ods.portalapi.account.models.Account;

import java.util.List;

@Repository
public interface AccountRepository extends JpaRepository<Account,String>, JpaSpecificationExecutor<Account> {
    List<Account> findByPhone( String phone );

    List<Account> findAllByIdIn( List<String> ids );
}
