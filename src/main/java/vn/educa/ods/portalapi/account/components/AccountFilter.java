package vn.educa.ods.portalapi.account.components;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

@Setter @Getter
@Schema( description = "Search account by username, similar fullname or phone")
public class AccountFilter {
    @Schema( description = "whole username")
    private String username;

    @Schema( description = "Similar full name")
    @Length( min = 3, message = "Not support search with name too short (must greater 3 characters)" )
    private String fullName;

    @Schema( description = "phone number")
    private String phone;
}
