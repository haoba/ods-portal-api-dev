package vn.educa.ods.portalapi.account.models;


import lombok.Getter;
import lombok.Setter;
import vn.educa.ods.commons.models.BaseEntity;
import vn.educa.ods.commons.models.enums.Gender;
import vn.educa.ods.portalapi.helpers.converters.gender.KidSystemGenderConverter;

import javax.persistence.*;
import java.util.Date;

@Setter @Getter
@Entity
@Table(schema = "educa_kid_new", name = "kid_user")
public class Profile extends BaseEntity {
    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    private Long id;

    private String profileName;

    private String avatar;

    @Column( name = "birthday" )
    private Date DOB;

    @Convert( converter = KidSystemGenderConverter.class )
    private Gender gender;

    private Long accountId;

    private String country;

    private Date createdAt;

    private Date updatedAt;
}
