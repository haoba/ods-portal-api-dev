package vn.educa.ods.portalapi.account;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.*;
import vn.educa.ods.commons.controllers.BaseRestController;
import vn.educa.ods.commons.controllers.PageResponse;
import vn.educa.ods.commons.controllers.RestResponse;
import vn.educa.ods.commons.models.dto.AccountDto;
import vn.educa.ods.commons.models.dto.ProfileDto;
import vn.educa.ods.portalapi.account.components.*;
import vn.educa.ods.portalapi.account.models.Account;
import vn.educa.ods.portalapi.account.models.Profile;
import vn.educa.ods.portalapi.exceptions.ResourceNotFoundException;
import vn.educa.ods.portalapi.helpers.PortalApiDesc;
import vn.educa.ods.portalapi.helpers.datesync.DateSyncFilter;
import vn.educa.ods.portalapi.helpers.datesync.DateSyncSpecification;
import vn.educa.ods.portalapi.packages.components.PackageRepository;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "api/v1/account", produces = "application/json")
@Tag(name = PortalApiDesc.ACCOUNT_API, description = PortalApiDesc.ACCOUNT_API_DESC )
@RequiredArgsConstructor
public class AccountController extends BaseRestController {

    final AccountRepository accountRepository;

    final AccountPackageRepository accountPackageRepository;

    final PackageRepository packageRepository;

    final ProfileRepository profileRepository;

    @GetMapping("{id}")
    public RestResponse<AccountDto> getAccount(@PathVariable String id){

        Optional<Account> account = accountRepository.findById(id.toUpperCase());
        if( account.isEmpty() ){
            throw new ResourceNotFoundException("[ODS] Account not found for this id:" + id );
        }

        AccountDto dto = AccountMapper.INSTANCE.entity2dto( account.get() );

        return new RestResponse.Builder<>(SUCCESS, dto).build();
    }

    @PostMapping("search")
    public PageResponse<AccountDto> search(@RequestBody @Valid AccountFilter filter,
                                           @RequestParam( required = false ) Integer pageIndex,
                                           @RequestParam( required = false ) Integer pageSize,
                                           @RequestParam( required = false, defaultValue = "updatedAt") String orderBy,
                                           @RequestParam( required = false, defaultValue = "false") Boolean desc ){

        Sort sort = Sort.by( orderBy );
        if( desc ){
            sort = sort.descending();
        }

        Pageable pageable = pageRequest( pageIndex, pageSize, sort );

        Page<Account> accountPagination = accountRepository.findAll(
                AccountSpecification.filterBy( filter ), pageable
        );

        List<AccountDto> accountDtoList = accountPagination.stream().map(
                AccountMapper.INSTANCE::entity2dto
        ).collect(Collectors.toList());

        return new PageResponse.Builder<>(SUCCESS, accountDtoList, accountPagination).build();
    }

    @GetMapping("profile/{id}")
    public RestResponse<ProfileDto> getProfile(@PathVariable String id){

        Optional<Profile> account = profileRepository.findById(id.toUpperCase());
        if( account.isEmpty()){
            throw new ResourceNotFoundException("[ODS] Account not found for this id:" + id );
        }

        ProfileDto dto = AccountMapper.INSTANCE.entity2dto( account.get() );

        return new RestResponse.Builder<>(SUCCESS, dto).build();
    }

    @Operation(
            summary = "Account synchronize",
            description = "Account synchronize by Updated Date"
    )
    @PostMapping("sync")
    public PageResponse<AccountDto> synchronize(@RequestBody DateSyncFilter filter,
                                                @RequestParam( required = false ) Integer pageIndex,
                                                @RequestParam( required = false ) Integer pageSize,
                                                @RequestParam( required = false, defaultValue = "updatedAt") String orderBy,
                                                @RequestParam( required = false, defaultValue = "false") Boolean desc ){

        Sort sort = Sort.by( orderBy );
        if( desc ){
            sort = sort.descending();
        }

        Pageable pageable = pageRequest( pageIndex, pageSize, sort );

        Page<Account> accountPagination = accountRepository.findAll(
                DateSyncSpecification.filterBySyncDate( filter ), pageable
        );

        List<AccountDto> accountDtoList = accountPagination.stream().map(
                AccountMapper.INSTANCE::entity2dto
        ).collect(Collectors.toList());

        return new PageResponse.Builder<>(SUCCESS, accountDtoList, accountPagination).build();
    }
}
