package vn.educa.ods.portalapi.account.components;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import vn.educa.ods.commons.models.dto.AccountDto;
import vn.educa.ods.commons.models.dto.AccountPackageDto;
import vn.educa.ods.commons.models.dto.ProfileDto;
import vn.educa.ods.portalapi.account.models.Account;
import vn.educa.ods.portalapi.account.models.AccountPackage;
import vn.educa.ods.portalapi.account.models.Profile;

@Mapper
public interface AccountMapper {
    AccountMapper INSTANCE = Mappers.getMapper( AccountMapper.class );

    AccountDto entity2dto(Account entity);
    Account    dto2entity(AccountDto dto);
    Account    clone(Account entity);

    ProfileDto entity2dto(Profile entity);
    Profile    dto2entity(ProfileDto dto);
    Profile    clone(Profile entity);

    AccountPackageDto entity2dto( AccountPackage entity );
    AccountPackage    dto2entity( AccountPackageDto entity );
    AccountPackage    clone( AccountPackage entity );
}
