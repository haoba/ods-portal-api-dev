package vn.educa.ods.portalapi.loglearn.category.components;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import vn.educa.ods.commons.models.dto.loglearn.StudyLogCategoryDto;
import vn.educa.ods.portalapi.loglearn.category.models.StudyLogCategory;

@Mapper
public interface StudyLogCategoryMapper {
    StudyLogCategoryMapper INSTANCE = Mappers.getMapper( StudyLogCategoryMapper.class );

    StudyLogCategoryDto entity2dto(StudyLogCategory entity);
    StudyLogCategory       dto2entity(StudyLogCategoryDto dto);
    StudyLogCategory       clone(StudyLogCategory entity);
}
