package vn.educa.ods.portalapi.loglearn.regular.components;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import vn.educa.ods.portalapi.helpers.PortalApiDesc;

import javax.persistence.MappedSuperclass;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.List;

@Setter @Getter
@Schema( description = "Log Learn Filter Conditional")
@MappedSuperclass
public abstract class LogLearnBaseFilter {
    @Schema( description = PortalApiDesc.PRODUCT_CODE_DESC )
    private String product;

    @Schema( description = "account id, must start with prefix EDU.x or KID.x")
    private String accountId;

    @Schema( description = "List of account id")
    @Size( max = 1000, message = "Number of accounts must be less 1000")
    private List<String> accountIds;

    @Schema( description = "username by lower case")
    private String username;

    @Schema( description = "start date", defaultValue = "2023-01-01T00:00:00.000Z")
    private Date dateStart;

    @Schema( description = "end date", defaultValue = "2023-02-01T00:00:00.000Z")
    private Date dateEnd;

    @Schema( description = "phone, auto append '0' if omit" )
    private String phone;
}
