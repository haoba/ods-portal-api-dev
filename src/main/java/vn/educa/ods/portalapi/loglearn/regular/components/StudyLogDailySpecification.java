package vn.educa.ods.portalapi.loglearn.regular.components;

import lombok.extern.slf4j.Slf4j;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;
import vn.educa.ods.portalapi.account.models.Account;
import vn.educa.ods.portalapi.lesson.models.Lesson;
import vn.educa.ods.portalapi.loglearn.regular.models.StudyLogDaily;
import vn.educa.ods.portalapi.utils.DateUtils;

import javax.persistence.criteria.Predicate;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Component
@Slf4j
public class StudyLogDailySpecification {

    public static Specification<StudyLogDaily> filterBy(@NotNull StudyLogDailyFilter filter, List<Account> accounts, List<Lesson> lessons ){
        return (root, query, cb) -> {
            final List<Predicate> predicates = new ArrayList<>();

            // filter by field
            if (filter.getProduct() != null) {
                predicates.add(cb.equal(root.get("product"), filter.getProduct()));
            }
            if (filter.getAccountId()!= null) {
                predicates.add( cb.equal(root.get("accountId"  ), filter.getAccountId() ));
            }

            // where in lessons
            final List<String> accountIds = filter.getAccountIds();
            if( accountIds!= null && !accountIds.isEmpty() ){
                predicates.add( root.get("accountId" ).in( accountIds ));
            }

            if( filter.getUsername()!=null ){
                predicates.add( cb.equal(root.get("username"  ), filter.getUsername() ));
            }

            // where in lessons
            if( lessons != null && !lessons.isEmpty() ){
                predicates.add( root.get("lessonId" ).in(
                        lessons.stream().map(
                                Lesson::getId
                        ).collect(Collectors.toList())
                ));
            }

            if( accounts != null && !accounts.isEmpty() ){
                predicates.add( root.get("accountId" ).in(
                        accounts.stream().map(
                                Account::getId
                        ).collect(Collectors.toList())
                ));
            }

            // where date in range
            if( filter.getDateStart() != null ){
                predicates.add( cb.greaterThanOrEqualTo(root.get("dateId"  ), DateUtils.dateToDateId( filter.getDateStart() ) ));
            }
            if( filter.getDateEnd() != null ){
                predicates.add( cb.lessThanOrEqualTo(root.get("dateId"  ), DateUtils.dateToDateId( filter.getDateEnd() ) ));
            }

            return cb.and(predicates.toArray(new Predicate[0]));
        };
    }

    public static Specification<StudyLogDaily> filterInList(String productCode, List<String> accountIds, List<Date> inDateRange ){
        return (root, query, cb) -> {
            final List<Predicate> predicates = new ArrayList<>();

            if( inDateRange.size()!=2 ){
                log.error("Internal Error! Date range required length=2");
                inDateRange.clear();
            }

            // filter by field
            if (productCode != null) {
                predicates.add(cb.equal(root.get("product"), productCode ));
            }

            if ( !accountIds.isEmpty() ) {
                predicates.add( root.get("accountId" ).in( accountIds ));
            }

            // where date in range
            if( inDateRange.get(0) != null ){
                predicates.add( cb.greaterThanOrEqualTo(root.get("dateId"  ), DateUtils.dateToDateId( inDateRange.get(0) ) ));
            }
            if( inDateRange.get(1) != null ){
                predicates.add( cb.lessThanOrEqualTo(root.get("dateId"  ), DateUtils.dateToDateId( inDateRange.get(1) ) ));
            }

            return cb.and(predicates.toArray(new Predicate[0]));
        };
    }
}
