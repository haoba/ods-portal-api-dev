package vn.educa.ods.portalapi.loglearn.regular.models;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.*;
import vn.educa.ods.portalapi.lesson.models.Lesson;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.*;
import java.math.BigDecimal;

@Setter @Getter
@Entity
@Table( name = "study_log_daily" )
public class StudyLogDaily {
    @Id
    private Long id;

    private String product;

    @Column(name = "userId")
    private String accountId;

    private String username;

    private int dateId;

    @ManyToOne( cascade = CascadeType.ALL, fetch = FetchType.EAGER )
    @NotFound(action = NotFoundAction.IGNORE)
    @BatchSize(size=1000)
    @Fetch(FetchMode.JOIN)
    private Lesson lesson;

    @Column(precision=10, scale=2)
    private BigDecimal partPointMax;

    @Column(precision=10, scale=2)
    private BigDecimal partPointMin;

    @Column( name = "partPointAgv", precision=10, scale=2)
    private BigDecimal partPointAvg;

    private int partCount;

    private String app;
}
