package vn.educa.ods.portalapi.loglearn.liveClass.components;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import vn.educa.ods.commons.models.dto.loglearn.StudyLogLiveClassDto;
import vn.educa.ods.portalapi.loglearn.liveClass.models.StudyLogLiveClass;

@Mapper
public interface StudyLogLiveClassMapper {
    StudyLogLiveClassMapper INSTANCE = Mappers.getMapper( StudyLogLiveClassMapper.class );

    StudyLogLiveClassDto entity2dto(StudyLogLiveClass entity);
    StudyLogLiveClass       dto2entity(StudyLogLiveClassDto dto);
    StudyLogLiveClass       clone(StudyLogLiveClass entity);
}
