package vn.educa.ods.portalapi.loglearn.champion.components;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;
import vn.educa.ods.portalapi.loglearn.champion.models.StudyLogChampion;

import java.util.List;
import java.util.Optional;

@Repository
public interface StudyLogChampionRepository extends JpaRepository<StudyLogChampion,String>,
        JpaSpecificationExecutor<StudyLogChampion> {
    Optional<StudyLogChampion> findByLessonId(String lessonId);
    List<StudyLogChampion> findAllByLessonIn(List<String> lessonIds );
}
