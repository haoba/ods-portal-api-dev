package vn.educa.ods.portalapi.loglearn.liveClass.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import vn.educa.ods.portalapi.exceptions.NotSupportedPortalApi;
import vn.educa.ods.portalapi.lesson.models.LiveClass;
import vn.educa.ods.portalapi.lesson.models.Part;
import vn.educa.ods.portalapi.loglearn.liveClass.components.StudyLogLiveClassFilter;
import vn.educa.ods.portalapi.loglearn.liveClass.components.StudyLogLiveClassKIDRepository;
import vn.educa.ods.portalapi.loglearn.liveClass.components.StudyLogLiveClassSpecification;
import vn.educa.ods.portalapi.loglearn.liveClass.components.StudyLogLiveClassTHRepository;
import vn.educa.ods.portalapi.loglearn.liveClass.models.StudyLogLiveClass;
import vn.educa.ods.portalapi.loglearn.liveClass.models.StudyLogLiveClassKID;
import vn.educa.ods.portalapi.loglearn.liveClass.models.StudyLogLiveClassTH;
import vn.educa.ods.portalapi.utils.ProductUtils;

import java.util.List;

@Service
public class StudyLogLiveClassServiceImpl implements StudyLogLiveClassService {

    final StudyLogLiveClassKIDRepository liveClassKIDRepository;
    final StudyLogLiveClassTHRepository liveClassTHRepository;

    @Autowired
    public StudyLogLiveClassServiceImpl(
            StudyLogLiveClassTHRepository liveClassTHRepository,
            StudyLogLiveClassKIDRepository liveClassKIDRepository ){
        this.liveClassKIDRepository = liveClassKIDRepository;
        this.liveClassTHRepository  = liveClassTHRepository;
    }

    public Page<? extends StudyLogLiveClass> search(StudyLogLiveClassFilter filter,
                                                    List<LiveClass> classes,
                                                    List<Part> parts, Pageable pageable){

        if( ProductUtils.isEduKID( filter.getProduct() ) ){
            return liveClassKIDRepository.findAll(
                    (Specification<StudyLogLiveClassKID>) StudyLogLiveClassSpecification.filterBy( filter, classes, parts ), pageable
            );
        }
        else if (ProductUtils.isEduTH( filter.getProduct() )){
            return liveClassTHRepository.findAll(
                    (Specification<StudyLogLiveClassTH>)StudyLogLiveClassSpecification.filterBy( filter, classes, parts ), pageable
            );
        }
        else {
            throw new NotSupportedPortalApi( "LiveClass not support for product: " + filter.getProduct() );
        }
    }
}
