package vn.educa.ods.portalapi.loglearn.regular.components;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import vn.educa.ods.commons.models.dto.loglearn.StudyLogAverageDto;
import vn.educa.ods.commons.models.dto.loglearn.StudyLogPartDto;
import vn.educa.ods.portalapi.loglearn.regular.models.StudyLogDaily;
import vn.educa.ods.portalapi.loglearn.regular.models.StudyLogPart;

@Mapper
public interface LogLearnMapper {
    LogLearnMapper INSTANCE = Mappers.getMapper( LogLearnMapper.class );

    StudyLogAverageDto  entity2dto(StudyLogDaily entity);
    StudyLogDaily       dto2entity(StudyLogAverageDto dto);
    StudyLogDaily       clone(StudyLogDaily entity);

    StudyLogPartDto     entity2dto(StudyLogPart entity);
    StudyLogPart        dto2entity(StudyLogPartDto dto);
    StudyLogPart        clone(StudyLogPart entity);
}
