package vn.educa.ods.portalapi.loglearn.regular.components;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;
import vn.educa.ods.portalapi.account.models.Account;
import vn.educa.ods.portalapi.lesson.models.Part;
import vn.educa.ods.portalapi.loglearn.regular.models.StudyLogPart;
import vn.educa.ods.portalapi.utils.DateUtils;

import javax.persistence.criteria.Predicate;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class StudyLogPartSpecification {

    public static Specification<? extends StudyLogPart> filterBy(@NotNull StudyLogPartFilter filter, List<Account> accounts, List<Part> parts ){
        return (root, query, cb) -> {
            final List<Predicate> predicates = new ArrayList<>();

            // filter by field
            // 2023.02.20-TuPN.Deleted.Start
            // PlatformLog table seperated by name, do not search by product
            //if (filter.getProduct() != null) {
            //    predicates.add(cb.equal(root.get("product"), filter.getProduct()));
            //}
            // 2023.02.20-TuPN.Deleted.End

            if (filter.getAccountId()!= null) {
                predicates.add( cb.equal(root.get("accountId"  ), filter.getAccountId() ));
            }

            if( filter.getUsername()!=null ){
                predicates.add( cb.equal(root.get("username"  ), filter.getUsername() ));
            }

            // where in parts
            if( parts != null && !parts.isEmpty() ){
                predicates.add( root.get("partId" ).in(
                        parts.stream().map(
                                Part::getId
                        ).collect(Collectors.toList())
                ));
            }

            if( accounts != null && !accounts.isEmpty() ){
                predicates.add( root.get("accountId" ).in(
                        accounts.stream().map(
                                Account::getId
                        ).collect(Collectors.toList())
                ));
            }

            // where date in range
            if( filter.getDateStart() != null ){
                predicates.add( cb.greaterThanOrEqualTo(root.get("dateId"  ), DateUtils.dateToDateId( filter.getDateStart() ) ));
            }
            if( filter.getDateEnd() != null ){
                predicates.add( cb.lessThanOrEqualTo(root.get("dateId"  ), DateUtils.dateToDateId( filter.getDateEnd() ) ));
            }

            return cb.and(predicates.toArray(new Predicate[0]));
        };
    }
}
