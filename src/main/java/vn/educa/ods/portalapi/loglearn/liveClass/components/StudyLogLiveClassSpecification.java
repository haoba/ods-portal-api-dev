package vn.educa.ods.portalapi.loglearn.liveClass.components;

import lombok.extern.slf4j.Slf4j;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;
import vn.educa.ods.portalapi.lesson.models.LiveClass;
import vn.educa.ods.portalapi.lesson.models.Part;
import vn.educa.ods.portalapi.loglearn.liveClass.models.StudyLogLiveClass;
import vn.educa.ods.portalapi.utils.DateUtils;

import javax.persistence.criteria.Predicate;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Component
@Slf4j
public class StudyLogLiveClassSpecification {

    public static Specification<? extends StudyLogLiveClass> filterBy(
            @NotNull StudyLogLiveClassFilter filter, List<LiveClass> liveClasses, List<Part> parts){
        return (root, query, cb) -> {
            final List<Predicate> predicates = new ArrayList<>();

            // filter by field
            if (filter.getProduct() != null) {
                predicates.add(cb.equal(root.get("product"), filter.getProduct()));
            }

            if (filter.getAccountId() != null) {
                predicates.add(cb.equal(root.get("accountId"), filter.getAccountId()));
            }

            // where in lessons
            final List<String> accountIds = filter.getAccountIds();
            if( accountIds!= null && !accountIds.isEmpty() ){
                predicates.add( root.get("accountId" ).in( accountIds ));
            }

            if( liveClasses!= null && !liveClasses.isEmpty() ){
                predicates.add( root.get("liveClass" ).in( liveClasses ));
            }

            if( parts!= null && !parts.isEmpty() ){
                predicates.add( root.get("part" ).in( parts ));
            }

            // where date in range
            if( filter.getDateStart() != null ){
                predicates.add( cb.greaterThanOrEqualTo( root.get("dateId"), DateUtils.dateToDateId( filter.getDateStart() ) ));
            }
            if( filter.getDateEnd() != null ){
                predicates.add( cb.lessThanOrEqualTo( root.get("dateId"), DateUtils.dateToDateId( filter.getDateEnd() ) ));
            }

            return cb.and(predicates.toArray(new Predicate[0]));
        };
    }
}
