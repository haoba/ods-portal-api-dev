package vn.educa.ods.portalapi.loglearn.regular.requests;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import vn.educa.ods.commons.models.dto.AccountDto;
import vn.educa.ods.portalapi.helpers.PortalApiDesc;

import java.util.Date;

@Setter
@Getter
@Schema( description = "Aggregate number of learn of account-product")
public class AccountNumOfLearnResponse {
    @Schema( description = PortalApiDesc.PRODUCT_CODE_DESC )
    private String product;

    @Schema( description = "Account id with prefix (EDU.xx, KID.xx)")
    private String accountId;

    @Schema( description = "Account detail information")
    private AccountDto account;

    @Schema( description = "Number of learn day in date range filter")
    private int numOfLearnDay;

    @Schema( description = "Min learn date in date range filter")
    private Date minLearnDate;

    @Schema( description = "Max learn date in date range filter")
    private Date maxLearnDate;
}
