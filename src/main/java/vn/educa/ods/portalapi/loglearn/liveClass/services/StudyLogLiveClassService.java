package vn.educa.ods.portalapi.loglearn.liveClass.services;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import vn.educa.ods.portalapi.lesson.models.LiveClass;
import vn.educa.ods.portalapi.lesson.models.Part;
import vn.educa.ods.portalapi.loglearn.liveClass.components.StudyLogLiveClassFilter;
import vn.educa.ods.portalapi.loglearn.liveClass.models.StudyLogLiveClass;

import java.util.List;

public interface StudyLogLiveClassService {
    Page<? extends StudyLogLiveClass> search(StudyLogLiveClassFilter filter,
                                             List<LiveClass> classes,
                                             List<Part> parts, Pageable pageable);
}
