package vn.educa.ods.portalapi.loglearn.regular.services;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import vn.educa.ods.commons.controllers.PageResponse;
import vn.educa.ods.portalapi.loglearn.regular.requests.AccountNumOfLearnResponse;
import vn.educa.ods.portalapi.utils.DateUtils;

import java.sql.ResultSet;
import java.text.ParseException;
import java.util.Date;
import java.util.List;

@Slf4j
public abstract class StudyLogDailyServiceAggregateImpl implements StudyLogDailyService {

    @Autowired
    NamedParameterJdbcTemplate jdbcTemplate;

    public PageResponse<AccountNumOfLearnResponse> aggregateAccountNumberOfLearn(String produceCode, List<String> accountIds, List<Date> inDateRange, Pageable pageable){

        // Selection command
        final StringBuilder sqlDataBuilder = new StringBuilder(
                "select " +
                "    USER_ID " +
                "  , PRODUCT " +
                "  , count(distinct DATE_ID) NUM_OF_LEARN_DAY " +
                "  , min(DATE_ID)                MIN_LEARN_DATE " +
                "  , max(DATE_ID)                MAX_LEARN_DATE " +
                "from" +
                "    ODS_ETL.STUDY_LOG_DAILY " +
                "where (1=1) "
        );

        // Where command
        if( produceCode!=null && !produceCode.isEmpty() ){
            sqlDataBuilder
                    .append( " and (")
                    .append(" PRODUCT = '").append( produceCode ).append("'")
                    .append(")");
        }

        if( accountIds !=null && !accountIds.isEmpty() ){
            sqlDataBuilder.append(" and( USER_ID in ( :accountIds  ))");
        }

        if( !inDateRange.isEmpty() ){
            sqlDataBuilder
                    .append(" and(")
                    .append(" DATE_ID >= ").append(DateUtils.dateToDateId(inDateRange.get(0)))
                    .append(" and ")
                    .append(" DATE_ID <= ").append(DateUtils.dateToDateId(inDateRange.get(1)))
                    .append(") ");
        }

        // group by sql
        sqlDataBuilder.append(" group by PRODUCT, USER_ID ");

        // pagination
        String sqlPaginationBuilder =
                "offset :offset rows " +
                "fetch next :pageSize rows only";

        // Step1: Calculate pagination
        String sqlCount = new StringBuilder()
                .append("SELECT count(1) TOTAL from (")
                .append( sqlDataBuilder )
                .append( ")" )
                .toString();
        List<Long> nTotalElements = jdbcTemplate.query(
                sqlCount,
                new MapSqlParameterSource("accountIds", accountIds),
                (rs,num)->{
                    return rs.getLong("TOTAL");
                }
        );

        // Step2: Query data
        final String sqlData = new StringBuilder()
                .append( sqlDataBuilder )
                .append(sqlPaginationBuilder)
                .toString();


        List<AccountNumOfLearnResponse> agg = jdbcTemplate.query(
                sqlData,
                new MapSqlParameterSource()
                        .addValue("accountIds", accountIds )
                        .addValue("offset",     pageable.getOffset())
                        .addValue("pageSize",   pageable.getPageSize()),
                (rs, num) ->{
                    AccountNumOfLearnResponse ret = new AccountNumOfLearnResponse();

                    ret.setProduct( produceCode );
                    ret.setAccountId( rs.getString("USER_ID"));
                    ret.setNumOfLearnDay( rs.getInt("NUM_OF_LEARN_DAY"));
                    try {
                        ret.setMinLearnDate( DateUtils.dateIdToDate( rs.getInt("MIN_LEARN_DATE")));
                        ret.setMaxLearnDate( DateUtils.dateIdToDate( rs.getInt("MAX_LEARN_DATE")));
                    } catch (ParseException e) {
                        e.printStackTrace();
                        log.error("ERROR: Cannot parse date");
                    }

                    return ret;
                }
        );

        // Pagination result
        PageResponse<AccountNumOfLearnResponse> pagination = new PageResponse<>();

        pagination.setElements( agg );
        pagination.setTotalElements( nTotalElements.get(0) );
        pagination.setTotalPages( nTotalElements.get(0).intValue() / pageable.getPageSize() + 1 );
        pagination.setPageIndex( pageable.getPageNumber() );
        pagination.setPageSize( pageable.getPageSize() );

        return pagination;
    }

    private AccountNumOfLearnResponse mapQueryResult2AccountNumOfLearnResponse( ResultSet rs ){
        return null;
    }
}
