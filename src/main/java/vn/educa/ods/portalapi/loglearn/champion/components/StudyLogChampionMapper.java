package vn.educa.ods.portalapi.loglearn.champion.components;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import vn.educa.ods.commons.models.dto.loglearn.StudyLogChampionDto;
import vn.educa.ods.portalapi.loglearn.champion.models.StudyLogChampion;

@Mapper
public interface StudyLogChampionMapper {
    StudyLogChampionMapper INSTANCE = Mappers.getMapper( StudyLogChampionMapper.class );

    StudyLogChampionDto entity2dto(StudyLogChampion entity);
    StudyLogChampion       dto2entity(StudyLogChampionDto dto);
    StudyLogChampion       clone(StudyLogChampion entity);
}
