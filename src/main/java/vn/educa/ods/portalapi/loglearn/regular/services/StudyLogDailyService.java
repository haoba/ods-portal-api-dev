package vn.educa.ods.portalapi.loglearn.regular.services;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import vn.educa.ods.commons.controllers.PageResponse;
import vn.educa.ods.portalapi.account.models.Account;
import vn.educa.ods.portalapi.lesson.models.Lesson;
import vn.educa.ods.portalapi.loglearn.regular.components.StudyLogDailyFilter;
import vn.educa.ods.portalapi.loglearn.regular.models.StudyLogDaily;
import vn.educa.ods.portalapi.loglearn.regular.requests.AccountNumOfLearnResponse;

import java.util.Date;
import java.util.List;

public interface StudyLogDailyService {
    Page<StudyLogDaily> searchByAccountLessonAndFilter(List<Account> accounts, List<Lesson> lessons, StudyLogDailyFilter filter, Pageable pageable);

    Page<StudyLogDaily> searchByAccountList(String produceCode, List<String> accountIds, List<Date> inDateRange, Pageable pageable);

    PageResponse<AccountNumOfLearnResponse> aggregateAccountNumberOfLearn(String produceCode, List<String> accountIds, List<Date> inDateRange, Pageable pageable);
}
