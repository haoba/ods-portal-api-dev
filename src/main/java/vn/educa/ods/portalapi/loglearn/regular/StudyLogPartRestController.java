package vn.educa.ods.portalapi.loglearn.regular;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.*;
import vn.educa.ods.commons.controllers.BaseRestController;
import vn.educa.ods.commons.controllers.PageResponse;
import vn.educa.ods.commons.models.dto.loglearn.StudyLogPartDto;
import vn.educa.ods.portalapi.account.components.AccountRepository;
import vn.educa.ods.portalapi.account.models.Account;
import vn.educa.ods.portalapi.helpers.PortalApiDesc;
import vn.educa.ods.portalapi.lesson.components.PartRepository;
import vn.educa.ods.portalapi.lesson.models.Part;
import vn.educa.ods.portalapi.loglearn.regular.components.LogLearnMapper;
import vn.educa.ods.portalapi.loglearn.regular.components.StudyLogPartFilter;
import vn.educa.ods.portalapi.loglearn.regular.models.StudyLogPart;
import vn.educa.ods.portalapi.loglearn.regular.services.StudyLogDailyService;
import vn.educa.ods.portalapi.loglearn.regular.services.StudyLogPartService;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping( value = {
        "api/v1/log-learn/regular-class/lesson-part-log",
        "api/v1/log-learn/regular-class/lesson-game-log"
    }, produces = "application/json")
@Tag(name = PortalApiDesc.LOG_LEARN_API, description = PortalApiDesc.LOG_LEARN_API_DESC)
public class StudyLogPartRestController extends BaseRestController {

    @Autowired
    StudyLogDailyService studyLogDailyService;

    @Autowired
    StudyLogPartService studyLogPartService;

    @Autowired
    PartRepository partRepository;

    @Autowired
    AccountRepository accountRepository;

    /**
     * Search Study log of student
     * @param filter filter by
     * @param pageIndex page index
     * @param pageSize page size, default 50
     * @param orderBy order by data, default date
     * @param desc true for order desc/ otherwise order desc
     * @return pagination of StudyLogAverageDto
     */
    @PostMapping("search")
    @Operation(
            summary = "Search log learn/study log of a student by Part/Game",
            description = "Search by Product, AccountId, Username, Phone, lesson and in a date range"
    )
    public PageResponse<StudyLogPartDto> search(@RequestBody @Valid StudyLogPartFilter filter,
                                                @RequestParam( required = false) Integer pageIndex,
                                                @RequestParam( required = false) Integer pageSize,
                                                @RequestParam( required = false, defaultValue = "date") String orderBy,
                                                @RequestParam( required = false, defaultValue = "true") Boolean desc
            ){

        // search by lesson
        List<Part> parts = new ArrayList<>();

        if( filter.getPartCode()!=null ){
            parts.addAll(
                    partRepository.findByCode( filter.getPartCode() )
            );
            if( parts.isEmpty() ){
                return new PageResponse.Builder<StudyLogPartDto>(StatusCode.SUCCESS, "Not found any part by code").build();
            }
        }

        if( filter.getPartName() !=null ){
            parts.addAll(
                    partRepository.findByNameContains( filter.getPartName() )
            );
            if( parts.isEmpty() ){
                return new PageResponse.Builder<StudyLogPartDto>(StatusCode.SUCCESS, "Not found any part by name").build();
            }
        }


        // search by phone
        List<Account> accounts = new ArrayList<>();
        if( filter.getPhone() !=null ){
            String phone = filter.getPhone();
            if( !filter.getPhone().startsWith("0") ){
                phone = "0" + phone;
            }
            accounts.addAll(
                    accountRepository.findByPhone( phone  )
            );

            if( accounts.isEmpty() ){
                return new PageResponse.Builder<StudyLogPartDto>(StatusCode.SUCCESS, "Not found user by phone").build();
            }
        }

        final Sort sort;
        if( orderBy.compareToIgnoreCase("date")==0 ){
            sort = Sort.by("dateId");
        }
        else{
            sort = Sort.by( orderBy );
        }

        final Pageable pageable;
        if( desc ) {
            pageable = pageRequest(pageIndex, pageSize, sort.descending());
        }
        else{
            pageable = pageRequest(pageIndex, pageSize, sort );
        }

        Page<?> studyLogParts = studyLogPartService.searchByAccountPartAndFilter(
                accounts, parts, filter, pageable
        );

        if( studyLogParts.isEmpty() ){
            return new PageResponse.Builder<StudyLogPartDto>(StatusCode.SUCCESS, "Not found any study log.").build();
        }

        List<StudyLogPartDto> logLearn = studyLogParts.stream().map(
                x -> LogLearnMapper.INSTANCE.entity2dto( (StudyLogPart)x)
        ).collect(Collectors.toList());

        return new PageResponse.Builder<>(SUCCESS, logLearn, studyLogParts ).build();
    }
}
