package vn.educa.ods.portalapi.loglearn.category.components;

import lombok.extern.slf4j.Slf4j;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;
import vn.educa.ods.portalapi.lesson.models.Unit;
import vn.educa.ods.portalapi.loglearn.category.models.StudyLogCategory;
import vn.educa.ods.portalapi.utils.DateUtils;

import javax.persistence.criteria.Predicate;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Component
@Slf4j
public class StudyLogCategorySpecification {

    public static Specification<StudyLogCategory> filterBy(@NotNull StudyLogCategoryFilter filter, List<Unit> units ){
        return (root, query, cb) -> {
            final List<Predicate> predicates = new ArrayList<>();

            // filter by field
            if (filter.getProduct() != null) {
                predicates.add(cb.equal(root.get("product"), filter.getProduct()));
            }

            if (filter.getAccountId() != null) {
                predicates.add(cb.equal(root.get("accountId"), filter.getAccountId()));
            }

            // where in units
            final List<String> accountIds = filter.getAccountIds();
            if( accountIds!= null && !accountIds.isEmpty() ){
                predicates.add( root.get("accountId" ).in( accountIds ));
            }

            // where in units
            if( units!= null && !units.isEmpty() ){
                predicates.add( root.get("unit" ).in( units ));
            }

            // where date in range
            if( filter.getDateStart() != null ){
                predicates.add( cb.greaterThanOrEqualTo( root.get("dateId"), DateUtils.dateToDateId( filter.getDateStart() ) ));
            }
            if( filter.getDateEnd() != null ){
                predicates.add( cb.lessThanOrEqualTo( root.get("dateId"), DateUtils.dateToDateId( filter.getDateEnd() ) ));
            }

            return cb.and(predicates.toArray(new Predicate[0]));
        };
    }
}
