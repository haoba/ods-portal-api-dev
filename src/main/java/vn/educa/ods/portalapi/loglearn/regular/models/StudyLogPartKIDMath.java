package vn.educa.ods.portalapi.loglearn.regular.models;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Setter @Getter
@Table( name = "study_log_platfroms_kid_math")
public class StudyLogPartKIDMath extends StudyLogPart {
}
