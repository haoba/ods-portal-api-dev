package vn.educa.ods.portalapi.loglearn.liveClass.components;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;
import vn.educa.ods.portalapi.loglearn.liveClass.models.StudyLogLiveClassTH;

@Repository
public interface StudyLogLiveClassTHRepository extends JpaRepository<StudyLogLiveClassTH,String>,
        JpaSpecificationExecutor<StudyLogLiveClassTH> {
}
