package vn.educa.ods.portalapi.loglearn.category.models;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import vn.educa.ods.commons.models.BaseEntity;
import vn.educa.ods.portalapi.lesson.models.Step;
import vn.educa.ods.portalapi.lesson.models.Unit;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.math.BigDecimal;
import java.util.Date;

@Setter @Getter
@Entity( name = "study_log_category_kid")
public class StudyLogCategory extends BaseEntity {

    @Id
    protected String id;

    @Column( name = "userId")
    private String accountId;

    private String username;

    private Integer dateId;

    @Column( name = "time")
    private Date date;

    @ManyToOne
    @NotFound( action = NotFoundAction.IGNORE )
    private Step step;

    private BigDecimal stepPoint;

    @ManyToOne
    @NotFound( action = NotFoundAction.IGNORE )
    private Unit unit;

    private String product;
}
