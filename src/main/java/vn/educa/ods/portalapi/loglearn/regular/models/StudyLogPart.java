package vn.educa.ods.portalapi.loglearn.regular.models;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import vn.educa.ods.commons.models.BaseEntity;
import vn.educa.ods.portalapi.lesson.models.Lesson;
import vn.educa.ods.portalapi.lesson.models.Part;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import java.math.BigDecimal;
import java.util.Date;

@Setter @Getter
@MappedSuperclass
public class StudyLogPart extends BaseEntity {

    @Id
    protected String id;

    @Column( name = "userId")
    private String accountId;

    private String username;

    private Integer dateId;

    @Column( name = "\"DATE\"")
    private Date date;

    @ManyToOne
    @NotFound(action = NotFoundAction.IGNORE)
    private Lesson lesson;

    @ManyToOne
    @NotFound(action = NotFoundAction.IGNORE)
    private Part part;

    private BigDecimal partPoint;

    private String product;
}
