package vn.educa.ods.portalapi.loglearn.regular.components;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;

@Setter @Getter
@Schema( description = "Log Learn Filter Conditional")
public class StudyLogDailyFilter extends LogLearnBaseFilter {
    @Schema( description = "lesson-code by upper case")
    private String lessonCode;

    @Schema( description = "lesson name, search like %string%")
    private String lessonName;
}
