package vn.educa.ods.portalapi.loglearn.regular.services;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import vn.educa.ods.portalapi.account.models.Account;
import vn.educa.ods.portalapi.lesson.models.Part;
import vn.educa.ods.portalapi.loglearn.regular.components.StudyLogPartFilter;
import vn.educa.ods.portalapi.loglearn.regular.models.StudyLogPart;

import java.util.List;

public interface StudyLogPartService {
    Page<? extends StudyLogPart> searchByAccountPartAndFilter(List<Account> accounts, List<Part> parts, StudyLogPartFilter filter, Pageable pageable);
}
