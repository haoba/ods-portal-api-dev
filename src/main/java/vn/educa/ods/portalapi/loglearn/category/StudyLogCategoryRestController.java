package vn.educa.ods.portalapi.loglearn.category;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.*;
import vn.educa.ods.commons.controllers.BaseRestController;
import vn.educa.ods.commons.controllers.PageResponse;
import vn.educa.ods.commons.models.dto.loglearn.StudyLogCategoryDto;
import vn.educa.ods.portalapi.exceptions.NotSupportedPortalApi;
import vn.educa.ods.portalapi.helpers.PortalApiDesc;
import vn.educa.ods.portalapi.lesson.components.UnitRepository;
import vn.educa.ods.portalapi.lesson.models.Unit;
import vn.educa.ods.portalapi.loglearn.category.components.StudyLogCategoryFilter;
import vn.educa.ods.portalapi.loglearn.category.components.StudyLogCategoryMapper;
import vn.educa.ods.portalapi.loglearn.category.components.StudyLogCategoryRepository;
import vn.educa.ods.portalapi.loglearn.category.components.StudyLogCategorySpecification;
import vn.educa.ods.portalapi.loglearn.category.models.StudyLogCategory;
import vn.educa.ods.portalapi.utils.ProductUtils;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping( value = "api/v1/log-learn/category", produces = "application/json")
@Tag(name = PortalApiDesc.LOG_LEARN_API, description = PortalApiDesc.LOG_LEARN_API_DESC)
public class StudyLogCategoryRestController extends BaseRestController {

    @Autowired
    private StudyLogCategoryRepository categoryRepository;

    @Autowired
    private UnitRepository unitRepository;

    /**
     * Search Study log of student
     * @param filter filter by
     * @param pageIndex page index
     * @param pageSize page size, default 50
     * @param orderBy order by data, default date
     * @param desc true for order desc/ otherwise order desc
     * @return pagination of StudyLogAverageDto
     */
    @PostMapping("search")
    @Operation(
            summary = "Search log category",
            description = "Search by Product, AccountId, Parts and in a date range"
    )
    public PageResponse<StudyLogCategoryDto> search(@RequestBody @Valid StudyLogCategoryFilter filter,
                                                    @RequestParam( required = false) Integer pageIndex,
                                                    @RequestParam( required = false) Integer pageSize,
                                                    @RequestParam( required = false, defaultValue = "date") String orderBy,
                                                    @RequestParam( required = false, defaultValue = "true") Boolean desc
            ){

        final Sort sort;
        if( orderBy.compareToIgnoreCase("date")==0 ){
            sort = Sort.by("dateId");
        }
        else{
            sort = Sort.by( orderBy );
        }

        // just support Champion for EDU.TH
        if( !ProductUtils.isEduKID( filter.getProduct() )){
            throw new NotSupportedPortalApi("Category Log learn not support for product:" + filter.getProduct() );
        }

        final Pageable pageable;
        if( desc ) {
            pageable = pageRequest(pageIndex, pageSize, sort.descending());
        }
        else{
            pageable = pageRequest(pageIndex, pageSize, sort );
        }

        // get all lesson
        List<Unit> units = null;
        if( filter.getUnitIds()!=null && !filter.getUnitIds().isEmpty()){
            units = unitRepository.findAllByIdIn( filter.getUnitIds() );
            if( units.isEmpty() ){
                return new PageResponse.Builder<StudyLogCategoryDto>(StatusCode.SUCCESS,
                        "Not found any study log with unit:" + filter.getUnitIds() )
                        .build();
            }
        }

        Page<StudyLogCategory> categoryPage = categoryRepository.findAll(
                StudyLogCategorySpecification.filterBy(filter, units), pageable
        );

        if( categoryPage.isEmpty() ){
            return new PageResponse.Builder<StudyLogCategoryDto>(StatusCode.SUCCESS, "Not found any study log").build();
        }

        List<StudyLogCategoryDto> logLearn = categoryPage.stream().map(
                StudyLogCategoryMapper.INSTANCE::entity2dto
        ).collect(Collectors.toList());

        return new PageResponse.Builder<>(SUCCESS, logLearn, categoryPage ).build();
    }
}
