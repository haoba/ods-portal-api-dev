package vn.educa.ods.portalapi.loglearn.regular.components;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import vn.educa.ods.portalapi.helpers.PortalApiDesc;

import javax.validation.constraints.NotEmpty;

@Setter @Getter
@Schema( description = "Log Learn Filter Conditional")
public class StudyLogPartFilter extends LogLearnBaseFilter {
    @Schema( description = "lesson part/game code by upper case")
    private String partCode;

    @Schema( description = "lesson part/game name, search like %string%")
    private String partName;

    @Schema( description = PortalApiDesc.PRODUCT_CODE_DESC )
    @NotEmpty( message = "Product is required")
    private String product;
}
