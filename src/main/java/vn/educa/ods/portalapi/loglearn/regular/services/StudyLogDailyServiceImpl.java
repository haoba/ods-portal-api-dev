package vn.educa.ods.portalapi.loglearn.regular.services;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import vn.educa.ods.portalapi.account.models.Account;
import vn.educa.ods.portalapi.lesson.models.Lesson;
import vn.educa.ods.portalapi.loglearn.regular.components.StudyLogDailyFilter;
import vn.educa.ods.portalapi.loglearn.regular.components.StudyLogDailyRepository;
import vn.educa.ods.portalapi.loglearn.regular.components.StudyLogDailySpecification;
import vn.educa.ods.portalapi.loglearn.regular.models.StudyLogDaily;

import java.util.Date;
import java.util.List;

@Service
@Slf4j
public class StudyLogDailyServiceImpl extends StudyLogDailyServiceAggregateImpl {
    @Autowired
    StudyLogDailyRepository studyLogDailyRepository;

    @Override
    //@Cacheable(value = "log-learn-daily")
    public Page<StudyLogDaily> searchByAccountLessonAndFilter(List<Account> accounts, List<Lesson> lessons, StudyLogDailyFilter filter, Pageable pageable) {

        log.info("Invalidate log-learn cache: Load data without cache");

        return studyLogDailyRepository.findAll(
                StudyLogDailySpecification.filterBy( filter, accounts, lessons ), pageable
        );
    }

    @Override
    public Page<StudyLogDaily> searchByAccountList(String produceCode, List<String> accountIds, List<Date> inDateRanges, Pageable pageable) {
        log.info("Invalidate log-learn cache: Load data without cache");

        return studyLogDailyRepository.findAll(
                StudyLogDailySpecification.filterInList( produceCode, accountIds, inDateRanges ),
                pageable
        );
    }
}
