package vn.educa.ods.portalapi.loglearn.champion;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.*;
import vn.educa.ods.commons.controllers.BaseRestController;
import vn.educa.ods.commons.controllers.PageResponse;
import vn.educa.ods.commons.models.dto.loglearn.StudyLogChampionDto;
import vn.educa.ods.portalapi.exceptions.NotSupportedPortalApi;
import vn.educa.ods.portalapi.helpers.PortalApiDesc;
import vn.educa.ods.portalapi.lesson.components.LessonRepository;
import vn.educa.ods.portalapi.lesson.models.Lesson;
import vn.educa.ods.portalapi.loglearn.champion.components.StudyLogChampionFilter;
import vn.educa.ods.portalapi.loglearn.champion.components.StudyLogChampionMapper;
import vn.educa.ods.portalapi.loglearn.champion.components.StudyLogChampionRepository;
import vn.educa.ods.portalapi.loglearn.champion.components.StudyLogChampionSpecification;
import vn.educa.ods.portalapi.loglearn.champion.models.StudyLogChampion;
import vn.educa.ods.portalapi.utils.ProductUtils;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping( value = "api/v1/log-learn/champion-league", produces = "application/json")
@Tag(name = PortalApiDesc.LOG_LEARN_API, description = PortalApiDesc.LOG_LEARN_API_DESC)
public class StudyLogChampionRestController extends BaseRestController {

    @Autowired
    private StudyLogChampionRepository championRepository;

    @Autowired
    private LessonRepository lessonRepository;

    /**
     * Search Study log of student
     * @param filter filter by
     * @param pageIndex page index
     * @param pageSize page size, default 50
     * @param orderBy order by data, default date
     * @param desc true for order desc/ otherwise order desc
     * @return pagination of StudyLogAverageDto
     */
    @PostMapping("search")
    @Operation(
            summary = "Search log champion league",
            description = "Search by Product, AccountId, Parts and in a date range"
    )
    public PageResponse<StudyLogChampionDto> search(@RequestBody @Valid StudyLogChampionFilter filter,
                                                    @RequestParam( required = false) Integer pageIndex,
                                                    @RequestParam( required = false) Integer pageSize,
                                                    @RequestParam( required = false, defaultValue = "date") String orderBy,
                                                    @RequestParam( required = false, defaultValue = "true") Boolean desc
            ){

        final Sort sort;
        if( orderBy.compareToIgnoreCase("date")==0 ){
            sort = Sort.by("dateId");
        }
        else{
            sort = Sort.by( orderBy );
        }

        // just support Champion for EDU.TH
        if( !ProductUtils.isEduTH( filter.getProduct() )){
            throw new NotSupportedPortalApi("Champion League not support for product:" + filter.getProduct() );
        }

        final Pageable pageable;
        if( desc ) {
            pageable = pageRequest(pageIndex, pageSize, sort.descending());
        }
        else{
            pageable = pageRequest(pageIndex, pageSize, sort );
        }

        // get all lesson
        List<Lesson> lessons = null;
        if( filter.getLessonIds()!=null && !filter.getLessonIds().isEmpty()){
            lessons = lessonRepository.findAllByIdIn( filter.getLessonIds() );
            if( lessons.isEmpty() ){
                return new PageResponse.Builder<StudyLogChampionDto>(StatusCode.SUCCESS,
                        "Not found any study log with lesson:" + filter.getLessonIds() )
                        .build();
            }
        }

        Page<StudyLogChampion> championPage = championRepository.findAll(
                StudyLogChampionSpecification.filterBy(filter, lessons), pageable
        );

        if( championPage.isEmpty() ){
            return new PageResponse.Builder<StudyLogChampionDto>(StatusCode.SUCCESS, "Not found any study log").build();
        }

        List<StudyLogChampionDto> logLearn = championPage.stream().map(
                StudyLogChampionMapper.INSTANCE::entity2dto
        ).collect(Collectors.toList());

        return new PageResponse.Builder<>(SUCCESS, logLearn, championPage ).build();
    }
}
