package vn.educa.ods.portalapi.loglearn.liveClass.models;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;

@Setter @Getter
@Entity( name = "study_log_live_class_kid")
public class StudyLogLiveClassKID extends StudyLogLiveClass {
}
