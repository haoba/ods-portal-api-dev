package vn.educa.ods.portalapi.loglearn.regular.components;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;
import vn.educa.ods.portalapi.loglearn.regular.models.StudyLogPartEduMath;

@Repository
public interface StudyLogPartEduMathRepository extends JpaRepository<StudyLogPartEduMath,String>, JpaSpecificationExecutor<StudyLogPartEduMath> {
}
