package vn.educa.ods.portalapi.loglearn.regular.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import vn.educa.ods.portalapi.account.models.Account;
import vn.educa.ods.portalapi.lesson.models.Part;
import vn.educa.ods.portalapi.loglearn.regular.components.*;
import vn.educa.ods.portalapi.loglearn.regular.models.*;
import vn.educa.ods.portalapi.utils.ProductUtils;

import java.util.ArrayList;
import java.util.List;

@Service
public class StudyLogPartServiceImpl implements StudyLogPartService {

    final private StudyLogPartTHCSRepository thcsRepository;
    final private StudyLogPartTHRepository thRepository;
    final private StudyLogPartTH2Repository th2Repository;
    final private StudyLogPartKIDRepository kidRepository;
    final private StudyLogPartKIDMathRepository kidMathRepository;
    final private StudyLogPartEduMathRepository eduMathRepository;

    @Autowired
    public StudyLogPartServiceImpl(StudyLogPartTHCSRepository thcsRepository,
                                   StudyLogPartTHRepository thRepository,
                                   StudyLogPartTH2Repository th2Repository,
                                   StudyLogPartKIDRepository kidRepository,
                                   StudyLogPartKIDMathRepository kidMathRepository,
                                   StudyLogPartEduMathRepository eduMathRepository) {
        this.thcsRepository = thcsRepository;
        this.thRepository = thRepository;
        this.th2Repository = th2Repository;
        this.kidRepository = kidRepository;
        this.kidMathRepository = kidMathRepository;
        this.eduMathRepository = eduMathRepository;
    }

    @Override
    public Page<? extends StudyLogPart> searchByAccountPartAndFilter(List<Account> accounts, List<Part> parts, StudyLogPartFilter filter, Pageable pageable) {

        if( ProductUtils.isEduTH( filter.getProduct() ) ){
            Page<? extends StudyLogPart> logV2 = thRepository.findAll(
                    (Specification<StudyLogPartTH>) StudyLogPartSpecification.filterBy( filter, accounts, parts ), pageable
            );

            Page<? extends StudyLogPart> logV3 = th2Repository.findAll(
                    (Specification<StudyLogPartTH2>)StudyLogPartSpecification.filterBy( filter, accounts, parts ), pageable
            );

            List<StudyLogPart> itemV2 = (List<StudyLogPart>) logV2.toList();
            List<StudyLogPart> itemV3 = (List<StudyLogPart>) logV3.toList();
            List<StudyLogPart> itemTotal = new ArrayList<>();

            itemTotal.addAll( itemV2 );
            itemTotal.addAll( itemV3 );

            Page<? extends StudyLogPart> logAll = new PageImpl<StudyLogPart>(
                    itemTotal, pageable, logV2.getTotalElements() + logV3.getTotalElements()
            );
            return  logAll;
        }
        else if(  ProductUtils.isEduTHCS( filter.getProduct() )){
            return   thcsRepository.findAll(
                    (Specification<StudyLogPartTHCS>)StudyLogPartSpecification.filterBy( filter, accounts, parts ), pageable
            );
        }
        else if(ProductUtils.isEduMath( filter.getProduct() )){
                return   eduMathRepository.findAll(
                        (Specification<StudyLogPartEduMath>)StudyLogPartSpecification.filterBy( filter, accounts, parts ), pageable
                );
        }
        else if(  filter.getProduct().equalsIgnoreCase("EDU.KID") ){
            Page<? extends StudyLogPart> kid =  kidRepository.findAll(
                    (Specification<StudyLogPartKID>)StudyLogPartSpecification.filterBy( filter, accounts, parts ), pageable
            );

            Page<? extends StudyLogPart> kidMath =  kidMathRepository.findAll(
                    (Specification<StudyLogPartKIDMath>)StudyLogPartSpecification.filterBy( filter, accounts, parts ), pageable
            );

            List<StudyLogPart> itemKid     = (List<StudyLogPart>) kid.toList();
            List<StudyLogPart> itemKidMath = (List<StudyLogPart>) kidMath.toList();
            List<StudyLogPart> itemTotal = new ArrayList<>();

            itemTotal.addAll( itemKid );
            itemTotal.addAll( itemKidMath );

            Page<? extends StudyLogPart> logAll = new PageImpl<>(
                    itemTotal, pageable, kid.getTotalElements() + kidMath.getTotalElements()
            );
            return  logAll;
        }
        return Page.empty();
    }

}
