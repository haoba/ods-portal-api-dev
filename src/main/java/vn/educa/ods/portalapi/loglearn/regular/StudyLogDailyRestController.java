package vn.educa.ods.portalapi.loglearn.regular;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.*;
import vn.educa.ods.commons.controllers.BaseRestController;
import vn.educa.ods.commons.controllers.PageResponse;
import vn.educa.ods.commons.models.dto.AccountDto;
import vn.educa.ods.commons.models.dto.loglearn.StudyLogAverageDto;
import vn.educa.ods.portalapi.account.components.AccountMapper;
import vn.educa.ods.portalapi.account.components.AccountRepository;
import vn.educa.ods.portalapi.account.models.Account;
import vn.educa.ods.portalapi.exceptions.NotSupportedPortalApi;
import vn.educa.ods.portalapi.helpers.PortalApiDesc;
import vn.educa.ods.portalapi.lesson.components.LessonRepository;
import vn.educa.ods.portalapi.lesson.models.Lesson;
import vn.educa.ods.portalapi.loglearn.regular.components.LogLearnMapper;
import vn.educa.ods.portalapi.loglearn.regular.components.StudyLogDailyByListFilter;
import vn.educa.ods.portalapi.loglearn.regular.components.StudyLogDailyFilter;
import vn.educa.ods.portalapi.loglearn.regular.models.StudyLogDaily;
import vn.educa.ods.portalapi.loglearn.regular.requests.AccountNumOfLearnResponse;
import vn.educa.ods.portalapi.loglearn.regular.services.StudyLogDailyService;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping( value = "api/v1/log-learn/regular-class/daily-log", produces = "application/json")
@Tag(name = PortalApiDesc.LOG_LEARN_API, description = PortalApiDesc.LOG_LEARN_API_DESC)
public class StudyLogDailyRestController extends BaseRestController {

    @Autowired
    StudyLogDailyService studyLogDailyService;

    @Autowired
    LessonRepository lessonRepository;

    @Autowired
    AccountRepository accountRepository;

    /**
     * Search Study log of student
     * @param filter filter by
     * @param pageIndex page index
     * @param pageSize page size, default 50
     * @param orderBy order by data, default date
     * @param desc true for order desc/ otherwise order desc
     * @return pagination of StudyLogAverageDto
     */
    @PostMapping("search-daily")
    @Operation(
            summary = "Search log learn/study log of a student",
            description = "Search by Product, AccountId, Username, Phone, lesson and in a date range"
    )
    public PageResponse<StudyLogAverageDto> search(@RequestBody @Valid StudyLogDailyFilter filter,
                                                   @RequestParam( required = false) Integer pageIndex,
                                                   @RequestParam( required = false) Integer pageSize,
                                                   @RequestParam( required = false, defaultValue = "date") String orderBy,
                                                   @RequestParam( required = false, defaultValue = "true") Boolean desc
            ){

        // search by lesson
        List<Lesson> lessons = new ArrayList<>();

        if( filter.getLessonCode()!=null ){
            lessons.addAll(
                    lessonRepository.findByCode( filter.getLessonCode() )
            );
            if( lessons.isEmpty() ){
                return new PageResponse.Builder<StudyLogAverageDto>(StatusCode.SUCCESS, "Not found user by lessons").build();
            }
        }

        if( filter.getLessonName() !=null ){
            lessons.addAll(
                    lessonRepository.findByNameContains( filter.getLessonName() )
            );
            if( lessons.isEmpty() ){
                return new PageResponse.Builder<StudyLogAverageDto>(StatusCode.SUCCESS, "Not found user by lessons").build();
            }
        }


        // search by phone
        List<Account> accounts = new ArrayList<>();
        if( filter.getPhone() !=null ){
            String phone = filter.getPhone();
            if( !filter.getPhone().startsWith("0") ){
                phone = "0" + phone;
            }
            accounts.addAll(
                    accountRepository.findByPhone( phone  )
            );

            if( accounts.isEmpty() ){
                return new PageResponse.Builder<StudyLogAverageDto>(StatusCode.SUCCESS, "Not found user by phone").build();
            }
        }

        final Sort sort;
        if( orderBy.compareToIgnoreCase("date")==0 ){
            sort = Sort.by("dateId");
        }
        else{
            sort = Sort.by( orderBy );
        }

        final Pageable pageable;
        if( desc ) {
            pageable = pageRequest(pageIndex, pageSize, sort.descending());
        }
        else{
            pageable = pageRequest(pageIndex, pageSize, sort );
        }

        Page<StudyLogDaily> studyLogDaily = studyLogDailyService.searchByAccountLessonAndFilter(
                accounts, lessons, filter, pageable
        );

        if( studyLogDaily.isEmpty() ){
            return new PageResponse.Builder<StudyLogAverageDto>(StatusCode.SUCCESS, "Not found any study log").build();
        }

        List<StudyLogAverageDto> logLearn = studyLogDaily.stream().map(
                LogLearnMapper.INSTANCE::entity2dto
        ).collect(Collectors.toList());

        return new PageResponse.Builder<>(SUCCESS, logLearn, studyLogDaily ).build();
    }

    /**
     * Search Study log of student
     * @param filter filter by
     * @param pageIndex page index
     * @param pageSize page size, default 50
     * @param orderBy order by data, default date
     * @param desc true for order desc/ otherwise order desc
     * @return pagination of StudyLogAverageDto
     */
    @PostMapping("search-daily-in-list")
    @Operation(
            summary = "Search log learn/study log of a list student and product",
            description = "Search by Product, AccountId in a date range"
    )
    public PageResponse<StudyLogAverageDto> searchByList(@RequestBody @Valid StudyLogDailyByListFilter filter,
                                                   @RequestParam( required = false) Integer pageIndex,
                                                   @RequestParam( required = false) Integer pageSize,
                                                   @RequestParam( required = false, defaultValue = "date") String orderBy,
                                                   @RequestParam( required = false, defaultValue = "true") Boolean desc
    ){

        // search by lesson
        List<Lesson> lessons = new ArrayList<>();


        final Sort sort;
        if( orderBy.compareToIgnoreCase("date")==0 ){
            sort = Sort.by("dateId");
        }
        else{
            sort = Sort.by( orderBy );
        }

        final Pageable pageable;
        if( desc ) {
            pageable = pageRequest(pageIndex, pageSize, sort.descending());
        }
        else{
            pageable = pageRequest(pageIndex, pageSize, sort );
        }

        Page<StudyLogDaily> studyLogDaily = studyLogDailyService.searchByAccountList(
                filter.getProduct(), filter.getAccountIds(),
                Arrays.asList( filter.getDateStart(), filter.getDateEnd() ),
                pageable
        );

        if( studyLogDaily.isEmpty() ){
            return new PageResponse.Builder<StudyLogAverageDto>(StatusCode.SUCCESS, "Not found any study log").build();
        }

        List<StudyLogAverageDto> logLearn = studyLogDaily.stream().map(
                LogLearnMapper.INSTANCE::entity2dto
        ).collect(Collectors.toList());

        return new PageResponse.Builder<>(SUCCESS, logLearn, studyLogDaily ).build();
    }

    @PostMapping("search-part-detail")
    @Operation(
            summary = "Search log learn/study log of a student (search by part)",
            description = "Search by Product, AccountId, Username, Phone, lesson and in a date range",
            hidden = true
    )
    public PageResponse<StudyLogAverageDto> searchPart(@RequestBody @Valid StudyLogDailyFilter filter,
                                                       @RequestParam( required = false) Integer pageIndex,
                                                       @RequestParam( required = false) Integer pageSize,
                                                       @RequestParam( required = false, defaultValue = "date") String orderBy,
                                                       @RequestParam( required = false, defaultValue = "true") Boolean desc
    ) {
        throw new NotSupportedPortalApi("Coming soon!!! Please confirm admin!!!");
    }

    /**
     * Aggregate account lgon learn
     * @param filter filter by
     * @param pageIndex page index
     * @param pageSize page size, default 50
     * @param orderBy order by data, default date
     * @param desc true for order desc/ otherwise order desc
     * @return pagination of {@link AccountNumOfLearnResponse}
     */
    @PostMapping("aggregate/num-of-learn")
    @Operation(
            summary = "Aggregate number of log learn by account",
            description = "Tổng hợp số buổi học theo user"
    )
    public PageResponse<AccountNumOfLearnResponse> aggregateNumOfLearn(@RequestBody @Valid StudyLogDailyByListFilter filter,
                                                         @RequestParam( required = false) Integer pageIndex,
                                                         @RequestParam( required = false) Integer pageSize,
                                                         @RequestParam( required = false, defaultValue = "account") String orderBy,
                                                         @RequestParam( required = false, defaultValue = "true") Boolean desc
    ){
        final Sort sort;
        final int MAX_PAGE_SIZE = 2000; // aggregate support large page size
        if( orderBy.compareToIgnoreCase("account")==0 ){
            sort = Sort.by("userId");
        }
        else{
            sort = Sort.by( orderBy );
        }

        final Pageable pageable;
        if( desc ) {
            pageable = pageRequest(pageIndex, pageSize, MAX_PAGE_SIZE,  sort.descending());
        }
        else{
            pageable = pageRequest(pageIndex, pageSize, MAX_PAGE_SIZE, sort );
        }

        PageResponse<AccountNumOfLearnResponse> resp = studyLogDailyService.aggregateAccountNumberOfLearn(
                filter.getProduct(), filter.getAccountIds(),
                Arrays.asList( filter.getDateStart(), filter.getDateEnd() ),
                pageable
        );

        if( resp.getElements().isEmpty() ){
            return new PageResponse.Builder<AccountNumOfLearnResponse>(StatusCode.SUCCESS, "Not found any study log").build();
        }

        // get all account
        List<Account> accounts = accountRepository.findAllByIdIn( filter.getAccountIds() );

        // Mapping Id to entity
        final HashMap<String,AccountDto> accountMap = new HashMap<>();
        accounts.forEach( x->{
            accountMap.put( x.getId(), AccountMapper.INSTANCE.entity2dto( x ));
        });

        // save data
        resp.getElements().forEach( x->{
            if( accountMap.containsKey( x.getAccountId() )){
                x.setAccount( accountMap.get( x.getAccountId() ));
            }
        });

        resp.setStatus( SUCCESS.getValue() );
        resp.setMessage( "Success");

        return resp;
    }


}
