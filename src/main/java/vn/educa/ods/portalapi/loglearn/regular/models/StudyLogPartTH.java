package vn.educa.ods.portalapi.loglearn.regular.models;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Where;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Setter @Getter
@Table( name = "study_log_platforms_th")
@Where( clause = "(IS_STUDY_LOG_V3 != 1) or (IS_STUDY_LOG_V3 is null)") // Log app version 1,2
public class StudyLogPartTH extends StudyLogPart {

    private Boolean isStudyLogV3;
}
