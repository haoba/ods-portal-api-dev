package vn.educa.ods.portalapi.loglearn.category.components;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;
import vn.educa.ods.portalapi.loglearn.category.models.StudyLogCategory;

@Repository
public interface StudyLogCategoryRepository extends JpaRepository<StudyLogCategory,String>, JpaSpecificationExecutor<StudyLogCategory> {
}
