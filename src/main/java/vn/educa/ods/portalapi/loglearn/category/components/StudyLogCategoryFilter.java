package vn.educa.ods.portalapi.loglearn.category.components;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import vn.educa.ods.portalapi.helpers.PortalApiDesc;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.List;

@Setter @Getter
@Schema( description = "Log Learn Champion League Filter Conditional")
public class StudyLogCategoryFilter {
    @Schema( description = PortalApiDesc.PRODUCT_CODE_DESC )
    @NotBlank( message = "Product is required")
    private String product;

    @Schema( description = "Search with a specified account")
    private String accountId;

    @Schema( description = "List of account id")
    @Size( max = 1000, message = "Number of accounts must be less 1000")
    private List<String> accountIds;

    @Schema( description = "start date", defaultValue = "2023-01-01T00:00:00.000Z")
    private Date dateStart;

    @Schema( description = "end date", defaultValue = "2023-02-01T00:00:00.000Z")
    private Date dateEnd;

    @Schema( description = "All log with lesson-unit")
    @Size( max = 1000, message = "Number of units must be less 1000")
    private List<String> unitIds;
}
