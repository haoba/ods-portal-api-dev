package vn.educa.ods.portalapi.loglearn.champion.models;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import vn.educa.ods.commons.models.BaseEntity;
import vn.educa.ods.portalapi.lesson.models.Lesson;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.math.BigDecimal;
import java.util.Date;

@Setter @Getter
@Entity( name = "study_log_champion_league_th")
public class StudyLogChampion extends BaseEntity {

    @Id
    protected String id;

    @Column( name = "userId")
    private String accountId;

    private String username;

    private String product;

    private Integer dateId;

    @Column( name = "time")
    private Date date;

    @ManyToOne
    @NotFound( action = NotFoundAction.IGNORE )
    private Lesson lesson;

    private BigDecimal lessonPoint;

    private Integer questionNumber;

    private String answer;
}
