package vn.educa.ods.portalapi.loglearn.liveClass;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.*;
import vn.educa.ods.commons.controllers.BaseRestController;
import vn.educa.ods.commons.controllers.PageResponse;
import vn.educa.ods.commons.models.dto.loglearn.StudyLogLiveClassDto;
import vn.educa.ods.portalapi.helpers.PortalApiDesc;
import vn.educa.ods.portalapi.lesson.components.LiveClassRepository;
import vn.educa.ods.portalapi.lesson.components.PartRepository;
import vn.educa.ods.portalapi.lesson.models.LiveClass;
import vn.educa.ods.portalapi.lesson.models.Part;
import vn.educa.ods.portalapi.loglearn.liveClass.components.StudyLogLiveClassFilter;
import vn.educa.ods.portalapi.loglearn.liveClass.components.StudyLogLiveClassMapper;
import vn.educa.ods.portalapi.loglearn.liveClass.models.StudyLogLiveClass;
import vn.educa.ods.portalapi.loglearn.liveClass.services.StudyLogLiveClassService;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping( value = "api/v1/log-learn/live-class", produces = "application/json")
@Tag(name = PortalApiDesc.LOG_LEARN_API, description = PortalApiDesc.LOG_LEARN_API_DESC)
public class StudyLogLiveClassRestController extends BaseRestController {

    private final StudyLogLiveClassService logLiveClassService;
    private final LiveClassRepository classRepository;
    private final PartRepository partRepository;

    @Autowired
    public StudyLogLiveClassRestController(
            StudyLogLiveClassService classService,
            LiveClassRepository classRepository,
            PartRepository partRepository
    ){
        this.logLiveClassService = classService;
        this.classRepository = classRepository;
        this.partRepository = partRepository;
    }

    /**
     * Search Study log of student
     * @param filter filter by
     * @param pageIndex page index
     * @param pageSize page size, default 50
     * @param orderBy order by data, default date
     * @param desc true for order desc/ otherwise order desc
     * @return pagination of StudyLogAverageDto
     */
    @PostMapping("search")
    @Operation(
            summary = "Search log live class",
            description = "Search by Product, AccountId, Parts and in a date range"
    )
    public PageResponse<StudyLogLiveClassDto> search(@RequestBody @Valid StudyLogLiveClassFilter filter,
                                                     @RequestParam( required = false) Integer pageIndex,
                                                     @RequestParam( required = false) Integer pageSize,
                                                     @RequestParam( required = false, defaultValue = "date") String orderBy,
                                                     @RequestParam( required = false, defaultValue = "true") Boolean desc
            ){

        final Sort sort;
        if( orderBy.compareToIgnoreCase("date")==0 ){
            sort = Sort.by("dateId");
        }
        else{
            sort = Sort.by( orderBy );
        }

        final Pageable pageable;
        if( desc ) {
            pageable = pageRequest(pageIndex, pageSize, sort.descending());
        }
        else{
            pageable = pageRequest(pageIndex, pageSize, sort );
        }

        // live class
        List<LiveClass> liveClasses = null;
        if( filter.getLiveClassIds()!=null && !filter.getLiveClassIds().isEmpty() ){
            liveClasses = classRepository.findAllById( filter.getLiveClassIds() );
            if(liveClasses.isEmpty()){
                return new PageResponse.Builder<StudyLogLiveClassDto>(StatusCode.SUCCESS,
                        "Not found any study log with live-class:" + filter.getLiveClassIds() )
                        .build();
            }
        }

        // Lesson part
        List<Part> parts = null;
        if( filter.getPartIds()!=null && !filter.getPartIds().isEmpty() ){
            parts = partRepository.findAllById( filter.getPartIds() );
            if( parts.isEmpty() ){
                return new PageResponse.Builder<StudyLogLiveClassDto>(StatusCode.SUCCESS,
                        "Not found any study log with parts:" + filter.getPartIds() )
                        .build();
            }
        }

        Page<?> logLiveClasses = logLiveClassService.search(filter, liveClasses, parts, pageable );

        if( logLiveClasses==null || logLiveClasses.isEmpty() ){
            return new PageResponse.Builder<StudyLogLiveClassDto>(StatusCode.SUCCESS, "Not found any study log").build();
        }

        List<StudyLogLiveClassDto> logLearn = logLiveClasses.stream().map(
                x -> StudyLogLiveClassMapper.INSTANCE.entity2dto( (StudyLogLiveClass) x )
        ).collect(Collectors.toList());

        return new PageResponse.Builder<>(SUCCESS, logLearn, logLiveClasses ).build();
    }
}
