package vn.educa.ods.portalapi.loglearn.regular.components;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;
import vn.educa.ods.portalapi.loglearn.regular.models.StudyLogPartTH2;

@Repository
public interface StudyLogPartTH2Repository extends JpaRepository<StudyLogPartTH2,String>, JpaSpecificationExecutor<StudyLogPartTH2> {
}
