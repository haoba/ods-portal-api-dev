package vn.educa.ods.portalapi.loglearn.regular.models;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Setter @Getter
@Table( name = "study_log_platforms_th2")
public class StudyLogPartTH2 extends StudyLogPart {
}
