package vn.educa.ods.portalapi.loglearn.liveClass.models;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import vn.educa.ods.commons.models.BaseEntity;
import vn.educa.ods.portalapi.lesson.models.LiveClass;
import vn.educa.ods.portalapi.lesson.models.Part;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import java.util.Date;

@Setter @Getter
@MappedSuperclass
public class StudyLogLiveClass extends BaseEntity {
    @Id
    protected String id;

    @Column( name = "userId")
    private String accountId;

    private String username;

    private Integer dateId;

    @Column( name = "time")
    private Date date;

    @ManyToOne
    @NotFound( action = NotFoundAction.IGNORE )
    private LiveClass liveClass;

    @ManyToOne
    @NotFound( action = NotFoundAction.IGNORE )
    private Part part;

    private Integer partPoint;

    private Long timeAnswer;

    @Column( name = "liveindex" )
    private Integer liveIndex;

    private String product;
}
