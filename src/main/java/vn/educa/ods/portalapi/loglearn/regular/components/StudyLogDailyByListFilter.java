package vn.educa.ods.portalapi.loglearn.regular.components;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import vn.educa.ods.portalapi.helpers.PortalApiDesc;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.List;

@Setter @Getter
@Schema( description = "Log Learn Filter Conditional in a List Students")
public class StudyLogDailyByListFilter {
    @Schema( description = PortalApiDesc.PRODUCT_CODE_DESC )
    private String product;

    @Schema(description = "Lis tof account and product")
    @NotEmpty( message = "accountIds must is not empty")
    @NotNull( message = "accountIds must is not null")
    @Size( min = 1, max = 2000, message = "Length of item must in range [0..2000]")
    private List<String> accountIds;

    @Schema( description = "start date", defaultValue = "2023-01-01T00:00:00.000Z")
    private Date dateStart;

    @Schema( description = "end date", defaultValue = "2023-02-01T00:00:00.000Z")
    private Date dateEnd;
}
