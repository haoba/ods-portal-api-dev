package vn.educa.ods.portalapi.loglearn.liveClass.components;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import vn.educa.ods.portalapi.helpers.PortalApiDesc;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.List;

@Setter @Getter
@Schema( description = "Log Learn Filter Conditional")
public class StudyLogLiveClassFilter {
    @Schema( description = PortalApiDesc.PRODUCT_CODE_DESC )
    @NotBlank( message = "Product is required")
    private String product;

    @Schema( description = "Search with a specified account")
    private String accountId;

    @Schema( description = "List of account id")
    @Size( max = 1000, message = "Number of accounts must be less 1000")
    private List<String> accountIds;

    @Schema( description = "start date", defaultValue = "2023-01-01T00:00:00.000Z")
    private Date dateStart;

    @Schema( description = "end date", defaultValue = "2023-02-01T00:00:00.000Z")
    private Date dateEnd;

    @Schema( description = "All log with live-class")
    @Size( max = 1000, message = "Number of live-classes must be less 1000")
    private List<String> liveClassIds;

    @Schema( description = "All log with lesson-parts")
    @Size( max = 1000, message = "Number of lesson-parts must be less 1000")
    private List<String> partIds;
}
