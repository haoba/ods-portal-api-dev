package vn.educa.ods.portalapi.authentication;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

@Data
@Schema( description = "Login request")
@Setter @Getter
public class LoginRequest {

    @NotBlank
    @Schema( description = "Register token with ODS system before using", defaultValue = "ods/amngWB7mFtTvZaJGdW8QxDVh")
    private String productToken;

    @NotBlank
    @Schema( description = "Username in the ODS system", defaultValue = "ods-demo")
    private String username;

    @NotBlank
    @Schema( description = "Raw password", defaultValue = "RkFdCzffzPEwUh7nvHCG3P5m")
    private String password;
}
