package vn.educa.ods.portalapi.authentication;


import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import vn.educa.ods.portalapi.users.models.User;

import java.util.Date;

@Data
@Schema(description = "Response token for Auth, Fill JWT token to Authorization Bearer header ")
public class LoginResponse {
    @Schema( description = "JWT Access Token")
    private String accessToken;

    @Schema( description = "Type of Authorization")
    private String tokenType = "Bearer";

    private Long userId;

    private String fullName;

    private Date tokenExpiration;

    //System information
    private final String sysName        = "ODS Portal API";
    private final String sysAuthVersion = "ODS/JWT/v1.0.0";

    public LoginResponse(User user, String accessToken, Date tokenExpiration){
        this.accessToken = accessToken;
        this.userId      = user.getId();
        this.fullName    = user.getFullName();
        this.tokenExpiration = tokenExpiration;
    }
}

