package vn.educa.ods.portalapi.authentication.jwt;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import vn.educa.ods.portalapi.users.UserService;
import vn.educa.ods.portalapi.users.models.Privilege;
import vn.educa.ods.portalapi.users.models.Role;
import vn.educa.ods.portalapi.users.models.User;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Service
public class JwtUserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private UserService userService;

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String userName)
            throws UsernameNotFoundException, AccessDeniedException {
        User user = userService.findByUsername(userName);
        if( user==null ) {
            throw new UsernameNotFoundException("User not found with username : " + userName);
        }
        // ignore in active user
        if ( !user.getEnable()) {
            throw new AccessDeniedException("User " + user.getUsername() + " was disable");
        }

        return JwtUserDetailImpl.create(user, getAuthorities( user.getRoles() ));
    }

    @Transactional
    public UserDetails loadUserById(Long id) {
        User user = userService.findById( id );
        if( user==null ) {
            throw new UsernameNotFoundException("User not found with userId : " + id);
        }
        System.out.println("user: " + user);
        return JwtUserDetailImpl.create(user, getAuthorities( user.getRoles() ));
    }

    private Collection<? extends GrantedAuthority> getAuthorities(
            Collection<Role> roles) {

        return getGrantedAuthorities(getPrivileges(roles));
    }

    private List<String> getPrivileges(Collection<Role> roles) {

        List<String> privileges = new ArrayList<>();
        List<Privilege> collection = new ArrayList<>();
        for (Role role : roles) {
            privileges.add(role.getName());
            collection.addAll(role.getPrivileges());
        }
        for (Privilege item : collection) {
            privileges.add(item.getName());
        }
        return privileges;
    }

    private List<GrantedAuthority> getGrantedAuthorities(List<String> privileges) {
        List<GrantedAuthority> authorities = new ArrayList<>();
        for (String privilege : privileges) {
            authorities.add(new SimpleGrantedAuthority(privilege));
        }
        return authorities;
    }
}
