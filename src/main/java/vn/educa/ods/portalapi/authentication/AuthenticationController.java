package vn.educa.ods.portalapi.authentication;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.web.bind.annotation.*;
import vn.educa.ods.commons.controllers.BaseRestController;
import vn.educa.ods.commons.controllers.RestResponse;
import vn.educa.ods.portalapi.authentication.jwt.JwtTokenProvider;
import vn.educa.ods.portalapi.exceptions.NotSupportedPortalApi;
import vn.educa.ods.portalapi.helpers.PortalApiDesc;
import vn.educa.ods.portalapi.product.RegisteredProductService;
import vn.educa.ods.portalapi.users.UserService;
import vn.educa.ods.portalapi.users.models.User;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.Date;

@Slf4j
@RestController
@RequestMapping(value = "/api/auth/", produces = "application/json")
@RequiredArgsConstructor
@Tag(name = PortalApiDesc.AUTH_API, description = PortalApiDesc.AUTH_API_DESC)
public class AuthenticationController {

    final AuthenticationManager authenticationManager;

    final JwtTokenProvider tokenProvider;

    final UserService userService;

    final RegisteredProductService registeredProductService;

    @PostMapping(value = "/login", produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(
            summary = "Login Auth",
            description = "Login system and fill JWT token to Authorization Bearer header")
    public RestResponse<LoginResponse> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {

        // check: has registered project in system?
        if( !registeredProductService.productHasRegisteredAlready( loginRequest.getProductToken() )){
            throw new NotSupportedPortalApi("Product Token has not registered yet. Please inform to admin!!!");
        }

        User user = userService.findByUsername(loginRequest.getUsername());
        final Authentication authentication;
        try {
            authentication = authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));
        } catch (AuthenticationException authenticationException) {
            userService.loginFailMark(user);
            return new RestResponse.Builder<>(BaseRestController.StatusCode.FAILED,
                    new LoginResponse( user, authenticationException.getMessage(), null)
            ).build();
        }
        System.out.println("authentication: " + authentication );
        userService.activate(user, true);

        // Clear all cache for current user
        userService.clearCache( user.getUsername() );

//        try {
//            userService.checkLockedTime(user);
//        } catch (LoginException loginException) {
//            return new RestResponse.RestResponseBuilder(StatusCode.FAILED.getValue(),
//                    new JwtAuthenticationResponse(loginException.getMessage())).build();
//        }

        SecurityContextHolder.getContext().setAuthentication(authentication);

        String jwt = tokenProvider.generateToken(authentication);

        Date tokenExpired = tokenProvider.getTokenExpirationFromJWT( jwt );
        System.out.println("token jwt: "+ jwt);
        return new RestResponse.Builder<>(BaseRestController.StatusCode.SUCCESS,
                new LoginResponse(user, jwt, tokenExpired )).build();
    }

    @GetMapping("/logout")
    public RestResponse<Boolean> getLogout(HttpServletRequest request, HttpServletResponse response){

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null){
            new SecurityContextLogoutHandler().logout(request, response, auth);
        }

        return new RestResponse.Builder<>(BaseRestController.StatusCode.SUCCESS, true).message(
                "Logout success"
        ).build();
    }
}
