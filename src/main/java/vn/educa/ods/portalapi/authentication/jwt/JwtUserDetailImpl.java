package vn.educa.ods.portalapi.authentication.jwt;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import vn.educa.ods.portalapi.users.models.User;

import java.util.Collection;
import java.util.Objects;

@Data
public class JwtUserDetailImpl implements UserDetails {
    private Long id;

    private String username;

    private String fullName;

    @JsonIgnore
    private String email;

    @JsonIgnore
    private String password;

    public JwtUserDetailImpl( Long userId, String username, String fullName, String email, String password, Collection<? extends GrantedAuthority> authorities){
        this.id = userId;
        this.username = username;
        this.fullName = fullName;
        this.email = email;
        this.password = password;
        this.authorities = authorities;
    }

    private Collection<? extends GrantedAuthority> authorities;

    public static JwtUserDetailImpl create(User user, Collection<? extends GrantedAuthority> authorities) {

        return new JwtUserDetailImpl(
                user.getId(),
                user.getUsername(),
                user.getFullName(),
                user.getEmail(),
                user.getPassword(),
                authorities
        );
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        JwtUserDetailImpl that = (JwtUserDetailImpl) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
