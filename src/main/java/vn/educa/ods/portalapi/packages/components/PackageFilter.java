package vn.educa.ods.portalapi.packages.components;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

@Setter @Getter
@Schema( description = "Search package")
public class PackageFilter {
    @Schema( description = "Package name")
    @Length( min = 4, message = "Not support search with name too short (must greater 4 characters)" )
    private String name;

    @Schema( description = "Similar description")
    @Length( min = 4, message = "Not support search with name too short (must greater 4 characters)" )
    private String description;

    @Schema( description = "Price smallest")
    private Double priceFrom;

    @Schema( description = "Price largest")
    private Double priceTo;

    @Schema( description = "Number of from day")
    private Integer dayFrom;

    @Schema( description = "Number of from to")
    private Integer dayTo;
}
