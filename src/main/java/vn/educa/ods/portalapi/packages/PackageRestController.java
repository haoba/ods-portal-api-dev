package vn.educa.ods.portalapi.packages;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import vn.educa.ods.commons.controllers.BaseRestController;
import vn.educa.ods.commons.controllers.PageResponse;
import vn.educa.ods.commons.controllers.RestResponse;
import vn.educa.ods.commons.models.dto.PackageDto;
import vn.educa.ods.portalapi.exceptions.ResourceNotFoundException;
import vn.educa.ods.portalapi.helpers.PortalApiDesc;
import vn.educa.ods.portalapi.helpers.datesync.DateSyncFilter;
import vn.educa.ods.portalapi.helpers.datesync.DateSyncSpecification;
import vn.educa.ods.portalapi.helpers.datesync.DateSyncValidator;
import vn.educa.ods.portalapi.packages.components.PackageFilter;
import vn.educa.ods.portalapi.packages.components.PackageMapper;
import vn.educa.ods.portalapi.packages.components.PackageRepository;
import vn.educa.ods.portalapi.packages.components.PackageSpecification;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "api/v1/package", produces = "application/json")
@RequiredArgsConstructor
@Tag(name = PortalApiDesc.PACKAGE_API, description = PortalApiDesc.PACKAGE_API_DESC )
public class PackageRestController extends BaseRestController {

    final PackageRepository packageRepository;

    @GetMapping("{id}")
    public RestResponse<PackageDto> getPackage(@PathVariable Long id){

        Optional<Packages> packages = packageRepository.findById(id);
        if(packages.isEmpty()){
            throw new ResourceNotFoundException("[ODS] Package not found for this id:" + id );
        }

        PackageDto dto = PackageMapper.INSTANCE.entity2dto( packages.get() );

        return new RestResponse.Builder<>(SUCCESS, dto).build();
    }

    @PostMapping("search")
    public PageResponse<PackageDto> search(@RequestBody @Valid PackageFilter filter,
                                           @RequestParam( required = false ) Integer pageIndex,
                                           @RequestParam( required = false ) Integer pageSize){

        Pageable pageable = pageRequest( pageIndex, pageSize );

        Page<Packages> packagesPage = packageRepository.findAll(
                PackageSpecification.filterBy( filter ), pageable
        );

        List<PackageDto> packageDtoList = packagesPage.stream().map(
                PackageMapper.INSTANCE::entity2dto
        ).collect(Collectors.toList());

        return new PageResponse.Builder<>(SUCCESS, packageDtoList, packagesPage).build();
    }

    @Operation(
            summary = "Package synchronize",
            description = "Package synchronize by Updated Date"
    )
    @PostMapping("sync")
    public PageResponse<PackageDto> synchronize(@RequestBody DateSyncFilter filter,
                                                @RequestParam( required = false ) Integer pageIndex,
                                                @RequestParam( required = false ) Integer pageSize){

        Pageable pageable = pageRequest( pageIndex, pageSize );

        Page<Packages> packagesPage = packageRepository.findAll(
                DateSyncSpecification.filterBySyncDate(
                        DateSyncValidator.validate( filter )
                ),
                pageable
        );

        List<PackageDto> packageDtoList = packagesPage.stream().map(
                PackageMapper.INSTANCE::entity2dto
        ).collect(Collectors.toList());

        return new PageResponse.Builder<>(SUCCESS, packageDtoList, packagesPage).build();
    }
}
