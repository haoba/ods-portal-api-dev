package vn.educa.ods.portalapi.packages.components;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import vn.educa.ods.commons.models.dto.PackageDto;
import vn.educa.ods.portalapi.packages.Packages;

@Mapper
public interface PackageMapper {
    PackageMapper INSTANCE = Mappers.getMapper( PackageMapper.class );

    PackageDto entity2dto(Packages entity);
    Packages    dto2entity(PackageDto dto);
    Packages    clone(Package entity);
}
