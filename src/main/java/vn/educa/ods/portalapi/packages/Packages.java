package vn.educa.ods.portalapi.packages;


import lombok.Getter;
import lombok.Setter;
import vn.educa.ods.commons.models.BaseEntity;

import javax.persistence.*;
import java.util.Date;

@Setter @Getter
@Entity
@Table( name = "packages")
//@Where( clause = "is_active = true")
public class Packages extends BaseEntity {
    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    private Long id;

    private String name;

    private String description;

    private Integer kind;

    private Integer type;

    @Column(precision=20, scale=2)
    private Double price;

    @Column( name = "day")
    private Long days;

    private Boolean isActive;

    private Date createdAt;

    private Date updatedAt;
}
