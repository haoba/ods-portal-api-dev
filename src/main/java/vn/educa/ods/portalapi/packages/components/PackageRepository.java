package vn.educa.ods.portalapi.packages.components;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;
import vn.educa.ods.portalapi.packages.Packages;

import java.util.List;
import java.util.Optional;

@Repository
public interface PackageRepository extends JpaRepository<Packages,String>, JpaSpecificationExecutor<Packages> {
    Optional<Packages> findById( Long id );
    List<Packages> findByIdIn( List<Long> ids );
}
