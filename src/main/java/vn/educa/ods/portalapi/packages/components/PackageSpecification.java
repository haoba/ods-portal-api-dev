package vn.educa.ods.portalapi.packages.components;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;
import vn.educa.ods.portalapi.packages.Packages;

import javax.persistence.criteria.Predicate;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Component
public class PackageSpecification {

    public static Specification<Packages> filterBy(@NotNull PackageFilter filter ){
        return (root, query, cb) -> {
            final List<Predicate> predicates = new ArrayList<>();

            // filter by field
            if (filter.getName() != null) {
                predicates.add(cb.like(root.get("name"), "%" + filter.getName() + "%"));
            }

            if (filter.getDescription() != null) {
                predicates.add(cb.like(root.get("name"), "%" + filter.getName() + "%"));
            }

            if (filter.getPriceFrom() != null) {
                predicates.add(cb.greaterThanOrEqualTo(root.get("price"), filter.getPriceFrom() ));
            }
            if (filter.getPriceTo() != null) {
                predicates.add(cb.lessThanOrEqualTo(root.get("price"), filter.getPriceTo() ));
            }

            if (filter.getDayFrom() != null) {
                predicates.add(cb.greaterThanOrEqualTo(root.get("days"), filter.getDayFrom() ));
            }
            if (filter.getDayTo() != null) {
                predicates.add(cb.lessThanOrEqualTo(root.get("days"), filter.getDayTo() ));
            }

            return cb.and(predicates.toArray(new Predicate[0]));
        };
    }
}
