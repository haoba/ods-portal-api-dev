package vn.educa.ods.portalapi.main;

import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import vn.educa.ods.commons.controllers.BaseRestController;
import vn.educa.ods.commons.controllers.RestResponse;
import vn.educa.ods.portalapi.helpers.PortalApiDesc;

@RestController
@Slf4j
@RequestMapping( value = "api/health-check/", produces = "application/json")
@Tag(name = PortalApiDesc.HEALTH_CHECK_API, description = PortalApiDesc.HEALTH_CHECK_DESC)
public class HealthCheckController extends BaseRestController {

    @Data
    public class StatusResponse{
        private String status;
    }

    @GetMapping("status")
    public RestResponse<StatusResponse> status(){
        StatusResponse res = new StatusResponse();

        res.setStatus("Working");

        return new RestResponse.Builder<>(SUCCESS, res).build();
    }
}
