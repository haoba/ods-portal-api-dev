package vn.educa.ods.portalapi.configuration;

import org.hibernate.boot.model.naming.Identifier;
import org.hibernate.boot.model.naming.PhysicalNamingStrategy;
import org.hibernate.engine.jdbc.env.spi.JdbcEnvironment;

import java.util.Locale;

/**
 * Convert standard name of schema and column in entity
 * Oracle convention name: UPPER_NAME_SEPARATED_BY_UNDERSCORE_SYMBOL
 * etc:
 *      ods_etl.study_log_platform --> ODS_ETL.STUDY_LOG_PLATFORM
 *      odsEtl.studyLogPlatform    --> ODS_ETL.STUDY_LOG_PLATFORM
 */
public class OracleNamingStrategyCustom implements PhysicalNamingStrategy {

    @Override
    public Identifier toPhysicalCatalogName(Identifier identifier, JdbcEnvironment context) {
        if( identifier==null ){
            return null;
        }
        return oracleNamingStrategy(identifier, context );
    }

    @Override
    public Identifier toPhysicalSchemaName(Identifier identifier, JdbcEnvironment context) {
        if( identifier==null ){
            return null;
        }
        return oracleNamingStrategy(identifier, context );
    }

    @Override
    public Identifier toPhysicalTableName(Identifier name, JdbcEnvironment context) {
        return oracleNamingStrategy(name, context );
    }

    @Override
    public Identifier toPhysicalSequenceName(Identifier identifier, JdbcEnvironment context) {
        return oracleNamingStrategy(identifier, context );
    }

    @Override
    public Identifier toPhysicalColumnName(Identifier identifier, JdbcEnvironment context) {
        return oracleNamingStrategy(identifier, context );
    }

    private Identifier oracleNamingStrategy(Identifier identifier, JdbcEnvironment context ){
        return new Identifier(addUnderscores(identifier.getText()), identifier.isQuoted());
    }

    private static boolean isLower(Character ch){
        return Character.isDigit( ch ) || Character.isLowerCase( ch );
    }

    private static boolean isUpper(Character ch){
        return Character.isDigit( ch ) || Character.isUpperCase( ch );
    }

    protected static String addUnderscores(String name) {
        final StringBuilder buf = new StringBuilder(name);
        for (int i = 1; i < buf.length() - 1; i++) {
            if (isLower(buf.charAt(i - 1)) &&
                    isUpper(buf.charAt(i)) &&
                    isLower(buf.charAt(i + 1))) {
                buf.insert(i++, '_');
            }
        }
        return buf.toString().toUpperCase(Locale.ROOT);
    }
}
