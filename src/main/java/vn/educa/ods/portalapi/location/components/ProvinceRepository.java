package vn.educa.ods.portalapi.location.components;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;
import vn.educa.ods.portalapi.location.Province;

import java.util.List;
import java.util.Optional;

@Repository
public interface ProvinceRepository extends JpaRepository<Province,String>, JpaSpecificationExecutor<Province> {
    Optional<Province> findById( Long id );
    Optional<Province> findByCode(String id );
    List<Province> findByCountryCode(String countryCode );
}
