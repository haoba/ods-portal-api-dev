package vn.educa.ods.portalapi.location.components;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;
import vn.educa.ods.portalapi.location.District;
import vn.educa.ods.portalapi.location.Province;

import javax.persistence.criteria.Predicate;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Component
public class LocationSpecification {

    public static Specification<Province> filterProvinceBy(@NotNull LocationFilter filter ){
        return (root, query, cb) -> {
            final List<Predicate> predicates = new ArrayList<>();

            // filter by field
            if (filter.getCode() != null) {
                predicates.add(cb.equal(root.get("code"), filter.getCode()));
            }
            if (filter.getCode3() != null) {
                predicates.add(cb.equal(root.get("code3"), filter.getCode3()));
            }
            if (filter.getName() != null) {
                predicates.add(cb.like(root.get("name"), "%" + filter.getName() + "%"));
            }

            if (filter.getCountryCode() != null) {
                predicates.add(cb.equal(root.get("countryCode"), filter.getCountryCode() ));
            }

            return cb.and(predicates.toArray(new Predicate[0]));
        };
    }

    public static Specification<District> filterDistrictBy(@NotNull LocationFilter filter ){
        return (root, query, cb) -> {
            final List<Predicate> predicates = new ArrayList<>();

            // filter by field
            if (filter.getCode() != null) {
                predicates.add(cb.equal(root.get("code"), filter.getCode()));
            }
            if (filter.getCode3() != null) {
                predicates.add(cb.equal(root.get("code3"), filter.getCode3()));
            }
            if (filter.getName() != null) {
                predicates.add(cb.like(root.get("name"), "%" + filter.getName() + "%"));
            }

            return cb.and(predicates.toArray(new Predicate[0]));
        };
    }
}
