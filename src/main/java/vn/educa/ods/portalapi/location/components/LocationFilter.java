package vn.educa.ods.portalapi.location.components;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;

@Setter @Getter
@Schema( description = "Search location by name and code")
public class LocationFilter {
    @Schema( description = "Code")
    private String code;

    @Schema( description = "Code 3 (3 characters)")
    private String code3;

    @Schema( description = "District/Province name")
    private String name;

    @Schema( description = "Country code (VN,TL)")
    private String countryCode;
}
