package vn.educa.ods.portalapi.location.components;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import vn.educa.ods.commons.models.dto.DistrictDto;
import vn.educa.ods.commons.models.dto.ProvinceDto;
import vn.educa.ods.portalapi.location.District;
import vn.educa.ods.portalapi.location.Province;

@Mapper
public interface LocationMapper {
    LocationMapper INSTANCE = Mappers.getMapper( LocationMapper.class );

    DistrictDto entity2dto(District entity);
    District    dto2entity(DistrictDto dto);
    District    clone(District entity);

    ProvinceDto entity2dto(Province entity);
    Province    dto2entity(ProvinceDto dto);
    Province    clone(Province entity);
}
