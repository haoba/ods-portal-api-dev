package vn.educa.ods.portalapi.location;

import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import vn.educa.ods.commons.controllers.BaseRestController;
import vn.educa.ods.commons.controllers.PageResponse;
import vn.educa.ods.commons.controllers.RestResponse;
import vn.educa.ods.commons.models.dto.DistrictDto;
import vn.educa.ods.commons.models.dto.ProvinceDto;
import vn.educa.ods.commons.models.enums.CountryCode;
import vn.educa.ods.portalapi.exceptions.ResourceNotFoundException;
import vn.educa.ods.portalapi.helpers.PortalApiDesc;
import vn.educa.ods.portalapi.location.components.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "api/v1/location", produces = "application/json")
@Tag(name = PortalApiDesc.LOCATION_API, description = PortalApiDesc.LOCATION_API_DESC )
public class LocationRestController extends BaseRestController {

    @Autowired
    private DistrictRepository districtRepository;

    @Autowired
    private ProvinceRepository provinceRepository;

    @GetMapping("district/{id}")
    public RestResponse<DistrictDto> getDistrict(@PathVariable Long id){

        Optional<District> district = districtRepository.findById(id);
        if(district.isEmpty()){
            throw new ResourceNotFoundException("[ODS] District not found for this id:" + id );
        }

        DistrictDto dto = LocationMapper.INSTANCE.entity2dto( district.get() );

        return new RestResponse.Builder<>(SUCCESS, dto).build();
    }

    @GetMapping("province/{id}")
    public RestResponse<ProvinceDto> getProvince(@PathVariable Long id){

        Optional<Province> province = provinceRepository.findById(id);
        if(province.isEmpty()){
            throw new ResourceNotFoundException("[ODS] Province not found for this id:" + id );
        }

        ProvinceDto dto = LocationMapper.INSTANCE.entity2dto( province.get() );

        return new RestResponse.Builder<>(SUCCESS, dto).build();
    }

    @PostMapping("district/search")
    public PageResponse<DistrictDto> searchDistrict(@RequestBody LocationFilter filter,
                                           @RequestParam( required = false ) Integer pageIndex,
                                           @RequestParam( required = false ) Integer pageSize){

        Pageable pageable = pageRequest( pageIndex, pageSize );

        Page<District> districtPage = districtRepository.findAll(
                LocationSpecification.filterDistrictBy( filter ), pageable
        );

        List<DistrictDto> districtDtoList = districtPage.stream().map(
                LocationMapper.INSTANCE::entity2dto
        ).collect(Collectors.toList());

        return new PageResponse.Builder<>(SUCCESS, districtDtoList, districtPage).build();
    }

    @PostMapping("province/search")
    public PageResponse<ProvinceDto> searchProvince(@RequestBody LocationFilter filter,
                                            @RequestParam( required = false ) Integer pageIndex,
                                            @RequestParam( required = false ) Integer pageSize){

        Pageable pageable = pageRequest( pageIndex, pageSize );

        Page<Province> provincePage = provinceRepository.findAll(
                LocationSpecification.filterProvinceBy( filter ), pageable
        );

        List<ProvinceDto> provinceDtoList = provincePage.stream().map(
                LocationMapper.INSTANCE::entity2dto
        ).collect(Collectors.toList());

        return new PageResponse.Builder<>(SUCCESS, provinceDtoList, provincePage).build();
    }

    @GetMapping("province/{id}/districts")
    public RestResponse<List<DistrictDto>> getDistrictInProvince(@PathVariable Long id){

        Optional<Province> province = provinceRepository.findById(id);
        if(province.isEmpty()){
            throw new ResourceNotFoundException("[ODS] Province not found for this id:" + id );
        }

        List<District> districts = districtRepository.findByProvince( province.get() );


        List<DistrictDto> dto = districts.stream().map(
                LocationMapper.INSTANCE::entity2dto
        ).collect(Collectors.toList());

        return new RestResponse.Builder<>(SUCCESS, dto).build();
    }

    @GetMapping("province/all")
    public RestResponse<List<ProvinceDto>> getAll(@RequestParam CountryCode countryCode ){

        List<Province> provinces = provinceRepository.findByCountryCode( countryCode.name() );

        List<ProvinceDto> dto = provinces.stream().map(
                LocationMapper.INSTANCE::entity2dto
        ).collect(Collectors.toList());

        return new RestResponse.Builder<>(SUCCESS, dto).build();
    }
}
