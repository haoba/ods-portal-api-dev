package vn.educa.ods.portalapi.location;


import lombok.Getter;
import lombok.Setter;
import vn.educa.ods.commons.models.BaseEntity;
import vn.educa.ods.commons.models.enums.CountryCode;

import javax.persistence.*;

@Setter @Getter
@Entity
@Table( name = "provinces")
public class Province extends BaseEntity {
    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    private Long id;

    private String code;

    private String code3;

    private String name;

    private String countryCode;

    public CountryCode getCountryCode() {
        return CountryCode.parse( countryCode );
    }

    @Column( name = "refer_id")
    private String crm2Id;
}
