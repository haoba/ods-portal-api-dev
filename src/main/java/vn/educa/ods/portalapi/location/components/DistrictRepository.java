package vn.educa.ods.portalapi.location.components;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;
import vn.educa.ods.portalapi.location.District;
import vn.educa.ods.portalapi.location.Province;

import java.util.List;
import java.util.Optional;

@Repository
public interface DistrictRepository extends JpaRepository<District,String>, JpaSpecificationExecutor<District> {
    Optional<District> findById(Long id );
    Optional<District> findByCode(String id );

    List<District> findByProvince( Province province );
}
