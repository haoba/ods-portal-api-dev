package vn.educa.ods.portalapi.biz.masked;

import vn.educa.ods.commons.models.dto.AccountPackageDto;

public interface MaskedSensitiveDataService {

    AccountPackageDto mask( AccountPackageDto sensitiveData );

}
