package vn.educa.ods.portalapi.biz.masked;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import vn.educa.ods.commons.models.dto.AccountPackageDto;
import vn.educa.ods.commons.models.dto.ContactDto;
import vn.educa.ods.commons.utils.EncryptUtil;
import vn.educa.ods.portalapi.users.UserService;

import java.security.NoSuchAlgorithmException;

@Service
@Slf4j
@RequiredArgsConstructor
public class MaskedSensitiveDataServiceImpl implements MaskedSensitiveDataService {

    final UserService userService;

    private final static int SENSITIVE_DATA_RAW_LENGTH = 3;
    private final static int SENSITIVE_DATA_LENGTH     = 8;

    private static String mask(String raw ){
        if( raw==null ){
            return null;
        }

        if( raw.length() >= SENSITIVE_DATA_RAW_LENGTH ){
            raw = raw.substring(0, SENSITIVE_DATA_RAW_LENGTH );
        }
        raw = (raw+ "*".repeat( SENSITIVE_DATA_LENGTH )).substring(0, SENSITIVE_DATA_LENGTH );

        return raw;
    }

    private static String hash(String raw ){
        String hash;
        try{
            hash = EncryptUtil.md5( raw );
        } catch (NoSuchAlgorithmException e) {
            hash = "Not supported";
            log.error("Not supported Md5 algorithm");
            e.printStackTrace();
        }
        return hash;
    }

    public AccountPackageDto mask( AccountPackageDto sd ){

        // check if not permission 'unmask' role
        if( userService.isMaskSensitiveData( userService.currentUsername() ) ) {
            sd.setActivateCode(mask(sd.getActivateCode()));
            sd.setActivateCodeMd5(hash(sd.getActivateCode()));
        }

        return sd;
    }

    public ContactDto mask(ContactDto sd ){
        // check if not permission 'unmask' role
        if( userService.isMaskSensitiveData( userService.currentUsername() ) ) {
            sd.setPhone(    mask(sd.getPhone()));
            sd.setPhoneMd5( hash(sd.getPhone()));
        }
        return sd;
    }

}
