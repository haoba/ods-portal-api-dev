package vn.educa.ods.portalapi.lesson.components;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;

import javax.persistence.criteria.Predicate;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Component
public class LiveClassSpecification {

    public static<T> Specification<T> filterBy(@NotNull LiveClassFilter filter ){
        return (root, query, cb) -> {
            final List<Predicate> predicates = new ArrayList<>();

            // filter by field
            if (filter.getProduct() != null) {
                predicates.add(cb.equal(root.get("product"), filter.getProduct()));
            }

            if (filter.getProduct() != null) {
                predicates.add(cb.equal(root.get("product"), filter.getProduct()));
            }
            if (filter.getName() != null) {
                predicates.add( cb.like(root.get("name"  ), "%" + filter.getName() + "%"));
            }
            if (filter.getDescription() != null) {
                predicates.add( cb.like(root.get("description"  ), "%" + filter.getDescription() + "%"));
            }
            if (filter.getStatus() != null) {
                predicates.add(cb.equal(root.get("status"), filter.getStatus()));
            }

            return cb.and(predicates.toArray(new Predicate[0]));
        };
    }
}
