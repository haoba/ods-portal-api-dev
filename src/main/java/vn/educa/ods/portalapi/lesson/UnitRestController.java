package vn.educa.ods.portalapi.lesson;

import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import vn.educa.ods.commons.controllers.BaseRestController;
import vn.educa.ods.commons.controllers.PageResponse;
import vn.educa.ods.commons.controllers.RestResponse;
import vn.educa.ods.commons.models.dto.lessons.UnitDto;
import vn.educa.ods.portalapi.exceptions.ResourceNotFoundException;
import vn.educa.ods.portalapi.helpers.PortalApiDesc;
import vn.educa.ods.portalapi.lesson.components.LessonBaseFilter;
import vn.educa.ods.portalapi.lesson.components.LessonBaseSpecification;
import vn.educa.ods.portalapi.lesson.components.UnitMapper;
import vn.educa.ods.portalapi.lesson.components.UnitRepository;
import vn.educa.ods.portalapi.lesson.models.Unit;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping( value = "api/v1/lesson-unit", produces = "application/json")
@Tag(name = PortalApiDesc.LESSON_SYSTEM_API, description = PortalApiDesc.LESSON_SYSTEM_API_DESC)
public class UnitRestController extends BaseRestController {

    @Autowired
    UnitRepository unitRepository;

    @GetMapping("{id}")
    public RestResponse<UnitDto> get(@PathVariable String id){

        Optional<Unit> unit = unitRepository.findById(id.toUpperCase());
        if(unit.isEmpty()){
            throw new ResourceNotFoundException("[ODS] Lesson Level not found for this id:" + id );
        }

        UnitDto dto = UnitMapper.INSTANCE.entity2dto( unit.get() );

        return new RestResponse.Builder<>(SUCCESS, dto).build();
    }

    @PostMapping("search")
    public PageResponse<UnitDto> search(@RequestBody @Valid LessonBaseFilter filter,
                                          @RequestParam( required = false ) Integer pageIndex,
                                          @RequestParam( required = false ) Integer pageSize){

        Pageable pageable = pageRequest( pageIndex, pageSize );
        Page<Unit> unitPage = unitRepository.findAll(
                LessonBaseSpecification.filterBy( filter ), pageable
        );

        List<UnitDto> accountDtoList = unitPage.stream().map(
                UnitMapper.INSTANCE::entity2dto
        ).collect(Collectors.toList());

        return new PageResponse.Builder<>(SUCCESS, accountDtoList, unitPage).build();
    }
}
