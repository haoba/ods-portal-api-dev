package vn.educa.ods.portalapi.lesson.models;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Setter @Getter
@Entity
@Table( name = "lesson_liveclass" )
public class LiveClass {
    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    private String id;

    private String name;

    private String description;

    private String product;

    private Boolean status;

    private Date createdAt;

    private Date updatedAt;
}
