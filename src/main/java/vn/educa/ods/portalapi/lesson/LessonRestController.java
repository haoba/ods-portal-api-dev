package vn.educa.ods.portalapi.lesson;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import vn.educa.ods.commons.controllers.BaseRestController;
import vn.educa.ods.commons.controllers.PageResponse;
import vn.educa.ods.commons.controllers.RestResponse;
import vn.educa.ods.commons.models.dto.lessons.LessonDto;
import vn.educa.ods.portalapi.exceptions.ResourceNotFoundException;
import vn.educa.ods.portalapi.helpers.PortalApiDesc;
import vn.educa.ods.portalapi.lesson.components.LessonBaseFilter;
import vn.educa.ods.portalapi.lesson.components.LessonBaseSpecification;
import vn.educa.ods.portalapi.lesson.components.LessonMapper;
import vn.educa.ods.portalapi.lesson.components.LessonRepository;
import vn.educa.ods.portalapi.lesson.models.Lesson;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping( value = "api/v1/lesson", produces = "application/json")
@Tag(name = PortalApiDesc.LESSON_SYSTEM_API, description = PortalApiDesc.LESSON_SYSTEM_API_DESC)
public class LessonRestController extends BaseRestController {

    @Autowired
    LessonRepository lessonRepository;

    @GetMapping("{id}")
    @Operation( summary = "Get Lesson by Id")
    public RestResponse<LessonDto> get(@PathVariable String id){

        Optional<Lesson> lessons = lessonRepository.findById(id.toUpperCase());
        if(lessons.isEmpty()){
            throw new ResourceNotFoundException("[ODS] Account not found for this id:" + id );
        }

        LessonDto dto = LessonMapper.INSTANCE.entity2dto( lessons.get() );

        return new RestResponse.Builder<>(SUCCESS, dto).build();
    }

    @PostMapping("search")
    @Operation( summary = "Search lesson by name and code")
    public PageResponse<LessonDto> search(@RequestBody @Valid LessonBaseFilter filter,
                                          @RequestParam( required = false ) Integer pageIndex,
                                          @RequestParam( required = false ) Integer pageSize){

        Pageable pageable = pageRequest( pageIndex, pageSize );
        Page<Lesson> lessonPagination = lessonRepository.findAll(
                LessonBaseSpecification.filterBy( filter ), pageable
        );

        List<LessonDto> accountDtoList = lessonPagination.stream().map(
                LessonMapper.INSTANCE::entity2dto
        ).collect(Collectors.toList());

        return new PageResponse.Builder<>(SUCCESS, accountDtoList, lessonPagination).build();
    }
}
