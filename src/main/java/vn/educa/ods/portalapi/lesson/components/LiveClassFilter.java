package vn.educa.ods.portalapi.lesson.components;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import vn.educa.ods.portalapi.helpers.PortalApiDesc;

import javax.validation.constraints.NotBlank;

@Setter @Getter
@Schema( description = "Search live class")
public class LiveClassFilter {
    @Schema( description = PortalApiDesc.PRODUCT_CODE_DESC )
    @NotBlank( message = "Product is required")
    private String product;

    @Schema( description = "Live class name similar matching")
    @Length( min = 4, message = "Not support search with name string too short (must greater 4 characters)" )
    private String name;

    @Schema( description = "Live class description similar matching")
    @Length( min = 4, message = "Not support search with description string too short (must greater 4 characters)" )
    private String description;

    @Schema( description = "Status of live class")
    private Boolean status;
}
