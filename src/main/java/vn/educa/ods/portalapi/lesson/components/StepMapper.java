package vn.educa.ods.portalapi.lesson.components;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import vn.educa.ods.commons.models.dto.lessons.StepDto;
import vn.educa.ods.portalapi.lesson.models.Step;

@Mapper
public interface StepMapper {
    StepMapper INSTANCE = Mappers.getMapper( StepMapper.class );

    StepDto entity2dto(Step entity);
    Step    dto2entity(StepDto dto);
    Step    clone(Step entity);
}
