package vn.educa.ods.portalapi.lesson;

import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import vn.educa.ods.commons.controllers.BaseRestController;
import vn.educa.ods.commons.controllers.PageResponse;
import vn.educa.ods.commons.controllers.RestResponse;
import vn.educa.ods.commons.models.dto.lessons.LevelDto;
import vn.educa.ods.portalapi.exceptions.ResourceNotFoundException;
import vn.educa.ods.portalapi.helpers.PortalApiDesc;
import vn.educa.ods.portalapi.lesson.components.LessonBaseFilter;
import vn.educa.ods.portalapi.lesson.components.LessonBaseSpecification;
import vn.educa.ods.portalapi.lesson.components.LevelMapper;
import vn.educa.ods.portalapi.lesson.components.LevelRepository;
import vn.educa.ods.portalapi.lesson.models.Level;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping( value = "api/v1/lesson-level", produces = "application/json")
@Tag(name = PortalApiDesc.LESSON_SYSTEM_API, description = PortalApiDesc.LESSON_SYSTEM_API_DESC)
public class LevelRestController extends BaseRestController {

    @Autowired
    LevelRepository levelRepository;

    @GetMapping("{id}")
    public RestResponse<LevelDto> get(@PathVariable String id){

        Optional<Level> level = levelRepository.findById(id.toUpperCase());
        if(level.isEmpty()){
            throw new ResourceNotFoundException("[ODS] Lesson Level not found for this id:" + id );
        }

        LevelDto dto = LevelMapper.INSTANCE.entity2dto( level.get() );

        return new RestResponse.Builder<>(SUCCESS, dto).build();
    }

    @PostMapping("search")
    public PageResponse<LevelDto> search(@RequestBody @Valid LessonBaseFilter filter,
                                          @RequestParam( required = false ) Integer pageIndex,
                                          @RequestParam( required = false ) Integer pageSize){

        Pageable pageable = pageRequest( pageIndex, pageSize );
        Page<Level> levelPage = levelRepository.findAll(
                LessonBaseSpecification.filterBy( filter ), pageable
        );

        List<LevelDto> accountDtoList = levelPage.stream().map(
                LevelMapper.INSTANCE::entity2dto
        ).collect(Collectors.toList());

        return new PageResponse.Builder<>(SUCCESS, accountDtoList, levelPage).build();
    }
}
