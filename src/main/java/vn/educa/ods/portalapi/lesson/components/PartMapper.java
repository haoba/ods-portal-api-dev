package vn.educa.ods.portalapi.lesson.components;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import vn.educa.ods.commons.models.dto.lessons.PartDto;
import vn.educa.ods.portalapi.lesson.models.Part;

@Mapper
public interface PartMapper {
    PartMapper INSTANCE = Mappers.getMapper( PartMapper.class );

    PartDto entity2dto(Part entity);
    Part    dto2entity(PartDto dto);
    Part    clone(Part entity);
}
