package vn.educa.ods.portalapi.lesson.components;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;

import javax.persistence.criteria.Predicate;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Component
public class LessonBaseSpecification {

    public static<T> Specification<T> filterBy(@NotNull LessonBaseFilter filter ){
        return (root, query, cb) -> {
            final List<Predicate> predicates = new ArrayList<>();

            // filter by field
            if (filter.getProduct() != null) {
                predicates.add(cb.equal(root.get("product"), filter.getProduct()));
            }

            // filter by field
            if (filter.getCode() != null) {
                predicates.add(cb.equal(root.get("code"), filter.getCode()));
            }
            if (filter.getName() != null) {
                predicates.add(cb.or(
                        cb.like(root.get("name"  ), "%" + filter.getName() + "%"),
                        cb.like(root.get("nameEn"), "%" + filter.getName() + "%")
                ));
            }

            if( filter.getIds()!=null && !filter.getIds().isEmpty() ){
                predicates.add( root.get("id").in( filter.getIds() ));
            }

            return cb.and(predicates.toArray(new Predicate[0]));
        };
    }
}
