package vn.educa.ods.portalapi.lesson.components;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;
import vn.educa.ods.portalapi.lesson.models.Level;

import java.util.List;

@Repository
public interface LevelRepository extends JpaRepository<Level,String>, JpaSpecificationExecutor<Level> {
    List<Level> findByNameContains( String nameSearch );
}
