package vn.educa.ods.portalapi.lesson.components;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import vn.educa.ods.portalapi.helpers.PortalApiDesc;

import javax.validation.constraints.Size;
import java.util.List;

@Setter @Getter
@Schema( description = "Search lesson by code and similar name")
public class LessonBaseFilter {
    @Schema( description = PortalApiDesc.PRODUCT_CODE_DESC )
    private String product;

    @Schema( description = "Lesson code whole matching")
    private String code;

    @Schema( description = "Lesson name similar matching")
    @Length( min = 4, message = "Not support search with name too short (must greater 4 characters)" )
    private String name;

    @Schema( description = "Search by a list of ids")
    @Size( max = 1000, message = "Max size of ids must be less than 1000")
    private List<String> ids;
}
