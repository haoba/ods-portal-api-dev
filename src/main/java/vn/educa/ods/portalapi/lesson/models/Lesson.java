package vn.educa.ods.portalapi.lesson.models;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Setter @Getter
@Entity
@Table( name = "lessons" )
public class Lesson {
    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    private String id;

    private String code;

    private String name;

    private String nameEn;

    private String description;

    private String descriptionEn;

    private String product;

    private String unitId;

    private Date createdAt;

    private Date updatedAt;
}
