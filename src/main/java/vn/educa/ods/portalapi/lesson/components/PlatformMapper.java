package vn.educa.ods.portalapi.lesson.components;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import vn.educa.ods.commons.models.dto.lessons.PlatformDto;
import vn.educa.ods.portalapi.lesson.models.Platform;

@Mapper
public interface PlatformMapper {
    PlatformMapper INSTANCE = Mappers.getMapper( PlatformMapper.class );

    PlatformDto entity2dto(Platform entity);
    Platform    dto2entity(PlatformDto dto);
    Platform    clone(Platform entity);
}
