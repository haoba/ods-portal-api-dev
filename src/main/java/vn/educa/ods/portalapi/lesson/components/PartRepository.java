package vn.educa.ods.portalapi.lesson.components;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;
import vn.educa.ods.portalapi.lesson.models.Part;

import java.util.List;

@Repository
public interface PartRepository extends JpaRepository<Part,String>, JpaSpecificationExecutor<Part> {
    List<Part> findByCode( String code );
    List<Part> findByNameContains( String name );
    List<Part> findAllByIdIn(List<String> ids);
}
