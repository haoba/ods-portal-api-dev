package vn.educa.ods.portalapi.lesson.components;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;
import vn.educa.ods.portalapi.lesson.models.Lesson;

import java.util.List;

@Repository
public interface LessonRepository extends JpaRepository<Lesson,String>, JpaSpecificationExecutor<Lesson> {

    List<Lesson> findByCode( String code );

    List<Lesson> findByNameContains( String nameSearch );

    List<Lesson> findAllByIdIn( List<String> ids );
}
