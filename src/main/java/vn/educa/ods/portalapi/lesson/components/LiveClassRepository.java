package vn.educa.ods.portalapi.lesson.components;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;
import vn.educa.ods.portalapi.lesson.models.LiveClass;

import java.util.List;

@Repository
public interface LiveClassRepository extends JpaRepository<LiveClass,String>, JpaSpecificationExecutor<LiveClass> {
    List<LiveClass> findAllByIdIn(List<String> ids);
}
