package vn.educa.ods.portalapi.lesson;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import vn.educa.ods.commons.controllers.BaseRestController;
import vn.educa.ods.commons.controllers.PageResponse;
import vn.educa.ods.commons.controllers.RestResponse;
import vn.educa.ods.commons.models.dto.lessons.PartCategoryDto;
import vn.educa.ods.commons.models.dto.lessons.PartDto;
import vn.educa.ods.portalapi.exceptions.ResourceNotFoundException;
import vn.educa.ods.portalapi.helpers.PortalApiDesc;
import vn.educa.ods.portalapi.lesson.components.*;
import vn.educa.ods.portalapi.lesson.models.Part;
import vn.educa.ods.portalapi.lesson.models.PartCategory;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping( value = "api/v1/lesson-part", produces = "application/json")
@Tag(name = PortalApiDesc.LESSON_SYSTEM_API, description = PortalApiDesc.LESSON_SYSTEM_API_DESC)
public class PartRestController extends BaseRestController {

    final PartRepository partRepository;

    final PartCategoryRepository categoryRepository;

    @Autowired
    public PartRestController(PartRepository partRepository, PartCategoryRepository categoryRepository ){
        this.partRepository = partRepository;
        this.categoryRepository = categoryRepository;
    }

    @GetMapping("{id}")
    public RestResponse<PartDto> get(@PathVariable String id){

        Optional<Part> part = partRepository.findById(id.toUpperCase());
        if(part.isEmpty()){
            throw new ResourceNotFoundException("[ODS] Lesson Part not found for this id:" + id );
        }

        PartDto dto = PartMapper.INSTANCE.entity2dto( part.get() );

        return new RestResponse.Builder<>(SUCCESS, dto).build();
    }

    @PostMapping("search")
    public PageResponse<PartDto> search(@RequestBody @Valid LessonBaseFilter filter,
                                          @RequestParam( required = false ) Integer pageIndex,
                                          @RequestParam( required = false ) Integer pageSize){

        Pageable pageable = pageRequest( pageIndex, pageSize );
        Page<Part> partPagination = partRepository.findAll(
                LessonBaseSpecification.filterBy( filter ), pageable
        );

        List<PartDto> accountDtoList = partPagination.stream().map(
                PartMapper.INSTANCE::entity2dto
        ).collect(Collectors.toList());

        return new PageResponse.Builder<>(SUCCESS, accountDtoList, partPagination).build();
    }

    @GetMapping("categories")
    @Operation(
            summary = "Get category of part",
            description = "Get all category of part/Hoạt động ngoại khóa"
    )
    public RestResponse<List<PartCategoryDto>> categories(){

        // Get all category of part
        List<PartCategory> partCategories = categoryRepository.findAll();

        List<PartCategoryDto> ret = partCategories.stream().map(
                PartCategoryMapper.INSTANCE::entity2dto
        ).collect(Collectors.toList());

        return new RestResponse.Builder<>(SUCCESS, ret ).build();
    }
}
