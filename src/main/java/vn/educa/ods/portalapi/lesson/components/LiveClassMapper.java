package vn.educa.ods.portalapi.lesson.components;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import vn.educa.ods.commons.models.dto.lessons.LiveClassDto;
import vn.educa.ods.portalapi.lesson.models.LiveClass;

@Mapper
public interface LiveClassMapper {
    LiveClassMapper INSTANCE = Mappers.getMapper( LiveClassMapper.class );

    LiveClassDto entity2dto(LiveClass entity);
    LiveClass    dto2entity(LiveClassDto dto);
    LiveClass    clone(LiveClass entity);
}
