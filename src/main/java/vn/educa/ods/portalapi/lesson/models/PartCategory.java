package vn.educa.ods.portalapi.lesson.models;

import lombok.Getter;
import lombok.Setter;
import vn.educa.ods.commons.models.BaseEntity;
import vn.educa.ods.commons.models.enums.PartCategoryType;
import vn.educa.ods.portalapi.helpers.converters.PartCategoryTypeConverter;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Setter @Getter
@Entity
@Table( name = "lesson_part_categories" )
public class PartCategory extends BaseEntity {
    @Id
    private String id;

    private String name;

    private String description;

    @Convert( converter = PartCategoryTypeConverter.class )
    private PartCategoryType type;

    private Boolean isActive;

    private String product;

    private Date createdAt;

    private Date updatedAt;
}
