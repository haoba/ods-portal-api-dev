package vn.educa.ods.portalapi.lesson.models;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;
import java.util.Date;

@Setter @Getter
@Entity
@Table( name = "lesson_units" )
public class Unit {
    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    private String id;

    private String code;

    private String name;

    private String nameEn;

    private String description;

    private String descriptionEn;

    private String product;

    @ManyToOne
    @NotFound( action = NotFoundAction.IGNORE )
    private Level level;

    private Date createdAt;

    private Date updatedAt;
}
