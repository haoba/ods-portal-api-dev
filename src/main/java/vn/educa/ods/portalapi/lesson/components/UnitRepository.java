package vn.educa.ods.portalapi.lesson.components;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;
import vn.educa.ods.portalapi.lesson.models.Unit;

import java.util.List;

@Repository
public interface UnitRepository extends JpaRepository<Unit,String>, JpaSpecificationExecutor<Unit> {

    List<Unit> findByCode( String code );

    List<Unit> findByNameContains( String nameSearch );

    List<Unit> findAllByIdIn(List<String> ids );
}
