package vn.educa.ods.portalapi.lesson.components;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import vn.educa.ods.commons.models.dto.lessons.LessonDto;
import vn.educa.ods.portalapi.lesson.models.Lesson;

@Mapper
public interface LessonMapper {
    LessonMapper INSTANCE = Mappers.getMapper( LessonMapper.class );

    LessonDto entity2dto(Lesson entity);
    Lesson    dto2entity(LessonDto dto);
    Lesson    clone(Lesson entity);
}
