package vn.educa.ods.portalapi.lesson.models;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

@Setter @Getter
@Entity
@Table( name = "lesson_parts" )
public class Part {
    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    private String id;

    private String code;

    private String name;

    private String nameEn;

    private String description;

    private String descriptionEn;

    private String product;

    @ManyToMany
    @JoinTable(
            name="LESSON_PART_CATEGORY",
            joinColumns = @JoinColumn(name = "partId", referencedColumnName = "id" ),
            inverseJoinColumns = @JoinColumn(name = "id", referencedColumnName = "id")
    )
    @NotFound( action = NotFoundAction.IGNORE )
    private Set<PartCategory> category;

    @ManyToOne
    @NotFound( action = NotFoundAction.IGNORE )
    private Lesson lesson;

    private Date createdAt;

    private Date updatedAt;
}
