package vn.educa.ods.portalapi.lesson.components;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import vn.educa.ods.commons.models.dto.lessons.PartCategoryDto;
import vn.educa.ods.portalapi.lesson.models.PartCategory;

@Mapper
public interface PartCategoryMapper {
    PartCategoryMapper INSTANCE = Mappers.getMapper( PartCategoryMapper.class );

    PartCategoryDto entity2dto(PartCategory entity);
    PartCategory    dto2entity(PartCategoryDto dto);
    PartCategory    clone(PartCategory entity);
}
