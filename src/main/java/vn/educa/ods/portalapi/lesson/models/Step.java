package vn.educa.ods.portalapi.lesson.models;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;
import java.util.Date;

@Setter @Getter
@Entity
@Table( name = "lesson_steps" )
public class Step {
    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    private String id;

    private String name;

    private String description;

    private String product;

    private String partId;

    @ManyToOne
    @NotFound( action = NotFoundAction.IGNORE )
    private Platform platform;

    private Date createdAt;

    private Date updatedAt;
}
