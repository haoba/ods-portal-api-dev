package vn.educa.ods.portalapi.lesson.components;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;
import vn.educa.ods.portalapi.lesson.models.PartCategory;

@Repository
public interface PartCategoryRepository extends JpaRepository<PartCategory,String>, JpaSpecificationExecutor<PartCategory> {
}
