package vn.educa.ods.portalapi.lesson.components;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import vn.educa.ods.commons.models.dto.lessons.LevelDto;
import vn.educa.ods.portalapi.lesson.models.Level;

@Mapper
public interface LevelMapper {
    LevelMapper INSTANCE = Mappers.getMapper( LevelMapper.class );

    LevelDto entity2dto(Level entity);
    Level    dto2entity(LevelDto dto);
    Level    clone(Level entity);
}
