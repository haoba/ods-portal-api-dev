package vn.educa.ods.portalapi.lesson;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import vn.educa.ods.commons.controllers.BaseRestController;
import vn.educa.ods.commons.controllers.PageResponse;
import vn.educa.ods.commons.controllers.RestResponse;
import vn.educa.ods.commons.models.dto.lessons.LiveClassDto;
import vn.educa.ods.portalapi.exceptions.ResourceNotFoundException;
import vn.educa.ods.portalapi.helpers.PortalApiDesc;
import vn.educa.ods.portalapi.lesson.components.LiveClassFilter;
import vn.educa.ods.portalapi.lesson.components.LiveClassMapper;
import vn.educa.ods.portalapi.lesson.components.LiveClassRepository;
import vn.educa.ods.portalapi.lesson.components.LiveClassSpecification;
import vn.educa.ods.portalapi.lesson.models.LiveClass;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping( value = "api/v1/live-class", produces = "application/json")
@Tag(name = PortalApiDesc.LIVE_CLASS_API, description = PortalApiDesc.LIVE_CLASS_API_DESC)
public class LiveClassRestController extends BaseRestController {

    private final LiveClassRepository liveClassRepository;

    @Autowired
    public LiveClassRestController(LiveClassRepository liveClassRepository){
        this.liveClassRepository = liveClassRepository;
    }

    @GetMapping("{id}")
    @Operation( summary = "Get Live by Id")
    public RestResponse<LiveClassDto> get(@PathVariable String id){

        Optional<LiveClass> liveClass = liveClassRepository.findById(id.toUpperCase());
        if(liveClass.isEmpty()){
            throw new ResourceNotFoundException("[ODS] Account not found for this id:" + id );
        }

        LiveClassDto dto = LiveClassMapper.INSTANCE.entity2dto( liveClass.get() );

        return new RestResponse.Builder<>(SUCCESS, dto).build();
    }

    @PostMapping("search")
    @Operation( summary = "Search Live Class")
    public PageResponse<LiveClassDto> search(@RequestBody @Valid LiveClassFilter filter,
                                          @RequestParam( required = false ) Integer pageIndex,
                                          @RequestParam( required = false ) Integer pageSize){

        Pageable pageable = pageRequest( pageIndex, pageSize );
        Page<LiveClass> lessonPagination = liveClassRepository.findAll(
                LiveClassSpecification.filterBy( filter ), pageable
        );

        List<LiveClassDto> accountDtoList = lessonPagination.stream().map(
                LiveClassMapper.INSTANCE::entity2dto
        ).collect(Collectors.toList());

        return new PageResponse.Builder<>(SUCCESS, accountDtoList, lessonPagination).build();
    }
}
