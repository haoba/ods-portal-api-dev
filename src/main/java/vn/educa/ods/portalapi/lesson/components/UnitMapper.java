package vn.educa.ods.portalapi.lesson.components;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import vn.educa.ods.commons.models.dto.lessons.UnitDto;
import vn.educa.ods.portalapi.lesson.models.Unit;

@Mapper
public interface UnitMapper {
    UnitMapper INSTANCE = Mappers.getMapper( UnitMapper.class );

    UnitDto entity2dto(Unit entity);
    Unit    dto2entity(UnitDto dto);
    Unit    clone(Unit entity);
}
