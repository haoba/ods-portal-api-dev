package vn.educa.ods.portalapi.helpers;

public class PortalApiDesc {

    public final static String LESSON_SYSTEM_API        = "Lesson System/Courseware";
    public final static String LESSON_SYSTEM_API_DESC   = "Hệ thống học liệu";

    public final static String AUTH_API                 = "Authentication";
    public final static String AUTH_API_DESC            = "Login/Logout/Change Password";

    public final static String LIVE_CLASS_API           = "Live Class";
    public final static String LIVE_CLASS_API_DESC      = "Live Class";

    public final static String LOG_LEARN_API             = "Log Learn History (Study log...)";
    public final static String LOG_LEARN_API_DESC        = "Tra cứu lịch sử học";

    public final static String ACCOUNT_API               = "Product Account - AppUser";
    public final static String ACCOUNT_API_DESC          = "Tra cứu tài khoản học";

    public final static String PACKAGE_API               = "Learning Packages";
    public final static String PACKAGE_API_DESC          = "Tra cứu gói học";

    public final static String LOCATION_API              = "Location (District, Province, Ward)";
    public final static String LOCATION_API_DESC         = "Thông tin Quận huyện, Tỉnh thành";

    public final static String CONTACT_API               = "Contact API";
    public final static String CONTACT_API_DESC          = "Contact, Khách hàng, Người mua, Cha mẹ học sinh";

    public final static String CHILDREN_API               = "Children API";
    public final static String CHILDREN_API_DESC          = "Thông tin con Children";

    public final static String DEPART_CHANNEL_API        = "API kênh sở";
    public final static String DEPART_CHANNEL_DESC       = "API phân loại user cho kênh sở";

    public final static String HEALTH_CHECK_API          = "Health Check";
    public final static String HEALTH_CHECK_DESC         = "Check service status, useful for docker health check";

    public final static String ZALO_CALSS_API             = "Zalo Class";
    public final static String ZALO_CLASS_API_DESC        = "Các thông tin về ZaloClass";

    public final static String PRODUCT_CODE_DESC        = "Product Code (EDU.TH, EDU.THCS, EDU.KID, EDU.MATH)";

//    Haoba test

    public final static String CLASS_API           = "CLASS Information";
    public final static String CLASS_API_DESC   = "Thông tin lớp học";
}
