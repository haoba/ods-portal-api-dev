package vn.educa.ods.portalapi.helpers.converters.gender;

import lombok.extern.slf4j.Slf4j;
import vn.educa.ods.commons.models.enums.Gender;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter
@Slf4j
public class Crm3GenderConverter implements AttributeConverter<Gender,Integer> {
    private static final int MALE_INT_VAL     = 81;
    private static final int FEMALE_INT_VALUE = 82;

    @Override
    public Integer convertToDatabaseColumn(Gender gender) {
        if( gender==null ){
            return null;
        }

        switch ( gender ){
            case FEMALE:
                return FEMALE_INT_VALUE;
            case MALE:
                return MALE_INT_VAL;
            default:
                return null;
        }
    }

    @Override
    public Gender convertToEntityAttribute(Integer integer) {
        if( integer==null ){
            return null;
        }

        switch ( integer ){
            case FEMALE_INT_VALUE:
                return Gender.FEMALE;
            case MALE_INT_VAL:
                return Gender.MALE;
            default:
                log.warn("WARN: Unknown GenderType in CRM3:" + integer );
                return Gender.OTHER;
        }
    }
}
