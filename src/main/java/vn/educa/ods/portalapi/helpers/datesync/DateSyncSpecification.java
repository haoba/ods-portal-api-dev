package vn.educa.ods.portalapi.helpers.datesync;

import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.Predicate;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

public abstract class DateSyncSpecification {

    public static<T> Specification<T> filterBySyncDate(@NotNull DateSyncFilter filter ){
        return (root, query, cb) -> {
            final List<Predicate> predicates = new ArrayList<>();

            // filter by field
            if (filter.getChangedFrom() != null) {
                predicates.add(cb.greaterThanOrEqualTo(root.get("updatedAt"), filter.getChangedFrom()));
            }

            if (filter.getChangedTo() != null) {
                predicates.add(cb.lessThanOrEqualTo(root.get("updatedAt"), filter.getChangedTo()));
            }

            return cb.and(predicates.toArray(new Predicate[0]));
        };
    }
}
