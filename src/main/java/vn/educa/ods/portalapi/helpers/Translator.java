package vn.educa.ods.portalapi.helpers;

import org.springframework.stereotype.Component;

@Component
public class Translator {
    String format(String format, Object... args ){
        return format;
    }

    String message(String message){
        return message;
    }
}
