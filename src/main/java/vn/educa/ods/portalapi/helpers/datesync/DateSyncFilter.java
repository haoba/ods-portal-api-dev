package vn.educa.ods.portalapi.helpers.datesync;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Setter @Getter
@Schema( description = "Synchronize entity by Created Date or Updated Date")
public class DateSyncFilter {
    @Schema( description = "Objects were created/updated from this date")
    private Date changedFrom;

    @Schema( description = "Objects ware created/updated util this date")
    private Date changedTo;
}
