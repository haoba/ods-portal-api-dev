package vn.educa.ods.portalapi.helpers;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import java.util.List;

@Setter
@Getter
@Schema( description = "Phone List")
public class PhoneFilter {
    @Schema( description = "Phone Number List")
    @NotEmpty( message = "Phone required not empty")
    private List<String> phones;
}
