package vn.educa.ods.portalapi.helpers.converters.gender;

import vn.educa.ods.commons.models.enums.Gender;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter
public class OdsGenderConverter implements AttributeConverter<Gender,String> {
    @Override
    public String convertToDatabaseColumn(Gender gender) {
        if( gender==null ){
            return null;
        }

        switch ( gender ){
            case FEMALE:
                return "F";
            case MALE:
                return "M";
            default:
                return null;
        }
    }

    @Override
    public Gender convertToEntityAttribute(String integer) {
        if( integer==null ){
            return null;
        }

        switch ( integer ){
            case "F":
                return Gender.FEMALE;
            case "M":
                return Gender.MALE;
            default:
                    return Gender.OTHER;
        }
    }
}
