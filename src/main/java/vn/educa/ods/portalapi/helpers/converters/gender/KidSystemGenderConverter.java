package vn.educa.ods.portalapi.helpers.converters.gender;

import vn.educa.ods.commons.models.enums.Gender;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter
public class KidSystemGenderConverter implements AttributeConverter<Gender,Integer> {
    @Override
    public Integer convertToDatabaseColumn(Gender gender) {
        switch ( gender ){
            case FEMALE:
                return 0;
            case MALE:
                return 1;
            default:
                return 2;
        }
    }

    @Override
    public Gender convertToEntityAttribute(Integer integer) {
        switch ( integer ){
            case 0:
                return Gender.FEMALE;
            case 1:
                return Gender.MALE;
            default:
                    return Gender.OTHER;
        }
    }
}
