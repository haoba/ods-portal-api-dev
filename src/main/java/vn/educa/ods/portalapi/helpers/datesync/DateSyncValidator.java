package vn.educa.ods.portalapi.helpers.datesync;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

public class DateSyncValidator {

    private static final Date DATE_MIN = Date.from(
            LocalDate.of( 2010,01,01)
                    .atStartOfDay(ZoneId.systemDefault())
                    .toInstant()
    );

    public static DateSyncFilter validate( DateSyncFilter filter ){
        Date now = new Date();

        if( filter.getChangedFrom()==null || filter.getChangedFrom().before( DATE_MIN ) ){
            filter.setChangedTo( DATE_MIN );
        }

        if( filter.getChangedTo() == null || filter.getChangedTo().after( now ) ){
            filter.setChangedTo( now );
        }

        return filter;
    }
}
