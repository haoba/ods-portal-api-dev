package vn.educa.ods.portalapi.helpers.converters;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter
public class StringIntegerConverter implements AttributeConverter<String,Integer> {

    @Override
    public Integer convertToDatabaseColumn(String val) {
        if( val==null ){
            return null;
        }
        return Integer.valueOf( val );
    }

    @Override
    public String convertToEntityAttribute(Integer integer) {
        if( integer==null ){
            return null;
        }
        return String.valueOf( integer );
    }
}
