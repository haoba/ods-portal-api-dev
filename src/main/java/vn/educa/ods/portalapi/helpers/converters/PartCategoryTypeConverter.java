package vn.educa.ods.portalapi.helpers.converters;

import vn.educa.ods.commons.models.enums.PartCategoryType;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter
public class PartCategoryTypeConverter implements AttributeConverter<PartCategoryType,Integer> {
    @Override
    public Integer convertToDatabaseColumn(PartCategoryType type) {
        return type.getValue();
    }

    @Override
    public PartCategoryType convertToEntityAttribute(Integer integer) {
        if( integer==null ){
            return null;
        }
        return PartCategoryType.valueOf( integer );
    }
}
