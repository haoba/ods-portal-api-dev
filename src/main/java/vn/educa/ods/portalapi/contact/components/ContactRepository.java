package vn.educa.ods.portalapi.contact.components;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;
import vn.educa.ods.portalapi.contact.ContactCRM2;

import java.util.List;

@Repository
public interface ContactRepository extends JpaRepository<ContactCRM2,String>, JpaSpecificationExecutor<ContactCRM2> {
    List<ContactCRM2> findAllByPhoneIn(List<String> phones );
}
