package vn.educa.ods.portalapi.contact.components;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;
import vn.educa.ods.portalapi.contact.ContactCRM3;

import java.util.List;

@Repository
public interface ContactCrm3Repository extends JpaRepository<ContactCRM3,String>, JpaSpecificationExecutor<ContactCRM3> {
    List<ContactCRM3> findAllByPhoneIn(List<String> phones );
    List<ContactCRM3> findAllByIdIn( List<String> ids );
}
