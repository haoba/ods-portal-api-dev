package vn.educa.ods.portalapi.contact.components;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import vn.educa.ods.commons.models.dto.ContactDto;
import vn.educa.ods.portalapi.contact.ContactCRM2;
import vn.educa.ods.portalapi.contact.ContactCRM3;

@Mapper
public interface ContactMapper {
    ContactMapper INSTANCE = Mappers.getMapper( ContactMapper.class );

    ContactDto  entity2dto(ContactCRM2 entity);
    ContactCRM2 dto2entity(ContactDto dto);
    ContactCRM2 clone(ContactCRM2 entity);

    ContactDto  entity2dto(ContactCRM3 entity);
    ContactCRM3 dto2entityCRM3(ContactDto dto);
    ContactCRM3 clone(ContactCRM3 entity);
}
