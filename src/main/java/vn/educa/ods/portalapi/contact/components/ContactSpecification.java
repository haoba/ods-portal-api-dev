package vn.educa.ods.portalapi.contact.components;

import lombok.extern.slf4j.Slf4j;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;
import vn.educa.ods.portalapi.contact.ContactCRM2;
import vn.educa.ods.portalapi.contact.ContactCRM3;

import javax.persistence.criteria.Predicate;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Component
@Slf4j
public class ContactSpecification {

    public static Specification<ContactCRM2> filterByInCrm2(@NotNull ContactFilter filter ) {

        return (root, query, cb) -> {

            final List<Predicate> predicates = new ArrayList<>();

            // filter by field
            if (filter.getPhone() != null) {
                predicates.add(cb.equal(root.get("phone"), filter.getPhone()));
            }

            if (filter.getFullName() != null) {
                predicates.add(cb.like(root.get("fullName"), "%" + filter.getFullName() + "%"));
            }

            return cb.and(predicates.toArray(new Predicate[0]));
        };
    }

    public static Specification<ContactCRM3> filterByInCrm3(@NotNull ContactFilter filter ) {

        return (root, query, cb) -> {

            final List<Predicate> predicates = new ArrayList<>();

            // filter by field
            if (filter.getPhone() != null) {
                predicates.add(cb.equal(root.get("phone"), filter.getPhone()));
            }

            if (filter.getFullName() != null) {
                predicates.add(cb.like(root.get("fullName"), "%" + filter.getFullName() + "%"));
            }

            return cb.and(predicates.toArray(new Predicate[0]));
        };
    }
}
