package vn.educa.ods.portalapi.contact.components;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import vn.educa.ods.commons.models.enums.EducaSystem;

@Setter @Getter
@Schema( description = "Search account by username, similar fullname or phone")
public class ContactFilter {
    @Schema( description = "Similar full name")
    @Length( min = 3, message = "Not support search with name too short (must greater 3 characters)" )
    private String fullName;

    @Schema( description = "Phone number")
    private String phone;

    @Schema( description = "System (CRM,CRM3)")
    private EducaSystem system;

    @Schema( description = "Province Code")
    private String provinceCode;
}
