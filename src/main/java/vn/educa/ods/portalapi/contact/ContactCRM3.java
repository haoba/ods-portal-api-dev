package vn.educa.ods.portalapi.contact;


import lombok.Getter;
import lombok.Setter;
import vn.educa.ods.commons.models.BaseEntity;
import vn.educa.ods.commons.models.enums.EducaSystem;
import vn.educa.ods.commons.models.enums.Gender;
import vn.educa.ods.portalapi.location.District;
import vn.educa.ods.portalapi.location.Province;

import javax.persistence.*;
import java.util.Date;

@Setter @Getter
@Entity
@Table( name = "contacts_crm3")
public class ContactCRM3 extends BaseEntity {
    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    private String id;

    @Transient
    private EducaSystem system = EducaSystem.CRM3;

    private String address;

    private String fullName;

    @Transient
    private String email;

    @Transient
    private Gender gender;

    private String phone;

    @Transient
    private District district;

    @Transient
    private Province province;

    @Column( name = "DATE_CREATE")
    private Date createdAt;

    @Column( name = "DATE_MODIFY")
    private Date updatedAt;
}
