package vn.educa.ods.portalapi.contact;


import lombok.Getter;
import lombok.Setter;
import vn.educa.ods.commons.models.BaseEntity;
import vn.educa.ods.commons.models.enums.EducaSystem;
import vn.educa.ods.commons.models.enums.Gender;
import vn.educa.ods.portalapi.helpers.converters.gender.OdsGenderConverter;

import javax.persistence.*;
import java.util.Date;

@Setter @Getter
@Entity
@Table( name = "contacts_crm2")
public class ContactCRM2 extends BaseEntity {
    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    private String id;

    @Transient
    private EducaSystem system = EducaSystem.CRM;

    private String address;

    private String fullName;

    private String email;

    @Convert( converter = OdsGenderConverter.class )
    private Gender gender;

    private String phone;

    @Column( name = "districtId")
    private String districtCode;

    @Column( name = "provinceId")
    private String provinceCode;

    private Date createdAt;

    private Date updatedAt;
}
