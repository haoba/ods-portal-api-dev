package vn.educa.ods.portalapi.contact;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import vn.educa.ods.commons.controllers.BaseRestController;
import vn.educa.ods.commons.controllers.PageResponse;
import vn.educa.ods.commons.controllers.RestResponse;
import vn.educa.ods.commons.models.dto.AccountDto;
import vn.educa.ods.commons.models.dto.ContactDto;
import vn.educa.ods.portalapi.contact.components.*;
import vn.educa.ods.portalapi.exceptions.NotSupportedPortalApi;
import vn.educa.ods.portalapi.exceptions.ResourceNotFoundException;
import vn.educa.ods.portalapi.helpers.PortalApiDesc;
import vn.educa.ods.portalapi.helpers.datesync.DateSyncFilter;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@Slf4j
@RequestMapping(value = "api/v1/contact", produces = "application/json")
@RequiredArgsConstructor
@Tag(name = PortalApiDesc.CONTACT_API, description = PortalApiDesc.CONTACT_API_DESC)
public class ContactController extends BaseRestController {

    final ContactRepository contactRepository;

    final ContactCrm3Repository contactCrm3Repository;

    private ContactFilter validate(ContactFilter filter){
        if( filter.getPhone()!=null && !filter.getPhone().startsWith("0") ){
            filter.setPhone( "0" + filter.getPhone() );
        }

        return filter;
    }

    @GetMapping("crm2/{id}")
    @Operation( summary = "Get contact by ID" )
    public RestResponse<ContactDto> getContactCRM2(@PathVariable String id){

        Optional<ContactCRM2> account = contactRepository.findById(id.toLowerCase());
        if( account.isEmpty() ){
            throw new ResourceNotFoundException("[ODS] Contact not found id:" + id + " in CRM2 system" );
        }

        ContactDto dto = ContactMapper.INSTANCE.entity2dto( account.get() );

        return new RestResponse.Builder<>(SUCCESS, dto).build();
    }

    @GetMapping("crm3/{id}")
    @Operation( summary = "Search contact in CRM3 system" )
    public RestResponse<ContactDto> getContactCRM3(@PathVariable String id){

        Optional<ContactCRM3> account = contactCrm3Repository.findById(id.toUpperCase());
        if( account.isEmpty() ){
            throw new ResourceNotFoundException("[ODS] Contact not found id:" + id + " in CRM3 system");
        }

        ContactDto dto = ContactMapper.INSTANCE.entity2dto( account.get() );

        return new RestResponse.Builder<>(SUCCESS, dto).build();
    }

    @PostMapping("crm2/search")
    @Operation( summary = "Search contact in CRM2 system" )
    public PageResponse<ContactDto> searchCrm2(@RequestBody @Valid ContactFilter filter,
                                           @RequestParam( required = false ) Integer pageIndex,
                                           @RequestParam( required = false ) Integer pageSize){

        Pageable pageable = pageRequest( pageIndex, pageSize );

        ContactFilter newFilter = validate( filter );

        Page<ContactCRM2> contactPage = contactRepository.findAll(
                ContactSpecification.filterByInCrm2( newFilter ), pageable
        );

        List<ContactDto> contactDtoList = contactPage.stream().map(
                ContactMapper.INSTANCE::entity2dto
        ).collect(Collectors.toList());

        return new PageResponse.Builder<>(SUCCESS, contactDtoList, contactPage).build();
    }

    @PostMapping("crm3/search")
    @Operation( summary = "Search contact in CRM3 system" )
    public PageResponse<ContactDto> searchCrm3(@RequestBody @Valid ContactFilter filter,
                                           @RequestParam( required = false ) Integer pageIndex,
                                           @RequestParam( required = false ) Integer pageSize){

        Pageable pageable = pageRequest( pageIndex, pageSize );

        ContactFilter newFilter = validate( filter );

        Page<ContactCRM3> contactPage = contactCrm3Repository.findAll(
                ContactSpecification.filterByInCrm3( newFilter ), pageable
        );

        List<ContactDto> contactDtoList = contactPage.stream().map(
                ContactMapper.INSTANCE::entity2dto
        ).collect(Collectors.toList());

        return new PageResponse.Builder<>(SUCCESS, contactDtoList, contactPage).build();
    }

    @PostMapping(value = {"crm3/sync", "crm2/sync" })
    @Operation( summary = "Contact synchronize with CRM" )
    public PageResponse<AccountDto> synchronizeCRM3(@RequestBody DateSyncFilter filter,
                                                    @RequestParam( required = false ) Integer pageIndex,
                                                    @RequestParam( required = false ) Integer pageSize){

        throw new NotSupportedPortalApi("Not supported this API" );
    }
}
