package vn.educa.ods.portalapi.classes;

import lombok.Getter;
import lombok.Setter;
import vn.educa.ods.commons.models.BaseEntity;

import javax.persistence.*;
import java.util.Date;

@Getter @Setter
@Entity
@Table(name = "classes")
public class Classes extends BaseEntity {
    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    private String id;

    private String teacherId;

    private String gradeId;

    private Integer gradeCurrent;

    private String code;

    private String name;

    private String type;

    private String status;

    private Date createdAt;

    private Date updatedAt;

    private Integer isDeleted;

    private String product;
}
