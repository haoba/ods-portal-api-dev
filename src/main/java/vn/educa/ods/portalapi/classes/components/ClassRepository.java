package vn.educa.ods.portalapi.classes.components;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;
import vn.educa.ods.portalapi.classes.Classes;

import java.util.List;
import java.util.Optional;

@Repository
public interface ClassRepository extends JpaRepository<Classes,String>, JpaSpecificationExecutor<Classes> {
    Optional<Classes> findById(String id);
}
