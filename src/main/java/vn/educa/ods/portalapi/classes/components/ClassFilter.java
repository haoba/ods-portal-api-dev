package vn.educa.ods.portalapi.classes.components;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import vn.educa.ods.portalapi.helpers.PortalApiDesc;

import javax.validation.constraints.NotBlank;

@Setter @Getter
@Schema(description = "Search Class")
public class ClassFilter {
    @Schema( description = PortalApiDesc.PRODUCT_CODE_DESC )
    @NotBlank( message = "Product is required")
    private String product;

    @Schema( description = "Code of Class" )
    private String code;

    @Schema( description = "class name similar matching")
    @Length( min = 4, message = "Not support search with name string too short (must greater 4 characters)" )
    private String name;

    @Schema( description = "class type similar matching")
    @Length( min = 4, message = "Not support search with description string too short (must greater 4 characters)" )
    private String type;

    @Schema( description = "Status of class")
    private String status;

}
