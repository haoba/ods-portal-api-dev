package vn.educa.ods.portalapi.classes;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import vn.educa.ods.commons.controllers.PageResponse;
import vn.educa.ods.commons.controllers.RestResponse;
import vn.educa.ods.commons.controllers.BaseRestController;
import vn.educa.ods.commons.models.dto.ClassDTO;
import vn.educa.ods.portalapi.classes.components.ClassFilter;
import vn.educa.ods.portalapi.classes.components.ClassMapper;
import vn.educa.ods.portalapi.classes.components.ClassRepository;
import vn.educa.ods.portalapi.classes.components.ClassSpecification;
import vn.educa.ods.portalapi.exceptions.ResourceNotFoundException;
import vn.educa.ods.portalapi.helpers.PortalApiDesc;


import javax.validation.Valid;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "api/v1/class", produces = "application/json")
@RequiredArgsConstructor
@Tag(name = PortalApiDesc.CLASS_API, description = PortalApiDesc.CLASS_API_DESC)
public class ClassRestController extends BaseRestController{
    final ClassRepository classRepository;

    @GetMapping("{id}")
    public RestResponse<ClassDTO> getClass(@PathVariable String id){
        Optional<Classes> classes = classRepository.findById(id);
        if(classes.isEmpty()){
            throw new ResourceNotFoundException("[ODS] Class not found for this id:" + id );
        }

        ClassDTO dto = ClassMapper.INSTANCE.entity2dto( classes.get() );

        return new RestResponse.Builder<>(SUCCESS, dto).build();
    }

    @PostMapping("search")
    @Operation( summary = "Search Class")
    public PageResponse<ClassDTO> search(@RequestBody @Valid ClassFilter filter,
                                             @RequestParam( required = false ) Integer pageIndex,
                                             @RequestParam( required = false ) Integer pageSize){

        Pageable pageable = pageRequest( pageIndex, pageSize );
        Page<Classes> classPagination = classRepository.findAll(
                ClassSpecification.filterBy( filter ), pageable
        );

        List<ClassDTO> classDtoList = classPagination.stream().map(
                ClassMapper.INSTANCE::entity2dto
        ).collect(Collectors.toList());

        return new PageResponse.Builder<>(SUCCESS, classDtoList, classPagination).build();
    }
}
