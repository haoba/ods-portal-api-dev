package vn.educa.ods.portalapi.classes.components;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import vn.educa.ods.commons.models.dto.ClassDTO;
import vn.educa.ods.portalapi.classes.Classes;

@Mapper
public interface ClassMapper {
    ClassMapper INSTANCE = Mappers.getMapper( ClassMapper.class );

    ClassDTO entity2dto(Classes entity);
    Classes    dto2entity(ClassDTO dto);
    Classes    clone(Classes entity);
}
