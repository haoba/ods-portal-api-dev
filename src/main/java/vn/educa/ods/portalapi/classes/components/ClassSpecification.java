package vn.educa.ods.portalapi.classes.components;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;
import vn.educa.ods.portalapi.lesson.components.LiveClassFilter;

import javax.persistence.criteria.Predicate;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Component
public class ClassSpecification {

    public static<T> Specification<T> filterBy(@NotNull ClassFilter filter ){
        return (root, query, cb) -> {
            final List<Predicate> predicates = new ArrayList<>();

            // filter by field
            if (filter.getProduct() != null) {
                predicates.add(cb.equal(root.get("product"), filter.getProduct()));
            }
            if (filter.getCode() != null) {
                predicates.add(cb.equal(root.get("code"), filter.getCode()));
            }
            if (filter.getName() != null) {
                predicates.add(cb.like(root.get("name"), "%"+ filter.getName() + "%"));
            }
            if (filter.getType() != null) {
                predicates.add(cb.like(root.get("type"), "%" + filter.getType() + "%"));
            }
            if (filter.getStatus() != null) {
                predicates.add(cb.equal(root.get("status"), filter.getStatus()));
            }

            return cb.and(predicates.toArray(new Predicate[0]));
        };
    }
}
