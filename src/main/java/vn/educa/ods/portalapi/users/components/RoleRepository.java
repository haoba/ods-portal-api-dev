package vn.educa.ods.portalapi.users.components;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import vn.educa.ods.portalapi.users.models.Role;

@Repository
public interface RoleRepository extends JpaRepository<Role,Long> {
    Role findByName( String name );
}
