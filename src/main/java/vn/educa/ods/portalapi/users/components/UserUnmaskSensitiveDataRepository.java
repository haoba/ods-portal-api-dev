package vn.educa.ods.portalapi.users.components;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import vn.educa.ods.portalapi.users.models.UserUnmaskSensitiveData;

import java.util.Optional;

@Repository
public interface UserUnmaskSensitiveDataRepository extends JpaRepository<UserUnmaskSensitiveData,String> {
    Optional<UserUnmaskSensitiveData> getByUsername( String username);
}
