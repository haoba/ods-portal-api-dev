package vn.educa.ods.portalapi.users;

import vn.educa.ods.portalapi.users.models.User;

public interface UserService {

    User findById(Long id);

    User findByUsername(String username);

    String currentUsername();

    void activate(User user, boolean activate);

    void loginFailMark(User user);

    boolean isMaskSensitiveData(String username);

    void clearCache(String username);
}
