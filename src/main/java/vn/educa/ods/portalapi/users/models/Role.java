package vn.educa.ods.portalapi.users.models;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.Collection;
import java.util.Date;

@Entity
@Setter @Getter
@Table( schema = "odsApps", name = "odsRoles")
@Where( clause = "deleted = false")
@NoArgsConstructor
public class Role {
    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    private Boolean deleted = false;

    @ManyToMany(mappedBy = "roles")
    private Collection<User> users;

    @ManyToMany
    @JoinTable(
            schema = "odsApps", name = "odsRolesPrivileges",
            joinColumns = @JoinColumn( name = "roleId", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn( name = "privilegeId", referencedColumnName = "id")
    )
    private Collection<Privilege> privileges;

    @CreationTimestamp
    private Date createdAt;

    @UpdateTimestamp
    private Date updatedAt;

    public Role(String name){
        this.name = name;
    }
}
