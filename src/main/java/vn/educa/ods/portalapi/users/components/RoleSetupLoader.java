package vn.educa.ods.portalapi.users.components;

import lombok.RequiredArgsConstructor;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import vn.educa.ods.portalapi.users.models.Privilege;
import vn.educa.ods.portalapi.users.models.Role;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

/**
 * Auto setup default roles if not found
 */
@Component
@RequiredArgsConstructor
public class RoleSetupLoader implements ApplicationListener<ContextRefreshedEvent> {
    boolean alreadySetup = false;

    final UserRepository userRepository;

    final RoleRepository roleRepository;

    final PrivilegeRepository privilegeRepository;

    final PasswordEncoder passwordEncoder;

    @Override
    @Transactional
    public void onApplicationEvent(ContextRefreshedEvent event) {

        if (alreadySetup) {
            return;
        }

        final Privilege ro = createPrivilegeIfNotFound("READ_PRIVILEGE");
        final Privilege rw = createPrivilegeIfNotFound("WRITE_PRIVILEGE");

        createRoleIfNotFound("ROLE_ADMIN",    Arrays.asList( ro, rw));
        createRoleIfNotFound("ROLE_USER",     List.of(ro));
        createRoleIfNotFound("ROLE_GUEST",    List.of(ro));
        createRoleIfNotFound("ROLE_REPORTER", List.of(ro));

//        Role adminRole = roleRepository.findByName("ROLE_ADMIN");
//        User user = new User();
//        user.set("Test");
//        user.setLastName("Test");
//        user.setPassword(passwordEncoder.encode("test"));
//        user.setEmail("test@test.com");
//        user.setRoles(Arrays.asList(adminRole));
//        user.setEnabled(true);
//        userRepository.save(user);

        alreadySetup = true;
    }

    @Transactional
    Privilege createPrivilegeIfNotFound(String name) {

        Privilege privilege = privilegeRepository.findByName(name);
        if (privilege == null) {
            privilege = new Privilege(name);
            privilegeRepository.save(privilege);
        }
        return privilege;
    }

    @Transactional
    Role createRoleIfNotFound( String name, Collection<Privilege> privileges) {

        Role role = roleRepository.findByName(name);
        if (role == null) {
            role = new Role(name);
            role.setPrivileges(privileges);
            roleRepository.save(role);
        }
        return role;
    }
}
