package vn.educa.ods.portalapi.users;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import vn.educa.ods.portalapi.helpers.CacheDefinition;
import vn.educa.ods.portalapi.users.components.UserRepository;
import vn.educa.ods.portalapi.users.components.UserUnmaskSensitiveDataRepository;
import vn.educa.ods.portalapi.users.models.User;

import java.util.Date;
import java.util.concurrent.atomic.AtomicReference;

@Service
@RequiredArgsConstructor
@Slf4j
public class UserServiceImpl implements UserService {

    final UserRepository userRepository;

    final UserUnmaskSensitiveDataRepository userUnmaskSensitiveDataRepository;

    @Override
    public User findById(Long id) {
        return userRepository.getById( id );
    }

    @Override
    public User findByUsername(String username) {
        return userRepository.getByUsername( username ).orElse( null );
    }

    @Override
    public String currentUsername() {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if (principal instanceof UserDetails) {
            return  ((UserDetails)principal).getUsername();
        } else {
            return principal.toString();
        }
    }

    @Override
    public void activate(User user, boolean activate) {

        user.setEnable( activate );
        userRepository.save(user);
    }

    @Override
    public void loginFailMark(User user) {
    }

    /**
     * User need to mask/hidden sensitive data
     * @return
     */
    @Override
    @Cacheable(CacheDefinition.USER_MASK_SENSITIVE_DATA )
    public boolean isMaskSensitiveData(String username) {
        AtomicReference<Boolean> isUnmask = new AtomicReference<>(false);
        userUnmaskSensitiveDataRepository.getByUsername( username ).ifPresent(
                x-> isUnmask.set( x.getUnmask() && x.getExpiredDate().after( new Date() ) )
        );

        if( isUnmask.get() ){
            log.info("User {} access sensitive data", username );
        }

        return ! isUnmask.get();
    }

    @CacheEvict( CacheDefinition.USER_MASK_SENSITIVE_DATA )
    public void clearCacheMaskSensitiveData(String username){
    }

    @Override
    public void clearCache(String username) {
        clearCacheMaskSensitiveData( username );
    }
}
