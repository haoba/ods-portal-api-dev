package vn.educa.ods.portalapi.users.models;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table( schema = "odsApps", name = "odsUserUnmaskSensitiveData")
@Setter @Getter
public class UserUnmaskSensitiveData {
    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    private String username;

    private Boolean unmask;

    private Date expiredDate;

    private Date createdAt;

    private Date updatedAt;
}
