package vn.educa.ods.portalapi.exceptions;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import vn.educa.ods.commons.controllers.BaseRestController;
import vn.educa.ods.commons.controllers.RestResponse;

@ControllerAdvice
@Slf4j
public class GlobalExceptionHandler {

    @ExceptionHandler(value = ResourceNotFoundException.class)
    public ResponseEntity<RestResponse> handleAppException(ResourceNotFoundException exception) {

        RestResponse response = new RestResponse.Builder(BaseRestController.StatusCode.FAILED, null)
                .message(exception.getMessage())
                .build();

        log.error( exception.getMessage() );

        return new ResponseEntity(response, HttpStatus.BAD_REQUEST );
    }

    @ExceptionHandler(value = NotSupportedPortalApi.class)
    public ResponseEntity<RestResponse> handleAppException(NotSupportedPortalApi exception) {

        RestResponse response = new RestResponse.Builder(BaseRestController.StatusCode.FAILED, null)
                .message(exception.getMessage())
                .build();

        log.error( exception.getMessage() );

        return new ResponseEntity(response, HttpStatus.BAD_REQUEST );
    }

    @ExceptionHandler(value = HttpMessageNotReadableException.class)
    public ResponseEntity<RestResponse> handleAppException(HttpMessageNotReadableException exception) {

        RestResponse response = new RestResponse.Builder(BaseRestController.StatusCode.FAILED, null)
                .message(exception.getMessage())
                .build();

        log.error( exception.getMessage() );

        return new ResponseEntity(response, HttpStatus.BAD_REQUEST );
    }

    @ExceptionHandler(value = MethodArgumentNotValidException.class)
    public ResponseEntity<RestResponse> handleAppException(MethodArgumentNotValidException exception) {

        RestResponse response = new RestResponse.Builder(BaseRestController.StatusCode.FAILED, null)
                .message(exception.getMessage())
                .build();

        log.error( exception.getMessage() );

        return new ResponseEntity(response, HttpStatus.BAD_REQUEST );
    }
}
