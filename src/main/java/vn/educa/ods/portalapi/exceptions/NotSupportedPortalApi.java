package vn.educa.ods.portalapi.exceptions;

public class NotSupportedPortalApi extends RuntimeException {
    public NotSupportedPortalApi(String msg){
        super(msg);
    }
}
