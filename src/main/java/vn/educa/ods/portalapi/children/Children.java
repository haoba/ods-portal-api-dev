package vn.educa.ods.portalapi.children;


import lombok.Getter;
import lombok.Setter;
import vn.educa.ods.commons.models.BaseEntity;
import vn.educa.ods.commons.models.enums.EducaSystem;
import vn.educa.ods.commons.models.enums.Gender;
import vn.educa.ods.portalapi.helpers.converters.gender.OdsGenderConverter;

import javax.persistence.*;
import java.util.Date;

@Setter @Getter
@Entity
@Table( name = "contact_children")
public class Children extends BaseEntity {
    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    private String id;

    @Transient
    private EducaSystem system = EducaSystem.CRM;

    private String fullName;

    private Date dob;

    @Convert( converter = OdsGenderConverter.class )
    private Gender gender;

    @Column( name = "contactId")
    private String contactIdCrm2;

    @Transient
    private String contactIdCrm3;

    private String school;

    @Column( name = "studentClass")
    private String classLevel;

    private Date createdAt;

    private Date updatedAt;
}
