package vn.educa.ods.portalapi.children.components;

import lombok.extern.slf4j.Slf4j;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;
import vn.educa.ods.portalapi.children.Children;
import vn.educa.ods.portalapi.children.ChildrenCrm3;

import javax.persistence.criteria.Predicate;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Component
@Slf4j
public class ChildrenSpecification {

    public static Specification<Children> filterInCrm2(@NotNull ChildrenFilter filter ) {

        return (root, query, cb) -> {
            final List<Predicate> predicates = new ArrayList<>();

            // filter by field
            if (filter.getFullName() != null) {
                predicates.add( cb.like(root.get("fullName"), "%" + filter.getFullName() + "%"));
            }

            if( filter.getContactIds() !=null ){
                predicates.add( root.get("contactIdCrm2").in( filter.getContactIds())  );
            }

            if( filter.getDobFrom()!=null || filter.getDobTo()!=null ) {
                if (filter.getDobFrom() != null) {
                    predicates.add(cb.greaterThanOrEqualTo(root.get("dob"), filter.getDobFrom()));
                }

                if (filter.getDobTo() != null) {
                    predicates.add(cb.lessThanOrEqualTo(root.get("dob"), filter.getDobTo()));
                }
            }

            return cb.and(predicates.toArray(new Predicate[0]));
        };
    }

    public static Specification<ChildrenCrm3> filterInCrm3(@NotNull ChildrenFilter filter ) {
        return (root, query, cb) -> {
            final List<Predicate> predicates = new ArrayList<>();

            // filter by field
            if (filter.getFullName() != null) {
                predicates.add( cb.like(root.get("fullName"), "%" + filter.getFullName() + "%"));
            }

            if( filter.getContactIds() !=null ){
                predicates.add( root.get("contactIdCrm2").in( filter.getContactIds())  );
            }

            if( filter.getDobFrom()!=null || filter.getDobTo()!=null ) {
                if (filter.getDobFrom() != null) {
                    predicates.add(cb.greaterThanOrEqualTo(root.get("dob"), filter.getDobFrom()));
                }

                if (filter.getDobTo() != null) {
                    predicates.add(cb.lessThanOrEqualTo(root.get("dob"), filter.getDobTo()));
                }
            }

            return cb.and(predicates.toArray(new Predicate[0]));
        };
    }
}
