package vn.educa.ods.portalapi.children;

import io.swagger.v3.oas.annotations.Hidden;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import vn.educa.ods.commons.controllers.BaseRestController;
import vn.educa.ods.commons.controllers.PageResponse;
import vn.educa.ods.commons.controllers.RestResponse;
import vn.educa.ods.commons.models.dto.AccountDto;
import vn.educa.ods.commons.models.dto.ChildrenDto;
import vn.educa.ods.commons.models.dto.ContactDto;
import vn.educa.ods.portalapi.children.components.ChildrenCrm3Repository;
import vn.educa.ods.portalapi.children.components.ChildrenMapper;
import vn.educa.ods.portalapi.children.components.ChildrenRepository;
import vn.educa.ods.portalapi.children.requests.ChildrenCRM3ByPhonesResponse;
import vn.educa.ods.portalapi.contact.ContactCRM3;
import vn.educa.ods.portalapi.contact.components.ContactCrm3Repository;
import vn.educa.ods.portalapi.contact.components.ContactFilter;
import vn.educa.ods.portalapi.contact.components.ContactMapper;
import vn.educa.ods.portalapi.exceptions.NotSupportedPortalApi;
import vn.educa.ods.portalapi.exceptions.ResourceNotFoundException;
import vn.educa.ods.portalapi.helpers.PortalApiDesc;
import vn.educa.ods.portalapi.helpers.datesync.DateSyncFilter;
import vn.educa.ods.portalapi.utils.PhoneUtil;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@Slf4j
@RequestMapping(value = "api/v1/children", produces = "application/json")
@RequiredArgsConstructor
@Tag(name = PortalApiDesc.CHILDREN_API, description = PortalApiDesc.CHILDREN_API_DESC)
public class ChildrenController extends BaseRestController {

    final ChildrenRepository childrenRepository;

    final ChildrenCrm3Repository childrenCrm3Repository;

    final ContactCrm3Repository contactCrm3Repository;

    @GetMapping("crm2/{id}")
    @Operation( summary = "Get children by ID" )
    public RestResponse<ChildrenDto> getChildrenCrm2(@PathVariable String id){

        Optional<Children> children = childrenRepository.findById(id.toLowerCase());
        if( children.isEmpty() ){
            throw new ResourceNotFoundException("[ODS] Children not found id:" + id + " in CRM2 system" );
        }

        ChildrenDto dto = ChildrenMapper.INSTANCE.entity2dto( children.get() );

        return new RestResponse.Builder<>(SUCCESS, dto).build();
    }

    @GetMapping("crm3/{id}")
    @Operation( summary = "Get children information in CRM3 system" )
    public RestResponse<ChildrenDto> getContactCRM3(@PathVariable Long id){

        Optional<ChildrenCrm3> children = childrenCrm3Repository.findById( id );
        if( children.isEmpty() ){
            throw new ResourceNotFoundException("[ODS] Children not found id:" + id + " in CRM2 system" );
        }

        ChildrenDto dto = ChildrenMapper.INSTANCE.entity2dto( children.get() );

        return new RestResponse.Builder<>(SUCCESS, dto).build();
    }

    @PostMapping("crm2/search")
    @Operation( summary = "Search contact in CRM2 system" )
    @Hidden
    public PageResponse<ContactDto> searchCrm2(@RequestBody @Valid ContactFilter filter,
                                           @RequestParam( required = false ) Integer pageIndex,
                                           @RequestParam( required = false ) Integer pageSize){

        throw new NotSupportedPortalApi("Not supported this API" );
    }

    @PostMapping("crm3/search")
    @Operation( summary = "Search contact in CRM3 system" )
    @Hidden
    public PageResponse<ContactDto> searchCrm3(@RequestBody @Valid ContactFilter filter,
                                           @RequestParam( required = false ) Integer pageIndex,
                                           @RequestParam( required = false ) Integer pageSize){

        throw new NotSupportedPortalApi("Not supported this API" );
    }

    @PostMapping(value = {"crm3/sync", "crm2/sync" })
    @Operation( summary = "Contact synchronize with CRM" )
    @Hidden
    public PageResponse<AccountDto> synchronizeCRM3(@RequestBody DateSyncFilter filter,
                                                    @RequestParam( required = false ) Integer pageIndex,
                                                    @RequestParam( required = false ) Integer pageSize){

        throw new NotSupportedPortalApi("Not supported this API" );
    }

    @PostMapping("crm3/find-by-phones")
    @Operation( summary = "Search children by phones in CRM3 system" )
    public RestResponse<ChildrenCRM3ByPhonesResponse> getChildrenCRM3ByPhones(@RequestBody @NotEmpty @Valid List<String> phones ){
        List<String> phoneStd = PhoneUtil.standard( phones );

        // get all contact by phone
        List<ContactCRM3> contacts = contactCrm3Repository.findAllByPhoneIn( phoneStd );

        List<String> contactIds = contacts.stream().map(
                ContactCRM3::getId
        ).collect(Collectors.toList());

        // Find all children by contact ids
        List<ChildrenCrm3> children = childrenCrm3Repository.findAllByContactIdCrm3In( contactIds );

        List<ChildrenDto> childrenDtoList = children.stream().map(
                ChildrenMapper.INSTANCE::entity2dto
        ).collect(Collectors.toList());

        List<ContactDto> contactDtoList = contacts.stream().map(
                ContactMapper.INSTANCE::entity2dto
        ).collect(Collectors.toList());

        final ChildrenCRM3ByPhonesResponse res  = new ChildrenCRM3ByPhonesResponse();

        res.setContacts( contactDtoList );
        res.setChildren( childrenDtoList );

        return new RestResponse.Builder<>(SUCCESS, res ).build();
    }

    @PostMapping("crm3/find-by-contacts")
    @Operation( summary = "Search children by contact (parent) in CRM3 system" )
    public RestResponse<ChildrenCRM3ByPhonesResponse> getChildrenCRM3ByContacts( @RequestBody @NotEmpty @Valid List<String> contactIds ){
        List<String> contactIdsOk = contactIds.stream().map(
                String::toUpperCase
        ).collect(Collectors.toList());

        // get all contact by phone
        List<ContactCRM3> contacts = contactCrm3Repository.findAllByIdIn( contactIdsOk );

        // Find all children by contact ids
        List<ChildrenCrm3> children = childrenCrm3Repository.findAllByContactIdCrm3In( contactIdsOk );

        List<ChildrenDto> childrenDtoList = children.stream().map(
                ChildrenMapper.INSTANCE::entity2dto
        ).collect(Collectors.toList());

        List<ContactDto> contactDtoList = contacts.stream().map(
                ContactMapper.INSTANCE::entity2dto
        ).collect(Collectors.toList());

        final ChildrenCRM3ByPhonesResponse res  = new ChildrenCRM3ByPhonesResponse();

        res.setContacts( contactDtoList );
        res.setChildren( childrenDtoList );

        return new RestResponse.Builder<>(SUCCESS, res ).build();
    }
}
