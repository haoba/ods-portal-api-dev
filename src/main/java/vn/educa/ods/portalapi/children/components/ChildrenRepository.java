package vn.educa.ods.portalapi.children.components;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;
import vn.educa.ods.portalapi.children.Children;

@Repository
public interface ChildrenRepository extends JpaRepository<Children,String>, JpaSpecificationExecutor<Children> {
}
