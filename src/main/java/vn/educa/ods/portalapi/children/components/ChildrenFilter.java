package vn.educa.ods.portalapi.children.components;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Size;
import java.util.Date;
import java.util.List;

@Setter @Getter
@Schema( description = "Search account by username, similar fullname or phone")
public class ChildrenFilter {
    @Schema( description = "Similar full name")
    @Length( min = 3, message = "Not support search with name too short (must greater 3 characters)" )
    private String fullName;

    @Schema( description = "List of Contact Id (ParentID)")
    @Size( max = 1000, message = "Not support search with more 1000 contacts")
    private List<String> contactIds;

    @Schema( description = "Date of birth after this param")
    private Date dobFrom;

    @Schema( description = "Date of birth before this param")
    private Date dobTo;

    @Schema( description = "Include non-specified DOB (null value)")
    private Boolean unknownDobIncluding = false;
}
