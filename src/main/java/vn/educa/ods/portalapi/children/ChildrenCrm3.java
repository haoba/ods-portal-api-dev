package vn.educa.ods.portalapi.children;


import lombok.Getter;
import lombok.Setter;
import vn.educa.ods.commons.models.BaseEntity;
import vn.educa.ods.commons.models.enums.EducaSystem;
import vn.educa.ods.commons.models.enums.Gender;
import vn.educa.ods.portalapi.helpers.converters.gender.Crm3GenderConverter;
import vn.educa.ods.portalapi.helpers.converters.gender.OdsGenderConverter;

import javax.persistence.*;
import java.util.Date;

@Setter @Getter
@Entity
@Table( name = "ContactChildrenCrm3")
public class ChildrenCrm3 extends BaseEntity {
    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    private String id;

    private String childrenIdCrm2;

    @Transient
    private EducaSystem system = EducaSystem.CRM3;

    private String fullName;

    private Date dob;

    @Convert( converter = OdsGenderConverter.class )
    private Gender gender;

    @Convert( converter = Crm3GenderConverter.class )
    private Gender gender2;

    private String contactIdCrm2;

    @Column( name = "contactId")
    private String contactIdCrm3;

    private String school;

    private String classLevel;

    private Date createdAt;

    private Date updatedAt;
}
