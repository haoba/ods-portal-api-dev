package vn.educa.ods.portalapi.children.components;

import org.mapstruct.Mapper;
import org.mapstruct.Named;
import org.mapstruct.factory.Mappers;
import vn.educa.ods.commons.models.dto.ChildrenDto;
import vn.educa.ods.commons.models.enums.Gender;
import vn.educa.ods.portalapi.children.Children;
import vn.educa.ods.portalapi.children.ChildrenCrm3;

@Mapper
public interface ChildrenMapper {
    ChildrenMapper INSTANCE = Mappers.getMapper( ChildrenMapper.class );

    ChildrenDto entity2dto(Children entity);
    Children    dto2entity(ChildrenDto dto);
    Children    clone(Children entity);

    ChildrenDto  entity2dto(ChildrenCrm3 entity);
    ChildrenCrm3 dto2entityCRM3(ChildrenDto dto);
    ChildrenCrm3 clone(ChildrenCrm3 entity);

    @Named("genderToGender")
    default Gender genderToGender( ChildrenCrm3 entity ){
        if( entity.getGender()==null && entity.getGender2()!=null ){
            return entity.getGender2();
        }
        return entity.getGender();
    }

    @Named("idToId")
    default String idToId( ChildrenCrm3 entity ){
       return String.valueOf( entity.getId() );
    }
}
