package vn.educa.ods.portalapi.children.components;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;
import vn.educa.ods.portalapi.children.ChildrenCrm3;

import java.util.List;
import java.util.Optional;

@Repository
public interface ChildrenCrm3Repository extends JpaRepository<ChildrenCrm3,String>, JpaSpecificationExecutor<ChildrenCrm3> {
    Optional<ChildrenCrm3> findById( Long id);

    List<ChildrenCrm3> findAllByContactIdCrm3In( List<String> ids);
}
