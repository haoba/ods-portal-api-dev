package vn.educa.ods.portalapi.children.requests;

import lombok.Data;
import vn.educa.ods.commons.models.dto.ChildrenDto;
import vn.educa.ods.commons.models.dto.ContactDto;

import java.util.List;

@Data
public class ChildrenCRM3ByPhonesResponse {
    private List<ChildrenDto> children;
    private List<ContactDto>  contacts;
}
