package vn.educa.ods.portalapi.product;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import vn.educa.ods.portalapi.product.components.RegisteredProductRepository;

import javax.validation.constraints.NotNull;

@Service
@RequiredArgsConstructor
@Slf4j
public class RegisteredProductServiceImpl implements RegisteredProductService {

    final RegisteredProductRepository registeredProductRepository;
    /**
     * Has product been registered in system yet
     * @param productToken product token with format [Code]/[token]
     * @return true if registered
     */
    public boolean productHasRegisteredAlready(@NotNull String productToken){

        final String[] parts = productToken.split("/");
        if( parts.length != 2 ){
            log.warn("Product: {} has not registered (not valid format)", productToken);
            return false;
        }

        boolean isFound = registeredProductRepository.findTopByProductCodeAndToken(
                parts[0].toUpperCase(), parts[1]
        ).isPresent();

        if( isFound ){
            log.info("Product valid: has registered ok");
            return true;
        }
        log.warn("Product: {} has not registered", productToken);
        return false;
    }
}
