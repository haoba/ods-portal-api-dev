package vn.educa.ods.portalapi.product;

public interface RegisteredProductService {
    boolean productHasRegisteredAlready(String productToken);
}
