package vn.educa.ods.portalapi.product;


import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Where;
import vn.educa.ods.commons.models.BaseEntity;

import javax.persistence.*;
import java.util.Date;

@Setter @Getter
@Entity
@Table( schema = "odsApps", name = "odsRegisteredProducts")
@Where( clause = "enable = true")
public class RegisteredProducts extends BaseEntity {
    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    private String productCode;

    private String name;

    private String token;

    private Boolean enable;

    private Date createdAt;

    private Date updatedAt;
}
