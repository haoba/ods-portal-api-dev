package vn.educa.ods.portalapi.product.components;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;
import vn.educa.ods.portalapi.product.RegisteredProducts;

import java.util.Optional;

@Repository
public interface RegisteredProductRepository extends JpaRepository<RegisteredProducts,String>, JpaSpecificationExecutor<RegisteredProducts> {
    Optional<RegisteredProducts> findTopByProductCodeAndToken( String code, String token );
}
