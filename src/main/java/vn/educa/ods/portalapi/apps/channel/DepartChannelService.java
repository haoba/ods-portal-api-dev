package vn.educa.ods.portalapi.apps.channel;

import vn.educa.ods.portalapi.apps.channel.requests.UserClassificationByLogLeanResponse;

import java.util.List;

public interface DepartChannelService {
    List<UserClassificationByLogLeanResponse> getUserClassificationByLogLean( List<String> phones, int numOfLearn15days, int numOfLearn30days, int numOfLearn90days  );
}
