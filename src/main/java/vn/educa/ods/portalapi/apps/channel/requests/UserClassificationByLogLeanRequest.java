package vn.educa.ods.portalapi.apps.channel.requests;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Size;
import java.util.List;

@Data
@Schema( description = "Lọc số buổi học")
public class UserClassificationByLogLeanRequest {
    @Size( min = 1, max = 10000, message = "Số lượng số điện thoại từ 1-10.000 số")
    @Schema( description = "Danh sách số phone, cách nhau bới dấu ',', vd: 0983111222,0983111333,983111444")
    private List<String> phones;

    @Min( value = 1, message = "Số ngày học trong vòng 15 ngày" )
    @Max( value = 15, message = "Số ngày học trong 15 ngày")
    @Schema( description = "Số ngày học trong 15 ngày gần nhất")
    private Integer numberOfLeanIn15days = 3;

    @Schema( description = "Số ngày học trong 30 ngày gần nhất")
    private Integer numberOfLeanIn30days = 0;

    @Schema( description = "Số ngày học trong 90 ngày gần nhất")
    private Integer numberOfLeanIn90days = 0;
}
