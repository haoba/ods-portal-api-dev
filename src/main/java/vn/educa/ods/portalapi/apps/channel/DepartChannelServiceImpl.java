package vn.educa.ods.portalapi.apps.channel;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import vn.educa.ods.portalapi.apps.channel.requests.UserClassificationByLogLeanResponse;

import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
public class DepartChannelServiceImpl implements DepartChannelService {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public List<UserClassificationByLogLeanResponse> getUserClassificationByLogLean(List<String> phones, int numOfLearn15days, int numOfLearn30days, int numOfLearn90days) {

        // SQL
        final String sql = sqlUserClassificationByLogLeanBuilder_v2( phones, numOfLearn15days, numOfLearn30days, numOfLearn90days );

        List<UserClassificationByLogLeanResponse> agg = jdbcTemplate.query( sql,
                // row mapping
                ( row, num ) ->{
                    UserClassificationByLogLeanResponse r = new UserClassificationByLogLeanResponse();

                    r.setAccountId( row.getString("ACCOUNT_ID"));
                    r.setUsername(  row.getString("USERNAME"));
                    r.setFullName(  row.getString("FULL_NAME"));
                    r.setDob(       row.getDate("DOB"));
                    r.setPhone(     row.getString("PHONE"));
                    r.setNumberOfLearn15days( row.getInt("NUM_LEARN_15_DAYS"));
                    r.setNumberOfLearn30days( row.getInt("NUM_LEARN_30_DAYS"));
                    r.setNumberOfLearn90days( row.getInt("NUM_LEARN_90_DAYS"));
                    r.setProduct( row.getString("PRODUCT") );
                    r.setCreatedAt( row.getDate( "CREATED_AT") );
                    r.setUpdatedAt( row.getDate( "UPDATED_AT") );

                    return r;
                });

        return agg;
    }

    private List<String> standardPhoneSql(List<String> strList) {
        List<String> phones = new ArrayList<>();

        strList.forEach(s -> {
            // trim left, right stace characters
            s = s.trim();

            // add 0 to prefix if omit
            if (!s.startsWith("0")) {
                s = "0" + s;
            }

            // append ''
            phones.add("'" + s + "'");
        });

        return phones;
    }

    private String sqlUserClassificationByLogLeanBuilder_v1(List<String> phones, Integer numOfLearn15days, Integer numOfLearn30days, Integer numOfLearn90days) {
        // Query to find user by number of learn day
        final StringBuilder sql = new StringBuilder(
                " select" +
                        "    AC.ID   ACCOUNT_ID" +
                        "  , AC.USERNAME" +
                        "  , AC.FULL_NAME" +
                        "  , AC.DOB" +
                        "  , AC.PHONE" +
                        "  , AC.CREATED_AT" +
                        "  , AC.UPDATED_AT" +
                        "  , OAC.NUM_LEARN_15_DAYS" +
                        "  , OAC.NUM_LEARN_30_DAYS" +
                        "  , OAC.NUM_LEARN_90_DAYS" +
                        "  , OAC.PRODUCT" +
                        "  , SYSDATE EFFECTIVE_DATA_DATE" +
                        " from" +
                        "    ODS_ETL.ACCOUNTS                  AC" +
                        "    left join ODS_STAGING_2.ODS_ACCOUNT_CONTACT OAC on OAC.ACCOUNT_ID = AC.ID" +
                        " where "
        );

        // split phones to list with max size 1000,
        // Oracle limits max of elements in a query must lest than 1000
        List<List<String>> phoneSegment = new ArrayList<>();
        final int SEGMENT_SIZE = 1000;
        for (int i = 0; i < phones.size(); i = i + SEGMENT_SIZE) {
            phoneSegment.add(
                    phones.subList(i, Math.min(i + SEGMENT_SIZE, phones.size() - 1))
            );
        }

        // append 'where in' phone-list
        sql.append("\n    ( (1=0)");
        phoneSegment.forEach(phoneNumbers -> {
            List<String> phoneStdList = standardPhoneSql(phoneNumbers);
            sql
                    .append(" OR AC.PHONE in (")
                    .append(String.join(",", phoneStdList))
                    .append(") ");
        });
        sql.append("    ) ");

        // append where-num-of-day
        if (numOfLearn15days > 0) {
            sql
                    .append(" AND OAC.NUM_LEARN_15_DAYS >= ")
                    .append(numOfLearn15days);
        }
        if (numOfLearn30days > 0) {
            sql
                    .append(" AND OAC.NUM_LEARN_30_DAYS >= ")
                    .append(numOfLearn30days);
        }
        if (numOfLearn90days > 0) {
            sql
                    .append(" AND OAC.NUM_LEARN_90_DAYS >= ")
                    .append(numOfLearn90days);
        }

        //DEBUG:
        log.info( sql.toString() );

        return sql.toString();
    }

    private String sqlUserClassificationByLogLeanBuilder_v2(List<String> phones, Integer numOfLearn15days, Integer numOfLearn30days, Integer numOfLearn90days) {
        // Query to find user by number of learn day
        final StringBuilder sql = new StringBuilder(
                " select" +
                        "    AC.ID   ACCOUNT_ID" +
                        "  , AC.USERNAME" +
                        "  , AC.FULL_NAME" +
                        "  , AC.DOB" +
                        "  , AC.PHONE" +
                        "  , AC.CREATED_AT" +
                        "  , AC.UPDATED_AT" +
                        "  , OAC.LEARN_15_DAYS NUM_LEARN_15_DAYS" +
                        "  , -1 NUM_LEARN_30_DAYS" +
                        "  , -1 NUM_LEARN_90_DAYS" +
                        "  , OAC.PRODUCT" +
                        "  , SYSDATE EFFECTIVE_DATA_DATE" +
                        " from" +
                        "    ODS_ETL.ACCOUNTS                  AC" +
                        "    left join ODS_ETL.ACCOUNT_MV_NUM_LEARN_15_DAYS OAC on OAC.ACCOUNT_ID = AC.ID" +
                        " where "
        );

        // split phones to list with max size 1000,
        // Oracle limits max of elements in a query must lest than 1000
        List<List<String>> phoneSegment = new ArrayList<>();
        final int SEGMENT_SIZE = 1000;
        for (int i = 0; i < phones.size(); i = i + SEGMENT_SIZE) {
            phoneSegment.add(
                    phones.subList(i, Math.min(i + SEGMENT_SIZE, phones.size() - 1))
            );
        }

        // append 'where in' phone-list
        sql.append("\n    ( (1=0)");
        phoneSegment.forEach(phoneNumbers -> {
            List<String> phoneStdList = standardPhoneSql(phoneNumbers);
            sql
                    .append(" OR AC.PHONE in (")
                    .append(String.join(",", phoneStdList))
                    .append(") ");
        });
        sql.append("    ) ");

        // append where-num-of-day
        if (numOfLearn15days > 0) {
            sql
                    .append(" AND OAC.LEARN_15_DAYS >= ")
                    .append(numOfLearn15days);
        }

        //DEBUG:
        log.info( sql.toString() );
        System.out.println("sql: " + sql.toString());
        return sql.toString();
    }
}
