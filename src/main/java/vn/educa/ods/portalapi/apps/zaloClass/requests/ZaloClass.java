package vn.educa.ods.portalapi.apps.zaloClass.requests;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.util.Date;

@Data
@Schema( description = "Zalo Account Filter Conditional")
public class ZaloClass {

    @Schema( description = "Account Educa Id")
    private String accountId;

    @Schema( description = "Account Primary Id")
    private String accountPrimaryId;

    @Schema( description = "System")
    private String system;

    @Schema( description = "Username of account")
    private String useranme;

    @Schema( description = "Phone of account")
    private String phone;

    @Schema( description = "Product")
    private String product;

    @Schema( description = "Zalo Id")
    private String zaloId;

    @Schema( description = "Zalo Fullname")
    private String zaloName;

    @Schema( description = "Class Id")
    private String classId;

    @Schema( description = "Class Code")
    private String classCode;

    @Schema( description = "Class Name")
    private String className;

    @Schema( description = "Zalo Class Name")
    private String zaloClassName;

    @Schema( description = "Zalo Class URL")
    private String zaloClassUrl;

    @Schema( description = "Created At")
    private Date createdAt;

    @Schema( description = "Updated At")
    private Date updatedAt;
}
