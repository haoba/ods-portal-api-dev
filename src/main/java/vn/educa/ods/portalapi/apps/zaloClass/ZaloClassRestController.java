package vn.educa.ods.portalapi.apps.zaloClass;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import vn.educa.ods.commons.controllers.BaseRestController;
import vn.educa.ods.commons.controllers.PageResponse;
import vn.educa.ods.portalapi.apps.zaloClass.components.ZaloAccountFilter;
import vn.educa.ods.portalapi.apps.zaloClass.components.ZaloClassFilter;
import vn.educa.ods.portalapi.apps.zaloClass.requests.ZaloAccount;
import vn.educa.ods.portalapi.apps.zaloClass.requests.ZaloClass;
import vn.educa.ods.portalapi.apps.zaloClass.services.ZaloClassService;
import vn.educa.ods.portalapi.helpers.PortalApiDesc;

import javax.validation.Valid;

@RestController
@RequestMapping( value = "api/v1/zalo-class/acccount", produces = "application/json")
@Tag(name = PortalApiDesc.ZALO_CALSS_API, description = PortalApiDesc.ZALO_CLASS_API_DESC)
public class ZaloClassRestController extends BaseRestController {

    @Autowired
    final private ZaloClassService zaloClassService;

    public ZaloClassRestController( ZaloClassService zaloClassService ){
        this.zaloClassService = zaloClassService;
    }

    /**
     * Search Zalo Account
     * @param filter filter by
     * @param pageIndex page index
     * @param pageSize page size, default 50
     * @return pagination of StudyLogAverageDto
     */
    @PostMapping("search-zalo-account")
    @Operation(
            summary = "Search Zalo Account",
            description = "Search Zalo Account"
    )
    public PageResponse<ZaloAccount> search(@RequestBody @Valid ZaloAccountFilter filter,
                                            @RequestParam( required = false) Integer pageIndex,
                                            @RequestParam( required = false) Integer pageSize
            ){

        final Pageable pageable = pageRequest( pageIndex, pageSize );

        // search by lesson
        PageResponse<ZaloAccount> pageResponse = zaloClassService.searchZaloAccount( filter, pageable );


        return pageResponse;
    }

    /**
     * Search Zalo Account
     * @param filter filter by
     * @param pageIndex page index
     * @param pageSize page size, default 50
     * @return pagination of StudyLogAverageDto
     */
    @PostMapping("search-zalo-class")
    @Operation(
            summary = "Search Zalo Class",
            description = "Search Zalo Class"
    )
    public PageResponse<ZaloClass> search(@RequestBody @Valid ZaloClassFilter filter,
                                          @RequestParam( required = false) Integer pageIndex,
                                          @RequestParam( required = false) Integer pageSize
    ){

        final Pageable pageable = pageRequest( pageIndex, pageSize );

        // search by lesson
        PageResponse<ZaloClass> pageResponse = zaloClassService.searchZaloClass( filter, pageable );


        return pageResponse;
    }
}
