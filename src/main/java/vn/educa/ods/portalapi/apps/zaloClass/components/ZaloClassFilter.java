package vn.educa.ods.portalapi.apps.zaloClass.components;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import javax.validation.constraints.Size;
import java.util.List;

@Data
@Schema( description = "Zalo Account Filter Conditional")
public class ZaloClassFilter {
    @Schema( description = "Search by account id")
    @Size( max = 1000, message = "Max items must be less than 1000")
    private List<String> accountIds;

    @Schema( description = "Search by account primary-school")
    @Size( max = 1000, message = "Max items must be less than 1000")
    private List<String> accountPrimaryIds;

    @Schema( description = "Search by username")
    @Size( max = 1000, message = "Max items must be less than 1000")
    private List<String> usernames;

    @Schema( description = "lesson part/game name, search like %string%")
    @Size( max = 1000, message = "Max items must be less than 1000")
    private List<String> phones;
}
