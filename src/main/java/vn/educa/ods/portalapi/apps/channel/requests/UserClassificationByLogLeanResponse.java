package vn.educa.ods.portalapi.apps.channel.requests;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import vn.educa.ods.portalapi.helpers.PortalApiDesc;

import java.util.Date;

@Data
@Schema( description = "Phân tập user theo số buổi học")
public class UserClassificationByLogLeanResponse {

    @Schema( description = "Account Id")
    private String accountId;

    @Schema( description = "Username")
    private String username;

    @Schema( description = "Full name")
    private String fullName;

    @Schema( description = "Date of birthday")
    private Date dob;

    @Schema( description = "Phone Number")
    private String phone;

    @Schema( description = "Số buổi học trong 15 ngày gần nhất")
    private Integer numberOfLearn15days;

    @Schema( description = "Số buổi học trong 30 ngày gần nhất")
    private Integer numberOfLearn30days;

    @Schema( description = "Số buổi học trong 90 ngày gần nhất")
    private Integer numberOfLearn90days;

    @Schema( description = PortalApiDesc.PRODUCT_CODE_DESC )
    private String product;

    @Schema( description = "Ngày tạo account")
    private Date createdAt;

    private Date updatedAt;
}
