package vn.educa.ods.portalapi.apps.zaloClass.requests;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import vn.educa.ods.portalapi.account.models.Account;

import java.util.Date;

@Data
@Schema( description = "Zalo Account Filter Conditional")
public class ZaloAccount {
    @Schema( description = "Account Id")
    private String accountId;

    @Schema( description = "System")
    private String system;

    @Schema( description = "Username of account")
    private String useranme;

    @Schema( description = "Fullname")
    private String fullName;

    @Schema( description = "Phone of account")
    private String phone;

    @Schema( description = "Email")
    private String email;

    @Schema( description = "Status of account")
    private String status;

    @Schema( description = "Date of birth")
    private Date dob;

    @Schema( description = "")
    private String product;

    @Schema( description = "Zalo Id")
    private String zaloId;

    @Schema( description = "Zalo Fullname")
    private String zaloName;

    @Schema( description = "Created At")
    private Date createdAt;

    @Schema( description = "Updated At")
    private Date updatedAt;
}
