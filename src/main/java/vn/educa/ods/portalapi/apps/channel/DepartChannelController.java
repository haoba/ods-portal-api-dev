package vn.educa.ods.portalapi.apps.channel;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import vn.educa.ods.commons.controllers.BaseRestController;
import vn.educa.ods.commons.controllers.RestResponse;
import vn.educa.ods.portalapi.apps.channel.requests.UserClassificationByLogLeanRequest;
import vn.educa.ods.portalapi.apps.channel.requests.UserClassificationByLogLeanResponse;
import vn.educa.ods.portalapi.helpers.PortalApiDesc;

import java.util.List;

@RestController
@RequestMapping(value = "api/v1/depart-channel/user-classification", produces = "application/json")
@Tag(name = PortalApiDesc.DEPART_CHANNEL_API, description = PortalApiDesc.DEPART_CHANNEL_DESC)
public class DepartChannelController extends BaseRestController {

    @Autowired
    DepartChannelService departChannelService;

    @PostMapping("filter-number-of-learn")
    @Operation(
            summary = "Filter user bu number of learn day",
            description = "Filter user bu number of learn day"
    )
    public RestResponse<List<UserClassificationByLogLeanResponse>> search(@RequestBody UserClassificationByLogLeanRequest filter,
                                                                    @RequestParam( required = false) Integer pageIndex,
                                                                    @RequestParam( required = false) Integer pageSize
            ){

        final List<UserClassificationByLogLeanResponse> res= departChannelService.getUserClassificationByLogLean(
                filter.getPhones(),
                filter.getNumberOfLeanIn15days(),
                filter.getNumberOfLeanIn30days(),
                filter.getNumberOfLeanIn90days()
        );

        return new RestResponse.Builder<>(SUCCESS, res ).build();
    }
}
