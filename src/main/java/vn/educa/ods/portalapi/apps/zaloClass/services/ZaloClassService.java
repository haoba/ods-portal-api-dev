package vn.educa.ods.portalapi.apps.zaloClass.services;

import org.springframework.data.domain.Pageable;
import vn.educa.ods.commons.controllers.PageResponse;
import vn.educa.ods.portalapi.apps.zaloClass.components.ZaloAccountFilter;
import vn.educa.ods.portalapi.apps.zaloClass.components.ZaloClassFilter;
import vn.educa.ods.portalapi.apps.zaloClass.requests.ZaloAccount;
import vn.educa.ods.portalapi.apps.zaloClass.requests.ZaloClass;

public interface ZaloClassService {
    PageResponse<ZaloAccount> searchZaloAccount(ZaloAccountFilter filter, Pageable pageable);

    PageResponse<ZaloClass> searchZaloClass(ZaloClassFilter filter, Pageable pageable);
}
