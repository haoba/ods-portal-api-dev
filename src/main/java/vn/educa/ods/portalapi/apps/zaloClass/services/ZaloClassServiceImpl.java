package vn.educa.ods.portalapi.apps.zaloClass.services;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Service;
import vn.educa.ods.commons.controllers.PageResponse;
import vn.educa.ods.portalapi.apps.zaloClass.components.ZaloAccountFilter;
import vn.educa.ods.portalapi.apps.zaloClass.components.ZaloClassFilter;
import vn.educa.ods.portalapi.apps.zaloClass.requests.ZaloAccount;
import vn.educa.ods.portalapi.apps.zaloClass.requests.ZaloClass;
import vn.educa.ods.portalapi.loglearn.regular.requests.AccountNumOfLearnResponse;
import vn.educa.ods.portalapi.utils.DateUtils;

import java.text.ParseException;
import java.util.List;

@Service
@Slf4j
public class ZaloClassServiceImpl implements ZaloClassService {
    @Autowired
    private NamedParameterJdbcTemplate jdbcTemplate;

    public PageResponse<ZaloAccount> searchZaloAccount(ZaloAccountFilter filter, Pageable pageable){

        // Selection command
        final StringBuilder sqlDataBuilder = new StringBuilder(
                "select " +
                " AC.ID         ID " +
                ", AC.SYSTEM     SYSTEM "+
                ", AC.USERNAME   USERNAME "+
                ", AC.FULL_NAME  FULL_NAME "+
                ", AC.PHONE      PHONE "+
                ", AC.EMAIL      EMAIL "+
                ", AC.STATUS     STATUS "+
                ", AC.CREATED_AT CREATED_AT "+
                ", AC.UPDATED_AT UPDATED_AT "+
                ", AC.DOB        DOB "+
                ", AC.PRODUCT    PRODUCT "+
                ", AP.ZALO_ID "+
                ", AP.ZALO_NAME "+
                "from "+
                "ODS_ETL.ACCOUNTS        AC " +
                "left join ODS_ETL.ACCOUNT_PRIMARY AP on AP.USERNAME = AC.USERNAME "+
                "where (1=1) "
        );

        // Where command
        List<String> items = filter.getAccountIds();
        if( items!=null && !items.isEmpty() ){
            sqlDataBuilder
                    .append( " and (")
                    .append(" AC.ID in (")
                    .append( "'").append( String.join("','", items)).append("'")
                    .append(")")
                    .append(")");
        }

        items = filter.getPhones();
        if( items!=null && !items.isEmpty() ){
            sqlDataBuilder
                    .append( " and (")
                    .append(" AC.PHONE in (")
                    .append( "'").append( String.join("','", items)).append("'")
                    .append(")")
                    .append(")");
        }

        items = filter.getUsernames();
        if( items!=null && !items.isEmpty() ){
            sqlDataBuilder
                    .append( " and (")
                    .append(" AC.USERNAME in (")
                    .append( "'").append( String.join("','", items)).append("'")
                    .append(")")
                    .append(")");
        }

        // pagination
        String sqlPaginationBuilder =
                "offset :offset rows " +
                        "fetch next :pageSize rows only";

        // Step1: Calculate pagination
        String sqlCount = new StringBuilder()
                .append("SELECT count(1) TOTAL from (")
                .append( sqlDataBuilder )
                .append( ")" )
                .toString();

        log.info( sqlCount );
        List<Long> nTotalElements = jdbcTemplate.query(
                sqlCount,
                (rs,num)-> rs.getLong("TOTAL")
        );

        // Step2: Query data
        final String sqlData = new StringBuilder()
                .append( sqlDataBuilder )
                .append(sqlPaginationBuilder)
                .toString();

        List<ZaloAccount> zaList = jdbcTemplate.query(
                sqlData,
                new MapSqlParameterSource()
                        .addValue("offset",     pageable.getOffset())
                        .addValue("pageSize",   pageable.getPageSize()),
                (rs, num) ->{
                    final ZaloAccount ret = new ZaloAccount();

                    ret.setAccountId( rs.getString("ID") );
                    ret.setSystem( rs.getString("SYSTEM"));
                    ret.setUseranme( rs.getString("USERNAME"));
                    ret.setFullName( rs.getString("FULL_NAME"));
                    ret.setPhone( rs.getString("PHONE"));
                    ret.setEmail( rs.getString("EMAIL"));
                    ret.setStatus( rs.getString("STATUS"));
                    ret.setDob( rs.getDate("DOB"));
                    ret.setProduct( rs.getString("PRODUCT"));
                    ret.setZaloId( rs.getString("ZALO_ID"));
                    ret.setZaloName( rs.getString("ZALO_NAME"));
                    ret.setCreatedAt( rs.getDate("CREATED_AT"));
                    ret.setUpdatedAt( rs.getDate("UPDATED_AT"));

                    return ret;
                }
        );
        System.out.println("sql zalo_account: "+sqlData);
        // Pagination result
        PageResponse<ZaloAccount> pagination = new PageResponse<>();

        pagination.setElements( zaList );
        pagination.setTotalElements( nTotalElements.get(0) );
        pagination.setTotalPages( nTotalElements.get(0).intValue() / pageable.getPageSize() + 1 );
        pagination.setPageIndex( pageable.getPageNumber() );
        pagination.setPageSize( pageable.getPageSize() );

        return pagination;
    }

    public PageResponse<ZaloClass> searchZaloClass(ZaloClassFilter filter, Pageable pageable){

        // Selection command
        final StringBuilder sqlDataBuilder = new StringBuilder(
                "select " +
                        "    A.ID           ACCOUNT_ID " +
                        "  , AC.ID          PRIMARY_ACCOUNT_ID " +
                        "  , AC.SYSTEM      SYSTEM " +
                        "  , AC.USERNAME    USERNAME " +
                        "  , AC.PHONE       PHONE " +
                        "  , AC.CREATED_AT  CREATED_AT " +
                        "  , AC.UPDATED_AT  UPDATED_AT " +
                        "  , AC.PRODUCT     PRODUCT " +
                        "  , AC.ZALO_ID     ZALO_ID " +
                        "  , AC.ZALO_NAME   ZALO_NAME " +
                        "  , CL.ID          CLASS_ID " +
                        "  , CL.CODE        CLASS_CODE " +
                        "  , CL.NAME        CLASS_NAME " +
                        "  , ZC.NAME        ZALO_CLASS_NAME " +
                        "  , ZC.URL         ZALO_CLASS_URL " +
                        "from " +
                        "    ODS_ETL.ACCOUNT_PRIMARY AC " +
                        "    left join ODS_ETL.ACCOUNTS        A on A.USERNAME = AC.USERNAME "+
                        "    left join ODS_ETL.CLASSES         CL on CL.ID = AC.CLASS_ID " +
                        "    left join ODS_ETL.ZALO_CLASSES    ZC on ZC.CLASS_ID = AC.CLASS_ID " +
                        " where (1=1) "
        );

        // Where command
        List<String> items = filter.getAccountIds();
        if( items!=null && !items.isEmpty() ){
            sqlDataBuilder
                    .append( " and (")
                    .append(" A.ID in (")
                    .append( "'").append( String.join("','", items)).append("'")
                    .append(")")
                    .append(")");
        }

        items = filter.getAccountPrimaryIds();
        if( items!=null && !items.isEmpty() ){
            sqlDataBuilder
                    .append( " and (")
                    .append(" AC.ID in (")
                    .append( "'").append( String.join("','", items)).append("'")
                    .append(")")
                    .append(")");
        }

        items = filter.getPhones();
        if( items!=null && !items.isEmpty() ){
            sqlDataBuilder
                    .append( " and (")
                    .append(" AC.PHONE in (")
                    .append( "'").append( String.join("','", items)).append("'")
                    .append(")")
                    .append(")");
        }

        items = filter.getUsernames();
        if( items!=null && !items.isEmpty() ){
            sqlDataBuilder
                    .append( " and (")
                    .append(" AC.USERNAME in (")
                    .append( "'").append( String.join("','", items)).append("'")
                    .append(")")
                    .append(")");
        }

        // pagination
        String sqlPaginationBuilder =
                "offset :offset rows " +
                        "fetch next :pageSize rows only";

        // Step1: Calculate pagination
        String sqlCount = new StringBuilder()
                .append("SELECT count(1) TOTAL from (")
                .append( sqlDataBuilder )
                .append( ")" )
                .toString();

        log.info( sqlCount );
        List<Long> nTotalElements = jdbcTemplate.query(
                sqlCount,
                (rs,num)-> rs.getLong("TOTAL")
        );

        // Step2: Query data
        final String sqlData = new StringBuilder()
                .append( sqlDataBuilder )
                .append(sqlPaginationBuilder)
                .toString();

        List<ZaloClass> zaList = jdbcTemplate.query(
                sqlData,
                new MapSqlParameterSource()
                        .addValue("offset",     pageable.getOffset())
                        .addValue("pageSize",   pageable.getPageSize()),
                (rs, num) ->{
                    final ZaloClass ret = new ZaloClass();

                    ret.setAccountId( rs.getString("ACCOUNT_ID") );
                    ret.setAccountPrimaryId( rs.getString("PRIMARY_ACCOUNT_ID") );
                    ret.setSystem( rs.getString("SYSTEM"));
                    ret.setUseranme( rs.getString("USERNAME"));
                    ret.setPhone( rs.getString("PHONE"));
                    ret.setProduct( rs.getString("PRODUCT"));
                    ret.setZaloId( rs.getString("ZALO_ID"));
                    ret.setZaloName( rs.getString("ZALO_NAME"));
                    ret.setClassId( rs.getString("CLASS_ID"));
                    ret.setClassName( rs.getString("CLASS_CODE"));
                    ret.setClassCode( rs.getString("CLASS_NAME"));
                    ret.setZaloClassName( rs.getString("ZALO_CLASS_NAME"));
                    ret.setZaloClassUrl( rs.getString("ZALO_CLASS_URL"));

                    ret.setCreatedAt( rs.getDate("CREATED_AT"));
                    ret.setUpdatedAt( rs.getDate("UPDATED_AT"));

                    return ret;
                }
        );
        System.out.println("sql zalo_class: "+sqlData);
        // Pagination result
        PageResponse<ZaloClass> pagination = new PageResponse<>();

        pagination.setElements( zaList );
        pagination.setTotalElements( nTotalElements.get(0) );
        pagination.setTotalPages( nTotalElements.get(0).intValue() / pageable.getPageSize() + 1 );
        pagination.setPageIndex( pageable.getPageNumber() );
        pagination.setPageSize( pageable.getPageSize() );

        return pagination;
    }
}
