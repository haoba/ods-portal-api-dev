package vn.educa.ods.portalapi.utils;

import vn.educa.ods.commons.models.enums.ProductCode;

public class ProductUtils {
    /**
     * Check product code is EDU.KID
     * @param productCode product code
     * @return true for KID
     */
    public static boolean isEduKID(String productCode ){
        return ProductCode.EDU_KID.toString().compareToIgnoreCase( productCode ) == 0;
    }

    /**
     * Check product code is EDU.TH
     * @param productCode product code
     * @return true for TH
     */
    public static boolean isEduTH(String productCode ){
        return ProductCode.EDU_TH.toString().compareToIgnoreCase( productCode ) == 0;
    }

    /**
     * Check product code is EDU.THCS
     * @param productCode product code
     * @return true for THCS
     */
    public static boolean isEduTHCS(String productCode ){
        return ProductCode.EDU_THCS.toString().compareToIgnoreCase( productCode ) == 0;
    }

    /**
     * Check product code is EDU.MATH
     * @param productCode product code
     * @return true for MATH
     */
    public static boolean isEduMath(String productCode ){
        return ProductCode.EDU_MATH.toString().compareToIgnoreCase( productCode ) == 0;
    }
}
