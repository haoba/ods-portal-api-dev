package vn.educa.ods.portalapi.utils;

import java.util.List;
import java.util.stream.Collectors;

public class PhoneUtil {

    /**
     * Phone Standatd
     * @param phones
     * @return
     */
    public static List<String> standard(List<String> phones){
        return phones.stream().map(x ->{
            if( !x.startsWith("0") ){
                return "0" + x;
            }
            return x;
        }).collect(Collectors.toList());
    }

    private static List<String> removeZeroPrefix( List<String> phones ){
        return phones.stream().map(x ->{
            if( x.startsWith("0") ){
                return x.substring(1);
            }
            return x;
        }).collect(Collectors.toList());
    }
}
