package vn.educa.ods.portalapi.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateUtils {

    public static int dateToDateId( Date date ) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);

        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH) + 1;
        int day = calendar.get(Calendar.DAY_OF_MONTH);

        return ((year * 100) + month) * 100 + day;
    }

    public static Date dateIdToDate(int dateId ) throws ParseException {
        return new SimpleDateFormat("yyyyMMdd").parse( "" + dateId );
    }
}
