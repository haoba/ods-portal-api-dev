package vn.educa.ods.commons.models.dto.loglearn;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import vn.educa.ods.portalapi.helpers.PortalApiDesc;
import vn.educa.ods.portalapi.lesson.models.Lesson;

import java.math.BigDecimal;
import java.util.Date;

@Data
@Schema( description = "LogLearn Champion League")
public class StudyLogChampionDto {

    @Schema( description = "If of log learn")
    protected String id;

    @Schema( description ="Account Id" )
    private String accountId;

    @Schema( description ="Username of account" )
    private String username;

    @Schema( description = PortalApiDesc.PRODUCT_CODE_DESC )
    private String product;

    @Schema( description ="Learn date" )
    private Integer dateId;

    @Schema( description ="Learn time" )
    private Date date;

    @Schema( description ="Lesson" )
    private Lesson lesson;

    @Schema( description ="Point of lesson" )
    private BigDecimal lessonPoint;

    @Schema( description ="Number of question" )
    private Integer questionNumber;

    @Schema( description ="Answer choice" )
    private String answer;
}
