package vn.educa.ods.commons.models.dto.loglearn;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import vn.educa.ods.commons.models.dto.lessons.LessonDto;
import vn.educa.ods.commons.models.dto.lessons.PartDto;
import vn.educa.ods.portalapi.helpers.PortalApiDesc;

import java.util.Date;

@Data
@Schema( description = "LogLearn theo part/game")
public class StudyLogPartDto {

    @Schema( description = "Original id")
    private String id;

    @Schema( description = PortalApiDesc.PRODUCT_CODE_DESC )
    private String product;

    @Schema( description = "Account ID")
    private String accountId;

    @Schema( description = "Username")
    private String username;

    @Schema( description = "DateID format: YYYYMMDD, etc: 20221231")
    private int dateId;

    @Schema( description = "Learn date")
    private Date date;

    private PartDto part;

    private LessonDto lesson;

    @Schema( description = "Part point")
    private Double partPoint;
}
