package vn.educa.ods.commons.models.dto.loglearn;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import vn.educa.ods.commons.models.dto.lessons.LiveClassDto;
import vn.educa.ods.commons.models.dto.lessons.PartDto;
import vn.educa.ods.portalapi.helpers.PortalApiDesc;

import java.util.Date;

@Data
@Schema( description = "LogLearn Live Class")
public class StudyLogLiveClassDto {
    @Schema( description ="Id of study log" )
    protected String id;

    @Schema( description ="Account Id" )
    private String accountId;

    @Schema( description ="Username of account" )
    private String username;

    @Schema( description = PortalApiDesc.PRODUCT_CODE_DESC )
    private String product;

    @Schema( description ="Learn date" )
    private Integer dateId;

    @Schema( description ="Learn time" )
    private Date date;

    @Schema( description ="Live Class" )
    private LiveClassDto liveClass;

    @Schema( description ="Lesson Part" )
    private PartDto part;

    @Schema( description ="Duration of answer by seconds" )
    private Integer partPoint;

    private Long timeAnswer;

    @Schema( description ="Live class index" )
    private Integer liveIndex;
}
