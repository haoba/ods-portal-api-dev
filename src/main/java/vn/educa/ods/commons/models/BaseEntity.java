package vn.educa.ods.commons.models;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.MappedSuperclass;

@Setter @Getter
@MappedSuperclass
public abstract class BaseEntity {
}
