package vn.educa.ods.commons.models.dto.lessons;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import vn.educa.ods.portalapi.helpers.PortalApiDesc;
import vn.educa.ods.portalapi.lesson.models.Platform;

import java.util.Date;

@Data
@Schema( description = "Lesson Steps (for KID only)")
public class StepDto {
    @Schema( description = "Lesson Step Id")
    private String id;

    @Schema( description = "Name of Lesson Step")
    private String name;

    @Schema( description = "Step detail description")
    private String description;

    @Schema( description = PortalApiDesc.PRODUCT_CODE_DESC )
    private String product;

    @Schema( description = "Part Id")
    private String partId;

    @Schema( description = "Platform - Kind of step")
    private Platform platform;

    @Schema( description = "Created At")
    private Date createdAt;

    @Schema( description = "Updated At")
    private Date updatedAt;
}
