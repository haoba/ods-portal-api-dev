package vn.educa.ods.commons.models.dto;


import lombok.Data;
import vn.educa.ods.commons.models.enums.Gender;

import java.util.Date;

@Data
public class ProfileDto {
    private String id;

    private String profileName;

    private String avatar;

    private String DOB;

    private Gender gender;

    private AccountDto account;

    private String country;

    private Date createdAt;

    private Date updatedAt;
}
