package vn.educa.ods.commons.models.dto.lessons;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import vn.educa.ods.portalapi.helpers.PortalApiDesc;

import java.util.Date;
import java.util.Set;

@Data
@Schema( description = "Lesson Part/Lesson Game")
public class PartDto {

    @Schema( description = "Lesson Part Id (GameID)")
    private String id;

    @Schema( description = "Lesson Part/Game code")
    private String code;

    @Schema( description = "Lesson Part/Game Name")
    private String name;

    @Schema( description = "Lesson Part/Game description")
    private String description;

    @Schema( description = "Lesson Part/Game Nam by English")
    private String nameEn;

    @Schema( description = "Lesson Part/Game description by English")
    private String descriptionEn;

    @Schema( description = "Lesson detail")
    private LessonDto lesson;

    @Schema( description = "Category of part")
    private Set<PartCategoryDto> category;

    @Schema( description = PortalApiDesc.PRODUCT_CODE_DESC )
    private String product;

    @Schema( description = "Updated date")
    private Date updatedAt;

    @Schema( description = "Created date")
    private Date createdAt;
}
