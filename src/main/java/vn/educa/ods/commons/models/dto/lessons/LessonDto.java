package vn.educa.ods.commons.models.dto.lessons;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import vn.educa.ods.portalapi.helpers.PortalApiDesc;

import java.util.Date;

@Data
@Schema( description = "Lesson Data")
public class LessonDto {

    @Schema( description = "Lesson ID")
    private String id;

    @Schema( description = "Lesson Code")
    private String code;

    @Schema( description = "Lesson Name")
    private String name;

    @Schema( description = "Description of lesson")
    private String description;

    @Schema( description = "Lesson name in English")
    private String nameEn;

    @Schema( description = "Lesson description in english")
    private String descriptionEn;

    @Schema( description = "Unit of lesson")
    private String unitId;

    @Schema( description = PortalApiDesc.PRODUCT_CODE_DESC )
    private String product;

    @Schema( description = "Updated date")
    private Date updatedAt;

    @Schema( description = "Created date")
    private Date createdAt;
}
