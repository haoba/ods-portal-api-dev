package vn.educa.ods.commons.models.dto.lessons;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import vn.educa.ods.portalapi.helpers.PortalApiDesc;

import java.util.Date;

@Data
@Schema( description = "Lesson Level")
public class LevelDto {

    @Schema( description = "Id of lesson level")
    private String id;

    @Schema( description = "Lesson Level Name")
    private String name;

    @Schema( description = "Description of lesson level")
    private String description;

    @Schema( description = "Lesson Level name by english")
    private String nameEn;

    @Schema( description = "Lesson Level description by English ")
    private String descriptionEn;

    @Schema( description = PortalApiDesc.PRODUCT_CODE_DESC )
    private String product;

    @Schema( description = "Updated date")
    private Date updatedAt;

    @Schema( description = "Created date")
    private Date createdAt;
}
