package vn.educa.ods.commons.models.dto;

import lombok.Getter;
import lombok.Setter;
import java.util.Date;
@Setter
@Getter
public class ClassDTO {
        private String id;

        private String teacherId;

        private String gradeId;

        private Integer gradeCurrent;

        private String code;

        private String name;

        private String type;

        private String status;

        private Date createdAt;

        private Date updatedAt;

        private Integer isDeleted;

        private String product;
}
