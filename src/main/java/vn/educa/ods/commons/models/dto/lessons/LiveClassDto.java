package vn.educa.ods.commons.models.dto.lessons;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import vn.educa.ods.portalapi.helpers.PortalApiDesc;

import java.util.Date;

@Data
@Schema( description = "Lesson Live Class")
public class LiveClassDto {
    @Schema( description = "Id of Live class")
    private String id;

    @Schema( description = "Name of Live class")
    private String name;

    @Schema( description = "Detail description")
    private String description;

    @Schema( description = PortalApiDesc.PRODUCT_CODE_DESC )
    private String product;

    @Schema( description = "Live class status")
    private Boolean status;

    private Date createdAt;

    private Date updatedAt;
}
