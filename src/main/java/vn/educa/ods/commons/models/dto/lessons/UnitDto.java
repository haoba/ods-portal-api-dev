package vn.educa.ods.commons.models.dto.lessons;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import vn.educa.ods.portalapi.helpers.PortalApiDesc;
import vn.educa.ods.portalapi.lesson.models.Level;

import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import java.util.Date;

@Data
@Schema( name = "LessonUnit",  description = "Lesson Unit")
public class UnitDto {

    @Schema( description = "Lesson Unit Id")
    private String id;

    @Schema( description = "Lesson Unit Code")
    private String code;

    @Schema( description = "Name of Unit")
    private String name;

    @Schema( description = "Description of Leson Unit")
    private String description;

    @Schema( description = "Lesson Unit name by english")
    private String nameEn;

    @Schema( description = "Lesson Unit description by english")
    private String descriptionEn;

    @Schema( description = PortalApiDesc.PRODUCT_CODE_DESC )
    private String product;

    @ManyToOne( fetch = FetchType.EAGER )
    @Schema( description = "Lesson Level")
    private Level level;

    @Schema( description = "Updated date")
    private Date updatedAt;

    @Schema( description = "Created date")
    private Date createdAt;
}
