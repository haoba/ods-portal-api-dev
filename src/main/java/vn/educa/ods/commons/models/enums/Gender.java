package vn.educa.ods.commons.models.enums;

public enum Gender {
    FEMALE,
    MALE,
    OTHER
}
