package vn.educa.ods.commons.models.dto;


import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import vn.educa.ods.commons.models.enums.EducaSystem;
import vn.educa.ods.commons.models.enums.Gender;

import java.util.Date;

@Data
@Schema( description = "Contact Children")
public class ChildrenDto {

    @Schema( description = "Id of children")
    private String id;

    @Schema( description = "CRM2,CRM3 system")
    private EducaSystem system = EducaSystem.CRM;

    @Schema( description = "Full Name")
    private String fullName;

    @Schema( description = "Date of Birthday")
    private Date dob;

    @Schema( description = "Gender")
    private Gender gender;

    @Schema( description = "ContactId (Parent) in CRM2 system")
    private String contactIdCrm2;

    @Schema( description = "ContactId (Parent) in CRM3 system")
    private String contactIdCrm3;

    @Schema( description = "School")
    private String school;

    @Schema( description = "Class, Student class")
    private String classLevel;

    private Date createdAt;

    private Date updatedAt;

    public String getContactIdCrm3() {
        if( contactIdCrm3!=null && !contactIdCrm3.isEmpty() &&
                !contactIdCrm3.startsWith("CRM3.") ){
            return "CRM3." + contactIdCrm3;
        }
        return contactIdCrm3;
    }
}
