package vn.educa.ods.commons.models.enums;

public enum CountryCode {
    VN("VN"),
    TL("TL"),
    UNK("UNK");

    final String value;
    CountryCode( String value ){
        this.value = value;
    }

    String getValue(){
        return value;
    }
    public static CountryCode parse(String code ){
        switch ( code ){
            case "VN": return VN;
            case "TL": return TL;
            default: return UNK;
        }
    }
}
