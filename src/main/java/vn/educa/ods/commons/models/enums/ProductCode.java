package vn.educa.ods.commons.models.enums;

public enum ProductCode {
    EDU_KID( "EDU.KID"),
    EDU_TH(  "EDU.TH"),
    EDU_THCS("EDU.THCS"),
    EDU_MATH("EDU.EDU_MATH"),
    UNK("UNK");

    final String value;
    ProductCode(String value ){
        this.value = value;
    }

    @Override
    public String toString(){
        return value;
    }

    public String getValue(){
        return value;
    }
    public static ProductCode parse(String code ){
        switch ( code ){
            case "EDU.KID":  return EDU_KID;
            case "EDU.TH":   return EDU_TH;
            case "EDU.THCS": return EDU_THCS;
            case "EDU.EDU_MATH": return EDU_MATH;
            default: return UNK;
        }
    }
}
