package vn.educa.ods.commons.models.dto.loglearn;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import vn.educa.ods.commons.models.dto.lessons.LessonDto;
import vn.educa.ods.portalapi.helpers.PortalApiDesc;

@Data
@Schema( description = "LogLearn da duoc tong hop theo ngay")
public class StudyLogAverageDto {

    @Schema( description = PortalApiDesc.PRODUCT_CODE_DESC )
    private String product;

    @Schema( description = "Account ID")
    private String accountId;

    @Schema( description = "Username")
    private String username;

    @Schema( description = "DateID format: YYYYMMDD, etc: 20221231")
    private int dateId;

    private LessonDto lesson;

    @Schema( description = "Max point of part in this date")
    private Double partPointMax;

    @Schema( description = "Min point of part in this date")
    private Double partPointMin;

    @Schema( description = "Average point of part in this date")
    private Double partPointAvg;

    @Schema( description = "Number of part count in day")
    private int partCount;
}
