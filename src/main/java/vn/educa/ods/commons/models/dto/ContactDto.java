package vn.educa.ods.commons.models.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import vn.educa.ods.commons.models.enums.EducaSystem;
import vn.educa.ods.commons.models.enums.Gender;

import java.util.Date;

@Data
@Schema( description = "Contact, Khach hang")
public class ContactDto {
    @Schema( description = "ContactId in the specified system")
    private String id;

    @Schema( description = "System CRM2, CRM3")
    private EducaSystem system;

    @Schema( description = "Full name")
    private String fullName;

    @Schema( description = "Address of contact")
    private String address;


    @Schema( description = "Email address")
    private String email;

    @Schema( description = "Gender")
    private Gender gender;

    @Schema( description = "Major phone number")
    private String phone;

    @Schema( description = "Phone checksum using for mapping external system")
    private String phoneMd5;

    @Schema( description = "Paid/VIP user or not")
    private Boolean isPaidUser;

    @Schema( description = "District")
    private String districtCode;

    @Schema( description = "Province")
    private String provinceCode;

    private Date createdAt;

    private Date updatedAt;
}
