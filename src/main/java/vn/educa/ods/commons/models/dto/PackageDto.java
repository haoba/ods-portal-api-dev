package vn.educa.ods.commons.models.dto;


import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Setter @Getter
public class PackageDto {
    private Long id;

    private String name;

    private String description;

    private Integer kind;

    private Integer type;

    private Double price;

    private Long days;

    private String isMainPackage;

    private Date createdAt;

    private Date updatedAt;
}
