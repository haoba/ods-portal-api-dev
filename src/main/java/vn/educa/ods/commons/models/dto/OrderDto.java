package vn.educa.ods.commons.models.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import vn.educa.ods.commons.models.enums.EducaSystem;

import java.math.BigDecimal;
import java.util.Date;

@Data
@Schema( description = "Order, Deal won, Đơn hàng")
public class OrderDto {

    private String id;

    private EducaSystem system;

    private String packageId;

    private String packageIdCrm3;

    private String packageGiftId;

    private String code;

    private Integer days;

    private String phone;

    private BigDecimal price;

    private String userId;

    private Date date;

    private Date createdAt;

    private Date updatedAt;

}
