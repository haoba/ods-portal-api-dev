package vn.educa.ods.commons.models.dto.lessons;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import vn.educa.ods.commons.models.enums.PartCategoryType;
import vn.educa.ods.portalapi.helpers.PortalApiDesc;

import java.util.Date;

@Data
@Schema( description = "Lesson Category")
public class PartCategoryDto {
    @Schema( description = "Id of lesson category")
    private String id;

    @Schema( description = "Name of category")
    private String name;

    @Schema( description = "Description of category")
    private String description;

    @Schema( description = "Category code")
    private PartCategoryType type;

    @Schema( description = "Is active or not")
    private Boolean isActive;

    @Schema( description = PortalApiDesc.PRODUCT_CODE_DESC )
    private String product;

    private Date createdAt;

    private Date updatedAt;
}
