package vn.educa.ods.commons.models.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import vn.educa.ods.portalapi.helpers.PortalApiDesc;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Account DTO
 */
@Data
@Schema( description = "Account Package / Gói học account đã mua")
public class AccountPackageDto {

    @Schema( description = "Account Package Id")
    private String id;

    @Schema( description = "Account Id")
    private String accountId;

    @Schema( description = "Account detail")
    private AccountDto account;

    @Schema( description = "PackageId")
    private String packageId;

    @Schema( description = "Package that account has bought")
    private PackageDto packages;

    @Schema( description = "Activate Code")
    private String activateCode;

    @Schema( description = "Md5 of activate code, using mapping in external system")
    private String activateCodeMd5;

    @Schema( description = "Activate date of this package")
    private Date startTime;

    @Schema( description = "Expired date of this package")
    private Date endTime;

    private BigDecimal type;

    @Schema( description = "Is paid account")
    private Boolean isPaid;

    @Schema( description = PortalApiDesc.PRODUCT_CODE_DESC )
    private String product;

    @Schema( description = "Product group" )
    private String productGroup;

    private Date createdAt;

    private Date updatedAt;
}
