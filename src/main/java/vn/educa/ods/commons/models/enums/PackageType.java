package vn.educa.ods.commons.models.enums;

public enum PackageType {
    KID_OLD(0);

    int value;

    PackageType( int value ){
        this.value = value;
    }

    public int getValue(){
        return this.value;
    }
}
