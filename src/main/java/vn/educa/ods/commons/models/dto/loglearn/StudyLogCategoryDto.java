package vn.educa.ods.commons.models.dto.loglearn;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import vn.educa.ods.commons.models.dto.lessons.StepDto;
import vn.educa.ods.commons.models.dto.lessons.UnitDto;
import vn.educa.ods.portalapi.helpers.PortalApiDesc;

import java.math.BigDecimal;
import java.util.Date;

@Data
@Schema( description = "LogLearn Category")
public class StudyLogCategoryDto {

    @Schema( description = "Category ID")
    protected String id;

    @Schema( description ="Account Id" )
    private String accountId;

    @Schema( description ="Username of account" )
    private String username;

    @Schema( description = PortalApiDesc.PRODUCT_CODE_DESC )
    private String product;

    @Schema( description ="Learn date" )
    private Integer dateId;

    @Schema( description ="Learn time" )
    private Date date;

    @Schema( description ="Learn Steps" )
    private StepDto step;

    @Schema( description ="Step point" )
    private BigDecimal stepPoint;

    @Schema( description ="Learn Unit" )
    private UnitDto unit;
}
