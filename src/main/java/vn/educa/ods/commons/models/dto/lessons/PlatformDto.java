package vn.educa.ods.commons.models.dto.lessons;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import vn.educa.ods.portalapi.helpers.PortalApiDesc;

import java.util.Date;

@Data
@Schema( description = "Lesson Platform")
public class PlatformDto {
    @Schema( description = "Lesson platform Id")
    private String id;

    @Schema( description = "Name of platform")
    private String name;

    @Schema( description = "Platform description")
    private String description;

    @Schema( description = "Kind of platform (LISTEN, SPEAK, WRITE, READ)")
    private String kindCode;

    @Schema( description = "Status active")
    private Boolean active;

    @Schema( description = PortalApiDesc.PRODUCT_CODE_DESC )
    private String product;

    @Schema( description = "Created At")
    private Date createdAt;

    @Schema( description = "Updated At")
    private Date updatedAt;
}
