package vn.educa.ods.commons.models.enums;

public enum PartCategoryType {
    THUYET_TRINH(1),
    KHOA_HOC(2),
    DIY(3),
    STORY_BOOK(4),
    THACH_DAU(5),
    UNK(0);

    int value;

    PartCategoryType(int value ){
        this.value = value;
    }

    public int getValue(){
        return this.value;
    }

    static public PartCategoryType valueOf(Integer i ){
        switch ( i ){
            case 1: return THUYET_TRINH;
            case 2: return KHOA_HOC;
            case 3: return DIY;
            case 4: return STORY_BOOK;
            case 5: return THACH_DAU;
            default:
                return UNK;
        }
    }
}
