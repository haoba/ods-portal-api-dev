package vn.educa.ods.commons.models.dto;


import lombok.Data;
import vn.educa.ods.commons.models.enums.CountryCode;

@Data
public class DistrictDto {
    private Long id;

    private String code;

    private String code3;

    private String name;

    private CountryCode countryCode;

    private ProvinceDto province;

    private String crm2Id;
}
