package vn.educa.ods.commons.models.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import vn.educa.ods.portalapi.helpers.PortalApiDesc;

import java.util.Date;

/**
 * Account DTO
 */
@Data
@Schema( description = "Account/Student Account in product")
public class AccountDto {
    @Schema( description = "Account ID")
    private String id;

    @Schema( description = "System (EDU for Edupia Products, KID for BABILALA)")
    private String system;

    @Schema( description = "Username")
    private String username;

    @Schema( description = "Fullname")
    private String fullName;

    @Schema( description = "Phone")
    private String phone;

    @Schema( description = "Email")
    private String email;

    @Schema( description = "Date-Of-Birth")
    private Date dob;

    @Schema( description = PortalApiDesc.PRODUCT_CODE_DESC )
    private String product;

    @Schema( description = "Updated date")
    private Date updatedAt;

    @Schema( description = "Created date")
    private Date createdAt;
}
