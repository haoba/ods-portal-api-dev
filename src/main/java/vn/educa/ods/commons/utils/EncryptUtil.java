package vn.educa.ods.commons.utils;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class EncryptUtil {

    /**
     * Calculate Md5 of text
     * @param raw input text
     * @return md5 of raw text
     * @throws NoSuchAlgorithmException if not supported
     */
    public static String md5( String raw ) throws NoSuchAlgorithmException {
        String activateCodeMd5;

        MessageDigest md = MessageDigest.getInstance("MD5");

        StringBuilder sb = new StringBuilder();
        for (byte c : md.digest(raw.getBytes(StandardCharsets.UTF_8))) {
            sb.append(String.format("%02x", c));
        }

        return sb.toString();
    }
}
