package vn.educa.ods.commons.controllers;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

public abstract class BaseRestController {
    /**
     * Restful Response Code
     */
    protected static final StatusCode SUCCESS = StatusCode.SUCCESS;
    protected static final StatusCode FAILED  = StatusCode.FAILED;

    private static final int DEFAULT_PAGE_SIZE = 50;
    private static final int MAX_PAGE_SIZE     = 1000;

    /**
     *
     * @param pageIndex
     * @param pageSize
     * @param sort
     * @return
     */
    protected Pageable pageRequest(Integer pageIndex, Integer pageSize, Sort sort) {
        if (pageIndex == null) {
            pageIndex = 0;
        }

        if (pageSize == null || pageSize < 0 || pageSize > MAX_PAGE_SIZE) {
            pageSize = DEFAULT_PAGE_SIZE;
        }

        return PageRequest.of(pageIndex, pageSize, sort);
    }

    /**
     *
     * @param pageIndex
     * @param pageSize
     * @param sort
     * @return
     */
    protected Pageable pageRequest(Integer pageIndex, Integer pageSize, int maxPageSize, Sort sort ) {
        if (pageIndex == null) {
            pageIndex = 0;
        }

        if (pageSize == null || pageSize < 0 || pageSize > maxPageSize) {
            pageSize = maxPageSize;
        }

        return PageRequest.of(pageIndex, pageSize, sort);
    }

    /**
     * Create a pagination for query
     *
     * @param pageIndex
     * @param pageSize
     * @return
     */
    protected Pageable pageRequest(Integer pageIndex, Integer pageSize) {
        return pageRequest(pageIndex, pageSize, Sort.unsorted());
    }

    public enum StatusCode {
        SUCCESS(0),
        FAILED(-1);

        private int value;
        StatusCode(int value){
            this.value = value;
        }

        public int getValue(){
            return value;
        }
    }
}
