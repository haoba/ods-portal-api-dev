package vn.educa.ods.commons.controllers;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;

/**
 * Response template
 * @param <T> Data response (DTO)
 */
@Setter @Getter
@Schema( description = "Response resource", hidden = true )
public class RestResponse<T> {
    @Schema( description = "Response status (0 for success, otherwise for errors)")
    private int status;

    @Schema( description = "Message response (Success, or error message)")
    private String message;

    @Schema( description = "Data response")
    private T data;

    public static class Builder<T> {
        private final RestResponse<T> response = new RestResponse<T>();

        public Builder(BaseRestController.StatusCode statusCode, T data) {
            response.data = data;
            response.status = statusCode.getValue();
        }

        public Builder(int statusCode, T data) {
            response.status = statusCode;
            response.data   = data;
        }

        public Builder(BaseRestController.StatusCode statusCode, String message) {
            response.status  = statusCode.getValue();
            response.message = message;
        }

        public Builder<T> message(String userMessage) {
            response.message = userMessage;
            return this;
        }

        public RestResponse<T> build() {
            if( response.status==0 && (response.message==null)  ){
                response.message = "Success";
            }
            return response;
        }
    }
}
