package vn.educa.ods.commons.controllers;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import org.springframework.data.domain.Page;

import java.util.ArrayList;
import java.util.List;

/**
 * Response template with pagination
 * @param <T> Data response (DTO)
 */
@Data
@Schema(description = "Response data resource by pagination", hidden = true )
public class PageResponse<T> {

    @Schema( description = "Response status (0 for success, otherwise for errors)")
    private int status;

    @Schema( description = "Message response (Success, or error message)")
    private String message;

    @Schema( description = "Page index")
    private Integer pageIndex = 0;

    @Schema( description = "Page size")
    private Integer pageSize = 0;

    @Schema( description = "Total pages for all elements of resources")
    private Integer totalPages = 0;

    @Schema( description = "Total response elements")
    private Long totalElements = 0L;

    @Schema( description = "Element list (Data response)")
    private List<T> elements = new ArrayList<>();

    public static class Builder<T> {
        private final PageResponse<T> response = new PageResponse<T>();

        public Builder(BaseRestController.StatusCode statusCode, List<T> data, Page<?> page) {
            response.elements = data;
            response.status = statusCode.getValue();
            response.totalElements = page.getTotalElements();
            response.totalPages = page.getTotalPages();
            response.pageSize = page.getSize();
            response.pageIndex = page.getNumber();
        }

        public Builder(BaseRestController.StatusCode statusCode, String message) {
            response.status  = statusCode.getValue();
            response.message = message;
        }

        public PageResponse.Builder<T> message(String userMessage) {
            response.message = userMessage;
            return this;
        }

        public PageResponse<T> build() {
            if( response.status==0 && (response.message==null) ){
                response.message = "Success";
            }
            return response;
        }
    }
}
