package vn.educa.ods.portalapi;

import lombok.extern.slf4j.Slf4j;
import org.springframework.test.context.ActiveProfiles;

//@SpringBootTest
@Slf4j
@ActiveProfiles("test")
public class OdsPortalApiApplicationTests {
//
//    @Autowired
//    AccountController accountController;
//
//    @Autowired
//    DepartChannelController departChannelController;
//
//    @Autowired
//    ChildrenController childrenController;
//
//    @Autowired
//    ContactController contactController;
//
//    @Autowired
//    LessonRestController lessonRestController;
//
//    @Autowired
//    LevelRestController levelRestController;
//
//    @Autowired
//    PartRestController partRestController;
//
//    @Autowired
//    UnitRestController unitRestController;
//
//    @Test
//    void contextLoads() {
//        log.info("Verifying all autowire components...");
//        assertThat(accountController).isNotNull();
//        assertThat(departChannelController).isNotNull();
//        assertThat(childrenController).isNotNull();
//        assertThat(contactController).isNotNull();
//        assertThat(lessonRestController).isNotNull();
//        assertThat(levelRestController).isNotNull();
//        assertThat(partRestController).isNotNull();
//        assertThat(unitRestController).isNotNull();
//    }
}
