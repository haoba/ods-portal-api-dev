## ------------------------------------------------------------
## Deploy ODS Portal API
#    Author: tupn.hn@gmail.com
#    Created: 2022-Dec-01
## ------------------------------------------------------------
## Usage:
##  make build : build image and push to local
##  make start : start service
##  make stop  : stop service
help:
	@sed -ne '/@sed/!s/##//p' $(MAKEFILE_LIST)

# -----------------------------------------------------
# Docker build parameters
# -----------------------------------------------------
DOCKER_IMAGE_TAG = ods-portal-api:1.0.0
DOCKER_REGISTRY  = localhost:5001

# Stack name
STACK_NAME = ods-portal-api

# Load env file for secret key
-include .envfile
JASYPT_OPTS = \
    jasypt.encryptor.algorithm=PBEWithMD5AndDES   \
    jasypt.encryptor.password=$(APP_SECRET_KEY)

# -------------------------------------------------------------
# Build and push docker image
build:
	@echo Build Docker image....
	docker build -f Dockerfile -t $(DOCKER_REGISTRY)/$(DOCKER_IMAGE_TAG) ./
	@echo Push to docker registry
	docker image push $(DOCKER_REGISTRY)/$(DOCKER_IMAGE_TAG)

# -------------------------------------------------------------
# Start service
start:
	env IMAGE_TAG=$(DOCKER_REGISTRY)/$(DOCKER_IMAGE_TAG) \
	    APP_SECRET_KEY=$(APP_SECRET_KEY)    \
	    PROFILE=$(PROFILE)                  \
	    REPICA_SET=${REPICA_SET}            \
		docker stack deploy -c docker-stack.yml --resolve-image=always $(STACK_NAME)

# Stop service
stop:
	docker stack rm $(STACK_NAME)

# Encrypt password in application.properties file
encrypt-pass: ## Encrypt password : rawPass=<raw text> [APP_SECRET_KEY=secret-key]
	mvn jasypt:encrypt-value $(addprefix -D,$(JASYPT_OPTS)) -Djasypt.plugin.value=$(rawPass)
	@echo "with APP_SECRET_KEY=${APP_SECRET_KEY}"

# Decrypt password in application.properties file
decrypt-pass: ## Decrypt password : encryptedPass=<raw text>  [APP_SECRET_KEY=secret-key]
	mvn jasypt:decrypt-value $(addprefix -D,$(JASYPT_OPTS)) -Djasypt.plugin.value=$(encryptedPass)
