# ODS PortalAPI V0.1

Web API để export dữ liệu cho Developers, tích hợp trực tiếp vào các hệ thống khác.
Ứng dụng được viết trên Java SpringBoot.

## Build Project

### 1. Build on ODS supported environment

Dự án chạy trên môi trường ODS, nên đã support sẵn **Makefile** để build và CI/CD.


```bash
# Show all help
make help

# Build and push to docker image
make build

# start/stop service
make start|stop

# Encrypt password in file properties
make encrypt-pass rawPass=<raw text>

# Decrypt password
make decrypt-pass encryptedPass=<encryted string>
```

### 2. Build by command line

Để build trong môi trường Maven chỉ cần đánh lệnh

```bash
# package .jar
mvn clean package
```

### 3. Develop with IntelliJ

Để develop sử dụng IntelliJ. 

Cài đặt và cấu hình JAVA Options:

> Run/Debug Configurations > Build and Run > Add VM Options
```
-Djasypt.encryptor.password=<APP_SECRET_KEY> -Dspring.profiles.active=develop
```

### 4. Mã hóa password

Password trong file .properties nên mã hóa để bảo mật tốt hơn.
Ứng dụng sử dụng 'Jasypt' để mã hóa:

1. Cách tạo mã hóa:

- Copy file **.envfile.template** sang file **.envfile**.
- Thay đổi _APP_SECRET_KEY_ riêng của từng server

    ```bash
    # make encrypted password
    make encrypt-pass rawPass=<raw text>
  
    # Get encrypted result as
    # ENC(WtAdb3LbaWiHIXcliuYZEvscXTfskqcFkyqWvWZPPl0=)
    # Copy to application.properties
    #    spring.datasource.password=ENC(WtAdb3LbaWiHIXcliuYZEvscXTfskqcFkyqWvWZP

    # Decrypt password 
    make decrypt-pass encryptedPass=<encrypted text>
    ```
2. Chạy trên ODS environment

   ```
   make start
   ```

3. Chạy debug

    Debug app trên IntelliJ cần thiêt lập VM Options
    ```
    -Djasypt.encryptor.password=<your secret in .envfile>
    ```